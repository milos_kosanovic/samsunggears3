package com.healthassistant.app.services.measuring

/**
 * Created by darko on 11/13/17.
 */
interface LiveUpdateable {
    fun onValuesUpdated()
}