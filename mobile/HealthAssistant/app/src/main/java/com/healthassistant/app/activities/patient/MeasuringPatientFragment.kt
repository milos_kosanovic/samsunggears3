package com.healthassistant.app.activities.patient

import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.util.Log
import android.view.*
import android.widget.*
import com.healthassistant.app.HealthAssistant
import com.healthassistant.app.R
import com.healthassistant.app.helpers.CallLogHelper
import com.healthassistant.app.helpers.Constants
import com.healthassistant.app.helpers.LocalStorageService
import com.healthassistant.app.services.measuring.LiveUpdateable
import com.healthassistant.app.services.measuring.MeasureStartable
import kotlinx.android.synthetic.main.fragment_measuring_patient.*
import android.widget.CompoundButton
import com.healthassistant.app.Manifest
import com.healthassistant.app.services.http.VolleyService
import com.healthassistant.app.services.measuring.MobileSensorsService
import com.healthassistant.app.services.measuring.SAccessoryService
import org.json.JSONException
import org.json.JSONObject


class MeasuringPatientFragment : Fragment(), MeasureStartable {
    private lateinit var redLayout: LinearLayout
    private lateinit var redLayout1: LinearLayout
    private lateinit var redLayout2: LinearLayout
    private lateinit var greenLayout: LinearLayout
    private lateinit var greenLayout1: LinearLayout
    private lateinit var greenLayout2: LinearLayout
    private lateinit var cbPulse: CheckBox
    private lateinit var cbActivity: CheckBox
    private lateinit var cbLight: CheckBox
    private lateinit var cbPhone: CheckBox
    private lateinit var measureMessage: String
    private lateinit var userRole: String

    companion object {
        lateinit var liveUpdateable: LiveUpdateable
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater!!.inflate(R.layout.fragment_measuring_patient, container, false)

        PatientActivity.measureStartable = this
        val btnStartAll = rootView.findViewById<ImageButton>(R.id.btnStartAll)
        val btnStop = rootView.findViewById<ImageButton>(R.id.btnStopAll)

        greenLayout = rootView.findViewById<LinearLayout>(R.id.greenLayout)
        greenLayout1 = rootView.findViewById<LinearLayout>(R.id.greenLayout1)
        greenLayout2 = rootView.findViewById<LinearLayout>(R.id.greenLayout2)
        redLayout = rootView.findViewById<LinearLayout>(R.id.redLayout)
        redLayout1 = rootView.findViewById<LinearLayout>(R.id.redLayout1)
        redLayout2 = rootView.findViewById<LinearLayout>(R.id.redLayout2)
        cbPulse = rootView.findViewById(R.id.cbPulse)
        cbActivity = rootView.findViewById(R.id.cbActivity)
        cbLight = rootView.findViewById(R.id.cbLight)
        cbPhone = rootView.findViewById(R.id.cbPhone)

        userRole = HealthAssistant.userRole

        setCheckBoxesChecked()
        if(PatientActivity.isMeasuring) {
            setCheckBoxesDisabled()
        }

        cbPulse.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener {
            buttonView, isChecked -> run {
                HealthAssistant.cbPulseChecked = isChecked
            }
        })

        cbActivity.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener {
            buttonView, isChecked -> run {
                HealthAssistant.cbActivityChecked = isChecked
            }
        })

        cbLight.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener {
            buttonView, isChecked -> run {
                HealthAssistant.cbLightChecked = isChecked
            }
        })

        cbPhone.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener {
            buttonView, isChecked -> run {
                HealthAssistant.cbPhoneChecked = isChecked
            }
        })

        if(PatientActivity.isMeasuring) {
            setStopVisible()
        } else {
            setStartVisible()
        }

        btnStartAll.setOnClickListener(View.OnClickListener {
            if(PatientActivity.isConnected) {
                if(!checkCheckedBoxes().equals("")) {
                    PatientActivity.startMeasuring(this, checkCheckedBoxes())
                    PatientActivity.volleyService.get(Constants.MEASURING_TIMER_API_URL, null)
                    setCheckBoxesDisabled()
                } else {
                    Toast.makeText(this@MeasuringPatientFragment.context, Constants.CHOOSE_MEASURING_TYPE_MESSAGE, Toast.LENGTH_LONG).show()
                }
            } else if(userRole.equals(Constants.ADMIN_ROLE_RESPONSE_KEY)) {
                if(!checkCheckedBoxes().equals("")) {
                    PatientActivity.mobileSensorsService.startMobileSensors(this@MeasuringPatientFragment.context)
                    SAccessoryService.measurableResponse.onStartMeasuring()
                    Toast.makeText(this@MeasuringPatientFragment.context, Constants.PEER_NOT_CONNECTED_MESSAGE, Toast.LENGTH_LONG).show()
                    setCheckBoxesDisabled()
                } else {
                    Toast.makeText(this@MeasuringPatientFragment.context, Constants.CHOOSE_MEASURING_TYPE_MESSAGE, Toast.LENGTH_LONG).show()
                }
            }
            if(cbPhone.isChecked) {
                if(this@MeasuringPatientFragment.context.checkSelfPermission(android.Manifest.permission.READ_CALL_LOG) != PackageManager.PERMISSION_GRANTED) {
                    if(ActivityCompat.shouldShowRequestPermissionRationale(this@MeasuringPatientFragment.activity, android.Manifest.permission.READ_CALL_LOG)) {
                        Toast.makeText(this@MeasuringPatientFragment.context, Constants.CALL_LOG_PERMISSION_MESSAGE, Toast.LENGTH_LONG).show()
                    } else {
                        ActivityCompat.requestPermissions(this@MeasuringPatientFragment.activity, arrayOf(android.Manifest.permission.READ_CALL_LOG), 0)
                    }
                } else {
                    val curLog = CallLogHelper.getAllCallLogs(this@MeasuringPatientFragment.activity.contentResolver)
                    LocalStorageService.setLocalData(this@MeasuringPatientFragment.context, Constants.CALLS_DURATION, CallLogHelper.countCallsDuration(curLog))
                }
            }
        })

        btnStop.setOnClickListener {
            PatientActivity.mobileSensorsService.unregisterSensors()
            PatientActivity.isMeasuring = false
            onStopMeasuring()
            PatientActivity.stopMeasuring(this)
            try {
                PatientActivity.stopWatchSensorsTimer.cancel()
                PatientActivity.startWatchSensorsTimer.cancel()
            } catch (e: UninitializedPropertyAccessException) {}
        }

        return rootView
    }

    override fun onStartMeasuring() {
        setStopVisible()
        setCheckBoxesDisabled()
    }

    override fun onStopMeasuring() {
        setStartVisible()
        setCheckBoxesEnabled()
    }

    override fun updateValues() {
        liveUpdateable.onValuesUpdated()
    }

    fun setStartVisible() {
        greenLayout.visibility = View.VISIBLE
        greenLayout1.visibility = View.VISIBLE
        greenLayout2.visibility = View.VISIBLE
        redLayout.visibility = View.GONE
        redLayout1.visibility = View.GONE
        redLayout2.visibility = View.GONE
    }

    fun setStopVisible() {
        greenLayout.visibility = View.GONE
        greenLayout1.visibility = View.GONE
        greenLayout2.visibility = View.GONE
        redLayout.visibility = View.VISIBLE
        redLayout1.visibility = View.VISIBLE
        redLayout2.visibility = View.VISIBLE
    }

    fun checkCheckedBoxes(): String {
        measureMessage = ""
        if(cbPulse.isChecked && cbActivity.isChecked && cbLight.isChecked) {
            measureMessage = Constants.START_MEASURING_MESSAGE
        } else {
            if(cbPulse.isChecked) {
                measureMessage += Constants.PULSE_MEASURING_MESSAGE
            }
            if(cbActivity.isChecked) {
                measureMessage += Constants.ACTIVITY_MEASURING_MESSAGE
            }
            if(cbLight.isChecked) {
                measureMessage += Constants.LIGHT_MEASURING_MESSAGE
            }
        }

        if(measureMessage.equals("") && cbPhone.isChecked) {
            measureMessage = Constants.PHONE_MEASURING_MESSAGE
        }
        return measureMessage
    }

    fun setCheckBoxesChecked() {
        cbPulse.isChecked = HealthAssistant.cbPulseChecked
        cbActivity.isChecked = HealthAssistant.cbActivityChecked
        cbLight.isChecked = HealthAssistant.cbLightChecked
        cbPhone.isChecked = HealthAssistant.cbPhoneChecked
    }

    fun setCheckBoxesDisabled() {
        cbPulse.setEnabled(false)
        cbActivity.setEnabled(false)
        cbLight.setEnabled(false)
        cbPhone.setEnabled(false)
    }

    fun setCheckBoxesEnabled() {
        cbPulse.setEnabled(true)
        cbActivity.setEnabled(true)
        cbLight.setEnabled(true)
        cbPhone.setEnabled(true)
    }
}
