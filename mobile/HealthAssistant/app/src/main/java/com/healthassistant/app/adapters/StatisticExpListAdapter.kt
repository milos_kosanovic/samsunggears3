package com.healthassistant.app.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseExpandableListAdapter
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.healthassistant.app.R
import com.healthassistant.app.entities.StatisticItem

/**
 * Created by darko on 11/18/17.
 */
class StatisticExpListAdapter(val context: Context, val itemsList: List<StatisticItem>, val subItemsList: HashMap<StatisticItem, ArrayList<StatisticItem>>) : BaseExpandableListAdapter() {

    override fun getGroup(position: Int): Any {
        return itemsList.get(position)
    }

    override fun isChildSelectable(p0: Int, p1: Int): Boolean {
        return false
    }

    override fun hasStableIds(): Boolean {
        return false
    }

    override fun getGroupView(groupPosition: Int, isExpanded: Boolean, convertView: View?, parent: ViewGroup): View {
        val inflater = this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.statistic_expandable_item, null)

        val statisticItem = getGroup(groupPosition) as StatisticItem

        val statisticItemDate = view.findViewById<TextView>(R.id.txtStatisticDate)
        val expandButton = view.findViewById<ImageView>(R.id.expandButton)

        if(isExpanded) {
            expandButton.setImageResource(R.drawable.up_arrow)
        } else {
            expandButton.setImageResource(R.drawable.down_arrow)
        }

        statisticItemDate.text = statisticItem.date

        return view
    }

    override fun getChildrenCount(position: Int): Int {
        return subItemsList.get(itemsList.get(position))!!.size
    }

    override fun getChild(groupPosition: Int, childPosition: Int): Any {
        return subItemsList.get(itemsList.get(groupPosition))!!.get(childPosition)
    }

    override fun getGroupId(position: Int): Long {
        return position.toLong()
    }

    override fun getChildView(groupPosition: Int, childPosition: Int, isLastChild: Boolean, convertView: View?, parent: ViewGroup): View {
        val inflater = this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.statistic_expandable_subitem, null)

        val subItem = getChild(groupPosition, childPosition) as StatisticItem

        val statisticItemValue = view.findViewById<TextView>(R.id.txtStatisticValue)
        val statisticItemTime = view.findViewById<TextView>(R.id.txtStatisticTime)
        val statisticTypeImage = view.findViewById<ImageView>(R.id.statisticTypeImage)

        statisticItemValue.text = subItem.value
        statisticItemTime.text = subItem.time
        statisticTypeImage.setImageResource(subItem.icon)

        return view
    }

    override fun getChildId(itemPosition: Int, childPosition: Int): Long {
        return childPosition.toLong()
    }

    override fun getGroupCount(): Int {
        return itemsList.size
    }
}