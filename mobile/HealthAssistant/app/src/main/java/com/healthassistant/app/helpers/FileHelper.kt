package com.healthassistant.app.helpers

import android.content.Context
import android.os.Environment
import android.util.Log
import com.github.mikephil.charting.charts.Chart.LOG_TAG
import java.io.BufferedWriter
import java.io.File
import java.io.FileWriter
import java.io.IOException


/**
 * Created by darko on 11/28/17.
 */
class FileHelper {
    companion object {
        @JvmStatic
        fun writeDatatoCSV(file: File, data: String) {
            val fileWriter = FileWriter(file.absolutePath, true)
            try {
                val bw = BufferedWriter(fileWriter)
                bw.append(data)
                bw.newLine()
                bw.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }

        fun isExternalStorageWritable(): Boolean {
            val state = Environment.getExternalStorageState()
            return Environment.MEDIA_MOUNTED.equals(state)
        }

        fun getFilesStorageDir(context: Context, albumName: String): File {
            val file = File(context.getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS), albumName)
            if(!file.exists()) {
                if (!file.mkdirs()) {
                }
            }
            return file
        }
    }
}