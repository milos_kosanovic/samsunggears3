package com.healthassistant.app.services.http

import android.content.Context
import android.util.Log
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.healthassistant.app.helpers.Constants
import com.healthassistant.app.helpers.LocalStorageService
import org.json.JSONObject
import java.lang.reflect.InvocationTargetException


/**
 * Created by darko on 10/25/17.
 */
class VolleyService(var responseCallback: Responsable, var context: Context) {

    fun post(url: String, params: JSONObject) {
        try {
            val que = Volley.newRequestQueue(context)
            val request = object : JsonObjectRequest(Request.Method.POST, url, params,
                    Response.Listener {
                        response -> responseCallback.successResponse(response)
                    },
                    Response.ErrorListener {
                        error -> run {
                            try {
                                if(error.networkResponse.statusCode != 500) {
                                    val errData = JSONObject(String(error.networkResponse.data))
                                    responseCallback.errorResponse(errData)
                                } else {
                                    val errData = JSONObject()
                                    errData.put(Constants.MESSAGE_RESPONSE_KEY, Constants.NO_SERVER_MESSAGE)
                                    responseCallback.errorResponse(errData)
                                }
                            } catch(e: NullPointerException) {
                                e.printStackTrace()
                                val errData = JSONObject()
                                errData.put(Constants.MESSAGE_RESPONSE_KEY, error.message)
                                responseCallback.errorResponse(errData)
                            } catch(e: InvocationTargetException) {
                                e.printStackTrace()
                            }
                        }
                    }) {
                        override fun getHeaders(): MutableMap<String, String> {
                            val headers = HashMap<String, String>()
                            if(LocalStorageService.checkLocalData(context, Constants.USER_LOCAL_STORAGE)) {
                                val token = JSONObject(LocalStorageService.getLocalData(context, Constants.USER_LOCAL_STORAGE)).getString(Constants.TOKEN_RESPONSE_KEY)
                                headers.put("Authorization", "Bearer " + token)
                            }
                            return headers
                        }
                    }
            request.setRetryPolicy(DefaultRetryPolicy(50000, 5, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT))
            que.add(request)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun get(url: String, params: JSONObject?) {
        try {
            val que = Volley.newRequestQueue(context)
            val request = object : JsonObjectRequest(Request.Method.GET, url, params,
                    Response.Listener {
                        response -> responseCallback.successResponse(response)
                    },
                    Response.ErrorListener {
                        error -> run {
                            try {
                                if(error.networkResponse.statusCode != 500) {
                                    val errData = JSONObject(String(error.networkResponse.data))
                                    responseCallback.errorResponse(errData)
                                } else {
                                    val errData = JSONObject()
                                    errData.put(Constants.MESSAGE_RESPONSE_KEY, Constants.NO_SERVER_MESSAGE)
                                    responseCallback.errorResponse(errData)
                                }
                            } catch(e: NullPointerException) {
                                e.printStackTrace()
                                val errData = JSONObject()
                                responseCallback.errorResponse(errData)
                            }
                        }
                    }) {
                        override fun getHeaders(): MutableMap<String, String> {
                            val headers = HashMap<String, String>()
                            if(LocalStorageService.checkLocalData(context, Constants.USER_LOCAL_STORAGE)) {
                                val token = JSONObject(LocalStorageService.getLocalData(context, Constants.USER_LOCAL_STORAGE)).getString(Constants.TOKEN_RESPONSE_KEY)
                                Log.i("TOKEN", token)
                                headers.put("Authorization", "Bearer " + token)
                            }
                            return headers
                        }
                    }
            request.setRetryPolicy(DefaultRetryPolicy(50000, 5, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT))
            que.add(request)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}