package com.healthassistant.app.activities.login

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import com.healthassistant.app.HealthAssistant

import com.healthassistant.app.R
import com.healthassistant.app.activities.patient.PatientActivity
import com.healthassistant.app.helpers.Constants
import com.healthassistant.app.helpers.LocalStorageService
import com.healthassistant.app.helpers.NetworkConnectionService
import com.healthassistant.app.services.http.Responsable
import com.healthassistant.app.services.http.VolleyService
import org.json.JSONObject
import java.lang.reflect.InvocationTargetException

class SignUpFragment : Fragment(), Responsable {
    private lateinit var spinner: ProgressBar

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater!!.inflate(R.layout.fragment_sign_up, container, false)

        val btnSignUp = view.findViewById<Button>(R.id.btnSignUp)
        val txtName = view.findViewById<TextView>(R.id.txtName)
        val txtLastName = view.findViewById<TextView>(R.id.txtLastName)
        val txtEmail = view.findViewById<TextView>(R.id.txtEmail)
        val txtPassword = view.findViewById<TextView>(R.id.txtPassword)
        this.spinner = view.findViewById(R.id.pgSignUp)

        this.spinner.visibility = View.GONE

        btnSignUp.setOnClickListener {
            this.spinner.visibility = View.VISIBLE
            if(NetworkConnectionService.ifNetworkAvailable(this@SignUpFragment.context)) {
                if(!txtName.text.equals("") && !txtLastName.text.equals("") && !txtEmail.text.equals("") && !txtPassword.text.equals("")) {
                    if(validateEmail(txtEmail.text.toString())) {
                        val params = JSONObject()
                        params.put(Constants.NAME_PARAMS_KEY, txtName.text.toString())
                        params.put(Constants.LASTNAME_PARAMS_KEY, txtLastName.text.toString())
                        params.put(Constants.EMAIL_PARAMS_KEY, txtEmail.text.toString())
                        params.put(Constants.PASSWORD_PARAMS_KEY, txtPassword.text.toString())
                        params.put(Constants.ROLE_PARAMS_KEY, Constants.PATIENT_ROLE_RESPONSE_KEY)
                        params.put(Constants.PLAYER_ID_PARAMS_KEY, LocalStorageService.getLocalData(this@SignUpFragment.context, Constants.ONESIGNAL_PLAYER_ID_LOCALSTORAGE))
                        val volleyService = VolleyService(this, this@SignUpFragment.context)
                        volleyService.post(Constants.REGISTRATION_API_URL, params)
                    } else {
                        this.spinner.visibility = View.GONE
                        Toast.makeText(this@SignUpFragment.context, Constants.EMAIL_NOT_VALID_MESSAGE, Toast.LENGTH_SHORT).show()
                    }
                } else if(txtName.text.equals("")) {
                    this.spinner.visibility = View.GONE
                    Toast.makeText(this@SignUpFragment.context, Constants.NO_NAME_MESSAGE, Toast.LENGTH_LONG).show()
                } else if(txtLastName.text.equals("")) {
                    this.spinner.visibility = View.GONE
                    Toast.makeText(this@SignUpFragment.context, Constants.NO_LASTNAME_MESSAGE, Toast.LENGTH_LONG).show()
                } else if(txtEmail.text.equals("")) {
                    this.spinner.visibility = View.GONE
                    Toast.makeText(this@SignUpFragment.context, Constants.NO_EMAIL_MESSAGE, Toast.LENGTH_LONG).show()
                } else {
                    Toast.makeText(this@SignUpFragment.context, Constants.NO_PASSWORD_MESSAGE, Toast.LENGTH_LONG).show()
                }
            } else {
                this.spinner.visibility = View.GONE
                Toast.makeText(this@SignUpFragment.context, Constants.NO_NETWORK_CONNECTION_MESSAGE, Toast.LENGTH_LONG).show()
            }
        }

        return view
    }

    override fun successResponse(res: JSONObject) {
        this.spinner.visibility = View.GONE
        Toast.makeText(this@SignUpFragment.context, res.getString(Constants.MESSAGE_RESPONSE_KEY), Toast.LENGTH_SHORT).show()
        LocalStorageService.setLocalData(this@SignUpFragment.context, Constants.USER_LOCAL_STORAGE, res.getString(Constants.ENTITY_RESPONSE_KEY))
        HealthAssistant.userRole = JSONObject(LocalStorageService.getLocalData(this@SignUpFragment.context, Constants.USER_LOCAL_STORAGE)).getString(Constants.ROLE_PARAMS_KEY)
        startActivity(Intent(this@SignUpFragment.context, PatientActivity::class.java))
        this@SignUpFragment.activity.finish()
    }

    override fun errorResponse(err: JSONObject) {
        this.spinner.visibility = View.GONE
        try {
            Toast.makeText(this@SignUpFragment.context, err.getString(Constants.MESSAGE_RESPONSE_KEY), Toast.LENGTH_SHORT).show()
        } catch (e: InvocationTargetException) {
            e.printStackTrace()
        }
    }

    fun validateEmail(emailStr: String): Boolean {
        val matcher = Constants.VALID_EMAIL_ADDRESS_REGEX.matcher(emailStr)
        return matcher.find()
    }
}
