package com.healthassistant.app.helpers

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo



/**
 * Created by darko on 10/25/17.
 */
class NetworkConnectionService {
    companion object {
        fun ifNetworkAvailable(context: Context): Boolean {
            val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val activeNetworkInfo = connectivityManager.activeNetworkInfo
            return activeNetworkInfo != null && activeNetworkInfo.isConnected
        }
    }
}