package com.healthassistant.app.activities

import android.app.Activity
import android.app.Application
import android.content.Intent
import android.os.Bundle
import com.healthassistant.app.R
import com.healthassistant.app.helpers.Constants
import com.healthassistant.app.helpers.LocalStorageService
import android.os.CountDownTimer
import com.healthassistant.app.HealthAssistant
import com.healthassistant.app.activities.doctor.DoctorActivity
import com.healthassistant.app.activities.login.LoginActivity
import com.healthassistant.app.activities.patient.PatientActivity
import com.jaeger.library.StatusBarUtil
import org.json.JSONObject


class MainActivity : Activity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        StatusBarUtil.setColor(this@MainActivity, resources.getColor(R.color.statusBarWhite))
        val timer = object : CountDownTimer(3000, 1000) {
            override fun onTick(millisUntilFinished: Long) {}
            override fun onFinish() {
                if(LocalStorageService.checkLocalData(this@MainActivity, Constants.USER_LOCAL_STORAGE)) {
                    val userData = JSONObject(LocalStorageService.getLocalData(this@MainActivity, Constants.USER_LOCAL_STORAGE))
                    HealthAssistant.userRole = userData.getString(Constants.ROLE_RESPONSE_KEY)
                    if(userData.getString(Constants.ROLE_RESPONSE_KEY).equals(Constants.PATIENT_ROLE_RESPONSE_KEY) || userData.getString(Constants.ROLE_RESPONSE_KEY).equals(Constants.ADMIN_ROLE_RESPONSE_KEY)) {
                        startActivity(Intent(this@MainActivity, PatientActivity::class.java))
                        finish()
                    } else {
                        startActivity(Intent(this@MainActivity, DoctorActivity::class.java))
                        finish()
                    }
                } else {
                    startActivity(Intent(this@MainActivity, LoginActivity::class.java))
                    finish()
                }
            }
        }.start()
    }
}
