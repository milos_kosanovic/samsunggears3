package com.healthassistant.app.helpers;

/**
 * Created by Aleksa Simic on 5/13/2019.
 */
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import static com.healthassistant.app.helpers.ConstantsFacebook.FB_ID;
import static com.healthassistant.app.helpers.ConstantsFacebook.SDF_PATTERN;

public class FacebookHelper {
    public FacebookData fetchDataFinal(AccessToken accessToken,boolean loggedIn)
    {
        final ArrayList<ArrayList<String>> all_data=new ArrayList<>();//posts,comments,tagged_places,liked_posts
        final ArrayList<Float> avg_likes_comments=new ArrayList<>();
        final ArrayList<ArrayList<Float>> avg_reacts=new ArrayList<>();
        final ArrayList<Integer> friends_num_arr=new ArrayList<>();
        final ArrayList<String> birthday_arr=new ArrayList();
        if(loggedIn){
            GraphRequest.GraphJSONObjectCallback gCallBack=new GraphRequest.GraphJSONObjectCallback() {
                @Override
                public void onCompleted(JSONObject object, GraphResponse response) {
                    Log.d("DOBIO OBJEKAT KOD LOGIN",object.toString());
                    try{
                        String id= object.getString(FB_ID);
                        ArrayList<String> my_comments=get_my_comments(id);
                        ArrayList<String> posts= getPosts(
                                (JSONArray)object
                                        .getJSONObject(ConstantsFacebook.FB_POSTS_STRING)
                                        .get(ConstantsFacebook.FB_DATA_STRING));
                        ArrayList<String> ids= getPostIds(
                                (JSONArray)object
                                        .getJSONObject(ConstantsFacebook.FB_POSTS_STRING)
                                        .get(ConstantsFacebook.FB_DATA_STRING));
                        all_data.add(posts);
                        all_data.add(my_comments);
                        float avg_likes= getAvgLikes(ids);
                        float avg_comments= getAvgComments(ids);
                        avg_likes_comments.add(avg_likes);
                        avg_likes_comments.add(avg_comments);
                        ArrayList<Float> reacts = getAllReacts(ids);
                        avg_reacts.add(reacts);
                        ArrayList<String> tagged_places= getTaggedPlaces(
                                (JSONArray)object
                                        .getJSONObject(ConstantsFacebook.FB_TAGGED_PLACES_STRING)
                                        .get(ConstantsFacebook.FB_DATA_STRING));
                        ArrayList<String> liked_posts= getLikedPosts(
                                (JSONArray)object
                                        .getJSONObject(ConstantsFacebook.FB_LIKES_STRING)
                                        .get(ConstantsFacebook.FB_DATA_STRING));
                        all_data.add(tagged_places);
                        all_data.add(liked_posts);
                        int friends_num= getFriendsNum(object
                                .getJSONObject(ConstantsFacebook.FB_FRIENDS_STRING));
                        friends_num_arr.add(friends_num);
                        String birthday=get_birthday(object);
                        birthday_arr.add(birthday);
                    }
                    catch (JSONException e)
                    {
                        e.printStackTrace();
                    }
                }
            };
            Bundle parameters = new Bundle();
            parameters.putString(ConstantsFacebook.FB_FIELDS, ConstantsFacebook.FB_FIELDS_LEFT+ getYesterdayDate()+ConstantsFacebook.FB_FIELDS_RIGHT);
            final GraphRequest gr=GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(),gCallBack);
            gr.setParameters(parameters);
            Thread t = new Thread(new Runnable() {
                @Override
                public void run() {
                    gr.executeAndWait();
                }
            });
            t.start();
            try {
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        else {
            GraphRequest.Callback gCallback = new GraphRequest.Callback() {
                public void onCompleted(GraphResponse response) {
                    try {
                        JSONObject object=response.getJSONObject();
                        String id= object.getString(FB_ID);
                        ArrayList<String> my_comments=get_my_comments(id);
                        ArrayList<String> posts= getPosts(
                                (JSONArray)object
                                        .getJSONObject(ConstantsFacebook.FB_POSTS_STRING)
                                        .get(ConstantsFacebook.FB_DATA_STRING));
                        ArrayList<String> ids= getPostIds(
                                (JSONArray)object
                                        .getJSONObject(ConstantsFacebook.FB_POSTS_STRING)
                                        .get(ConstantsFacebook.FB_DATA_STRING));
                        all_data.add(posts);
                        all_data.add(my_comments);
                        float avg_likes= getAvgLikes(ids);
                        float avg_comments= getAvgComments(ids);
                        avg_likes_comments.add(avg_likes);
                        avg_likes_comments.add(avg_comments);
                        ArrayList<Float> reacts = getAllReacts(ids);
                        avg_reacts.add(reacts);
                        ArrayList<String> tagged_places= getTaggedPlaces(
                                (JSONArray)object
                                        .getJSONObject(ConstantsFacebook.FB_TAGGED_PLACES_STRING)
                                        .get(ConstantsFacebook.FB_DATA_STRING));
                        ArrayList<String> liked_posts= getLikedPosts(
                                (JSONArray)object
                                        .getJSONObject(ConstantsFacebook.FB_LIKES_STRING)
                                        .get(ConstantsFacebook.FB_DATA_STRING));
                        all_data.add(tagged_places);
                        all_data.add(liked_posts);
                        int friends_num= getFriendsNum(object
                                .getJSONObject(ConstantsFacebook.FB_FRIENDS_STRING));
                        friends_num_arr.add(friends_num);
                        String birthday=get_birthday(object);
                        birthday_arr.add(birthday);;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            };
            String request="me?fields=id,birthday,posts.since("+getYesterdayDate()+"),likes,tagged_places,friends";
            final GraphRequest graphRequest = new GraphRequest(AccessToken.getCurrentAccessToken(),request, null, HttpMethod.GET, gCallback);
            Thread t = new Thread(new Runnable() {
                @Override
                public void run() {
                    graphRequest.executeAndWait();
                }
            });
            t.start();
            try {
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return new FacebookData(avg_likes_comments.get(0),avg_likes_comments.get(1),friends_num_arr.get(0),avg_reacts.get(0),birthday_arr.get(0),all_data.get(1),all_data.get(0),all_data.get(2),all_data.get(3));
    }
    public String get_birthday(JSONObject obj){
        try{
            return obj.getString(ConstantsFacebook.FB_BIRTHDAY);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
            return ConstantsFacebook.FB_BIRTHDAY_EMPTY;
        }
    }
    public int getFriendsNum(JSONObject obj){
        try{
            return obj.getJSONObject(ConstantsFacebook.FB_SUMMARY).getInt(ConstantsFacebook.FB_TOTAL_COUNT);
        }
        catch (JSONException e) {
            e.printStackTrace();
            return 0;
        }
    }
    public ArrayList<String> getPosts(JSONArray posts){
        ArrayList<String> messages_from_post=new ArrayList<>();
        for (int i=0;i<posts.length();i++)
        {
            try
            {
                if(posts.getJSONObject(i).has(ConstantsFacebook.FB_POST_MESSAGE)
                        && !posts.getJSONObject(i).isNull(ConstantsFacebook.FB_POST_MESSAGE))
                {
                    String message = posts.getJSONObject(i).getString(ConstantsFacebook.FB_POST_MESSAGE);
                    messages_from_post.add(message);
                }
            }
            catch(JSONException e) {
                e.printStackTrace();
            }
        }
        return messages_from_post;
    }
    public ArrayList<String> getLikedPosts(JSONArray posts){
        ArrayList<String> liked_posts=new ArrayList<>();
        for (int i=0;i<posts.length();i++)
        {
            try
            {
                if(posts.getJSONObject(i).has(ConstantsFacebook.FB_NAME_STRING)
                        && !posts.getJSONObject(i).isNull(ConstantsFacebook.FB_NAME_STRING))
                {
                    String name = posts.getJSONObject(i).getString(ConstantsFacebook.FB_NAME_STRING);

                    if(posts.getJSONObject(i).has(ConstantsFacebook.FB_CREATED_TIME))
                        if(checkDate(parseFacebookDate(posts.getJSONObject(i).getString(ConstantsFacebook.FB_CREATED_TIME))))
                            liked_posts.add(name);
                }
            }
            catch(JSONException e) {
                e.printStackTrace();
            }
        }
        return liked_posts;
    }
    public ArrayList<String> getTaggedPlaces(JSONArray places){
        ArrayList<String> places_string=new ArrayList<>();
        for (int i=0;i<places.length();i++)
        {
            try
            {
                String places_tmp="";
                String city="";
                String country="";
                String street="";
                if (places.getJSONObject(i).has(ConstantsFacebook.FB_PLACE_STRING)){
                    String name=places.getJSONObject(i).getJSONObject(ConstantsFacebook.FB_PLACE_STRING).getString(ConstantsFacebook.FB_NAME_STRING);
                    String date=places.getJSONObject(i).getString(ConstantsFacebook.FB_CREATED_TIME);
                    if(checkDate(parseFacebookDate(date)) && places.getJSONObject(i).has(ConstantsFacebook.FB_PLACE_STRING) && places.getJSONObject(i)
                            .getJSONObject(ConstantsFacebook.FB_PLACE_STRING).has(ConstantsFacebook.FB_LOCATION_STRING))
                    {
                        city=places.getJSONObject(i).getJSONObject(ConstantsFacebook.FB_PLACE_STRING).getJSONObject(ConstantsFacebook.FB_LOCATION_STRING).getString(ConstantsFacebook.FB_CITY_STRING);
                        country=places.getJSONObject(i).getJSONObject(ConstantsFacebook.FB_PLACE_STRING).getJSONObject(ConstantsFacebook.FB_LOCATION_STRING).getString(ConstantsFacebook.FB_COUNTRY_STRING);
                        street=places.getJSONObject(i).getJSONObject(ConstantsFacebook.FB_PLACE_STRING).getJSONObject(ConstantsFacebook.FB_LOCATION_STRING).getString(ConstantsFacebook.FB_STREET_STRING);
                    }
                    places_tmp=name+ " " + city + " " + country + " " + street;
                    places_string.add(places_tmp);
                }

            }
            catch(JSONException e) {
                e.printStackTrace();
            }
        }
        return places_string;
    }
    public float getAvgLikes(ArrayList<String> ids)  {
        float count=ids.size();
        final ArrayList<Integer> likes_array=new ArrayList<>();

        GraphRequest.Callback gCallback = new GraphRequest.Callback() {
            public void onCompleted(GraphResponse response) {
                try {
                    int like_for_post=response.getJSONObject().getJSONObject(ConstantsFacebook.FB_SUMMARY).getInt(ConstantsFacebook.FB_TOTAL_COUNT);
                    likes_array.add(like_for_post);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        for (int i=0;i<ids.size();i++) {
            String request_string=ids.get(i)+ConstantsFacebook.FB_GET_LIKES;
            final GraphRequest graphRequest = new GraphRequest(AccessToken.getCurrentAccessToken(), request_string, null, HttpMethod.GET, gCallback);
            Thread t = new Thread(new Runnable() {
                @Override
                public void run() {
                    GraphResponse gResponse = graphRequest.executeAndWait();
                }
            });
            t.start();
            try {
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        int sum=0;
        for(int i=0;i<likes_array.size();i++)
        {
            sum+=likes_array.get(i);
        }
        return sum/count;
    }
    public float getAvgComments(ArrayList<String> ids)
    {
        float count=ids.size();
        final ArrayList<Integer> comments_array=new ArrayList<>();

        GraphRequest.Callback gCallback = new GraphRequest.Callback() {
            public void onCompleted(GraphResponse response) {
                try {

                    int comments_for_post=response.getJSONObject().getJSONObject(ConstantsFacebook.FB_SUMMARY).getInt(ConstantsFacebook.FB_TOTAL_COUNT);
                    comments_array.add(comments_for_post);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        for (int i=0;i<ids.size();i++) {
            String request_string=ids.get(i)+ConstantsFacebook.FB_GET_COMMENTS;
            final GraphRequest graphRequest = new GraphRequest(AccessToken.getCurrentAccessToken(), request_string, null, HttpMethod.GET, gCallback);
            Thread t = new Thread(new Runnable() {
                @Override
                public void run() {
                    GraphResponse gResponse = graphRequest.executeAndWait();
                }
            });
            t.start();
            try {
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        int sum=0;
        for(int i=0;i<comments_array.size();i++)
        {
            sum+=comments_array.get(i);
        }
        return sum/count;
    }
    public ArrayList<Float> getAllReacts(ArrayList<String> ids)
    {
        int count=ids.size();
        float sum_love=0;
        float sum_wow=0;
        float sum_haha=0;
        float sum_sad=0;
        float sum_angry=0;
        float sum_thankful=0;

        final ArrayList<Integer> love_array=new ArrayList<>();
        final ArrayList<Integer> wow_array=new ArrayList<>();
        final ArrayList<Integer> haha_array=new ArrayList<>();
        final ArrayList<Integer> sad_array=new ArrayList<>();
        final ArrayList<Integer> angry_array=new ArrayList<>();
        final ArrayList<Integer> thankful_array=new ArrayList<>();

        // String query_string=Constants.FB_REACTS_LEFT+id_string+Constants.FB_REACTS_RIGHT;
        GraphRequest.Callback gCallback = new GraphRequest.Callback() {
            public void onCompleted(GraphResponse response) {
                try {

                    int love=response.getJSONObject().getJSONObject(ConstantsFacebook.FB_REACTS_LOVE).getJSONObject(ConstantsFacebook.FB_SUMMARY).getInt(ConstantsFacebook.FB_TOTAL_COUNT);
                    int wow=response.getJSONObject().getJSONObject(ConstantsFacebook.FB_REACTS_WOW).getJSONObject(ConstantsFacebook.FB_SUMMARY).getInt(ConstantsFacebook.FB_TOTAL_COUNT);
                    int haha=response.getJSONObject().getJSONObject(ConstantsFacebook.FB_REACTS_HAHA).getJSONObject(ConstantsFacebook.FB_SUMMARY).getInt(ConstantsFacebook.FB_TOTAL_COUNT);
                    int sad=response.getJSONObject().getJSONObject(ConstantsFacebook.FB_REACTS_SAD).getJSONObject(ConstantsFacebook.FB_SUMMARY).getInt(ConstantsFacebook.FB_TOTAL_COUNT);
                    int angry=response.getJSONObject().getJSONObject(ConstantsFacebook.FB_REACTS_ANGRY).getJSONObject(ConstantsFacebook.FB_SUMMARY).getInt(ConstantsFacebook.FB_TOTAL_COUNT);
                    int thankful=response.getJSONObject().getJSONObject(ConstantsFacebook.FB_REACTS_THANKFUL).getJSONObject(ConstantsFacebook.FB_SUMMARY).getInt(ConstantsFacebook.FB_TOTAL_COUNT);

                    love_array.add(love);
                    wow_array.add(wow);
                    haha_array.add(haha);
                    sad_array.add(sad);
                    angry_array.add(angry);
                    thankful_array.add(thankful);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };

        for (int i=0;i<ids.size();i++) {
            String request_string=ConstantsFacebook.FB_REACTS_LEFT+ids.get(i)+ConstantsFacebook.FB_REACTS_RIGHT;
            final GraphRequest graphRequest = new GraphRequest(AccessToken.getCurrentAccessToken(), request_string, null, HttpMethod.GET, gCallback);
            Thread t = new Thread(new Runnable() {
                @Override
                public void run() {
                    GraphResponse gResponse = graphRequest.executeAndWait();
                }
            });
            t.start();
            try {
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        for (int i=0;i<ids.size();i++){
            sum_love+=love_array.get(i);
            sum_wow+=wow_array.get(i);
            sum_haha+=haha_array.get(i);
            sum_sad+=sad_array.get(i);
            sum_angry+=angry_array.get(i);
            sum_thankful+=thankful_array.get(i);
        }
        float avg_love=sum_love/count;
        float avg_wow=sum_wow/count;
        float avg_haha=sum_haha/count;
        float avg_sad=sum_sad/count;
        float avg_angry=sum_angry/count;
        float avg_thankful=sum_thankful/count;

        ArrayList<Float> konacni=new ArrayList<>();
        konacni.add(avg_love);
        konacni.add(avg_wow);
        konacni.add(avg_haha);
        konacni.add(avg_sad);
        konacni.add(avg_angry);
        konacni.add(avg_thankful);
        return konacni;

    }
    public ArrayList<String> get_my_comments(String id){
        final ArrayList<String> comments=new ArrayList<>();
        GraphRequest.Callback gCallback = new GraphRequest.Callback() {
            public void onCompleted(GraphResponse response) {
                try {
                    JSONArray data_posts=response.getJSONObject().getJSONObject("posts").getJSONArray(ConstantsFacebook.FB_DATA_STRING);
                    for(int i=0;i<data_posts.length();i++)
                    {
                        if(data_posts.getJSONObject(i).has("comments"))
                        {
                            JSONArray data_comments=data_posts.getJSONObject(i).getJSONObject("comments").getJSONArray("data");
                            for(int j=0;j<data_comments.length();j++)
                            {
                                String comment=data_comments.getJSONObject(i).getString(ConstantsFacebook.FB_POST_MESSAGE);
                                comments.add(comment);
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };

        String request_string=id+"/?fields=posts.since("+getYesterdayDate()+"){comments}";
        final GraphRequest graphRequest = new GraphRequest(AccessToken.getCurrentAccessToken(), request_string, null, HttpMethod.GET, gCallback);
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                GraphResponse gResponse = graphRequest.executeAndWait();
            }
        });
        t.start();
        try {
            t.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return comments;
    }
    public ArrayList<String> getPostIds(JSONArray posts){

        ArrayList<String> ids=new ArrayList<>();
        for(int i=0;i<posts.length();i++){
            try{
                String id=posts.getJSONObject(i).getString(ConstantsFacebook.FB_ID);
                ids.add(id);
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }
        }
        return ids;
    }
    public Date parseFacebookDate(String toParse)
    {
        try {
            SimpleDateFormat incomingFormat = new SimpleDateFormat(ConstantsFacebook.SDF_PATTERN);
            Date date = incomingFormat.parse(toParse);
            return date;
        }
        catch(ParseException e){
            Log.e("ParseException",e.toString());
            return null;
        }
    }
    public boolean checkDate(Date d)
    {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        return cal.getTime().before(d);
    }
    public String getYesterdayDate()
    {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -10);

        return new SimpleDateFormat(SDF_PATTERN).format(cal.getTime());
    }
    public boolean isLoggedIn() {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        return accessToken != null;
    }
}
