package com.healthassistant.app.activities.patient.notificationstabs

import android.content.Context
import android.content.Intent
import android.graphics.PorterDuff
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ExpandableListView
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.Toast

import com.healthassistant.app.R
import com.healthassistant.app.activities.login.LoginActivity
import com.healthassistant.app.adapters.NotificationsExpListAdapter
import com.healthassistant.app.entities.Notification
import com.healthassistant.app.helpers.Constants
import com.healthassistant.app.helpers.LocalStorageService
import com.healthassistant.app.services.http.Responsable
import com.healthassistant.app.services.http.VolleyService
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject

class NotificationsListFragment : Fragment(), Responsable {
    lateinit var notificationsListView: ExpandableListView
    lateinit var progressBar: ProgressBar
    lateinit var loadingLayout: LinearLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater!!.inflate(R.layout.fragment_notifications_list, container, false)
        notificationsListView = view.findViewById<ExpandableListView>(R.id.notificationsList)
        progressBar = view.findViewById(R.id.pgNotificationsLoading)
        loadingLayout = view.findViewById(R.id.loadingLayout)

        loadingLayout.visibility = View.VISIBLE
        notificationsListView.visibility = View.GONE

        progressBar.getIndeterminateDrawable().setColorFilter(resources.getColor(R.color.colorPrimary), PorterDuff.Mode.MULTIPLY)


        val volleyService = VolleyService(this, this@NotificationsListFragment.context)
        volleyService.get(Constants.NOTIFICATIONS_URL + Constants.SYSTEM_NOTIFICATION_TYPE_KEY, null)

        return view
    }

    override fun successResponse(res: JSONObject) {
        loadingLayout.visibility = View.GONE
        notificationsListView.visibility = View.VISIBLE

        if(!res.getString(Constants.ENTITY_RESPONSE_KEY).equals(Constants.READ_RESPONSE_MESSAGE)) {
            val notificationsJsonArray = JSONArray(res.getString(Constants.ENTITY_RESPONSE_KEY))

            val notificationsList = ArrayList<Notification>()
            val detailsList = HashMap<Notification, ArrayList<Notification>>()

            for (i in 0..(notificationsJsonArray.length() - 1)) {
                val currentNotifiation = JSONObject(notificationsJsonArray.get(i).toString())
                val newNotification = Notification(currentNotifiation.getInt(Constants.NOTIFICATION_ID_RESPONSE_KEY), currentNotifiation.getString(Constants.NOTIFICATION_TITLE_RESPONSE_KEY), currentNotifiation.getString(Constants.NOTIFICATION_DESCRIPTION_RESPONSE_KEY), currentNotifiation.getInt(Constants.NOTIFICATION_USER_ID_RESPONSE_KEY), currentNotifiation.getString(Constants.NOTIFICATION_STATUS_RESPONSE_KEY), currentNotifiation.getInt(Constants.NOTIFICATION_IS_READ_RESPONSE_KEY), currentNotifiation.getString(Constants.NOTIFICATION_DATE_RESPONSE_KEY))
                notificationsList.add(newNotification)
                val subItem = ArrayList<Notification>()
                subItem.add(newNotification)
                detailsList.put(notificationsList.get(i), subItem)
            }

            try {
                val adapter = NotificationsExpListAdapter(this, this@NotificationsListFragment.context, notificationsList, detailsList)
                notificationsListView.setAdapter(adapter)
            } catch(e: IllegalStateException) {
                e.printStackTrace()
            }

        }
    }

    override fun errorResponse(err: JSONObject) {
        loadingLayout.visibility = View.GONE
        notificationsListView.visibility = View.VISIBLE
        try {
            if(err.getString(Constants.ERROR_RESPONSE_KEY).equals(Constants.TOKEN_EXPIRED_RESPONSE_KEY)) {
                LocalStorageService.removeLocalData(this@NotificationsListFragment.context, Constants.USER_LOCAL_STORAGE)
                startActivity(Intent(this@NotificationsListFragment.context, LoginActivity::class.java))
                this@NotificationsListFragment.activity.finish()
            }
        } catch(e: JSONException) {
            e.printStackTrace()
            try {
                Toast.makeText(this@NotificationsListFragment.context, Constants.NO_SERVER_MESSAGE, Toast.LENGTH_SHORT).show()
            } catch (e: NullPointerException) {
                e.printStackTrace()
            }
        }
    }
}
