package com.healthassistant.app.activities.patient.statisticstabs

import android.content.Intent
import android.graphics.PorterDuff
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ExpandableListView
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.Toast

import com.healthassistant.app.R
import com.healthassistant.app.activities.login.LoginActivity
import com.healthassistant.app.adapters.StatisticExpListAdapter
import com.healthassistant.app.entities.StatisticItem
import com.healthassistant.app.helpers.Constants
import com.healthassistant.app.helpers.LocalStorageService
import com.healthassistant.app.services.http.Responsable
import com.healthassistant.app.services.http.VolleyService
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject

class PulsePatientFragment : Fragment(), Responsable {
    lateinit var pulseExpListView: ExpandableListView
    lateinit var loadingLayout: LinearLayout
    lateinit var progressBar: ProgressBar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater!!.inflate(R.layout.fragment_pulse_patient, container, false)
        pulseExpListView = view.findViewById(R.id.statisticPulseList)
        loadingLayout = view.findViewById(R.id.loadingLayout)
        progressBar = view.findViewById(R.id.progressBar)

        loadingLayout.visibility = View.VISIBLE
        pulseExpListView.visibility = View.GONE
        progressBar.getIndeterminateDrawable().setColorFilter(resources.getColor(R.color.colorPrimary), PorterDuff.Mode.MULTIPLY)


        val volleyService = VolleyService(this, this@PulsePatientFragment.context)
        volleyService.get(Constants.GET_STATISTIC_URL, null)

        return view
    }

    override fun successResponse(res: JSONObject) {
        loadingLayout.visibility = View.GONE
        pulseExpListView.visibility = View.VISIBLE

        val statisticJsonArray = JSONArray(res.getString(Constants.ENTITY_RESPONSE_KEY))
        val statisticList = ArrayList<StatisticItem>()
        val valuesList = HashMap<StatisticItem, ArrayList<StatisticItem>>()

        for(i in 0..(statisticJsonArray.length() - 1)) {
            val newStatisticItem = StatisticItem(R.drawable.pulse_icon)

            newStatisticItem.date = JSONObject(statisticJsonArray.get(i).toString()).getString("created_at")
            var isDateExist = false
            for(j in 0 .. (statisticList.size - 1)) {
                if(statisticList.get(j).date.equals(newStatisticItem.date)) {
                    isDateExist = true
                }
            }
            if(!isDateExist && !JSONObject(statisticJsonArray.get(i).toString()).getString("pulse").equals("")) {
                statisticList.add(newStatisticItem)
            }
        }

        for(i in 0 .. (statisticList.size - 1)) {
            val subItem = ArrayList<StatisticItem>()
            for(j in 0 .. (statisticJsonArray.length() - 1)) {
                if(JSONObject(statisticJsonArray.get(j).toString()).getString("created_at").equals(statisticList.get(i).date)) {
                    val newValue = StatisticItem(R.drawable.pulse_icon)
                    newValue.value = JSONObject(statisticJsonArray.get(j).toString()).getString("pulse")
                    newValue.time = JSONObject(statisticJsonArray.get(j).toString()).getString("time")
                    if(!newValue.value.equals("")) {
                        subItem.add(newValue)
                    }
                }
            }
            valuesList.put(statisticList.get(i), subItem)
        }

        try {
            val adapter = StatisticExpListAdapter(this@PulsePatientFragment.context, statisticList, valuesList)
            pulseExpListView.setAdapter(adapter)
        } catch(e: IllegalStateException) {
            e.printStackTrace()
        }
    }

    override fun errorResponse(err: JSONObject) {
        loadingLayout.visibility = View.GONE
        pulseExpListView.visibility = View.VISIBLE
        try {
            if(err.getString(Constants.ERROR_RESPONSE_KEY).equals(Constants.TOKEN_EXPIRED_RESPONSE_KEY)) {
                LocalStorageService.removeLocalData(this@PulsePatientFragment.context, Constants.USER_LOCAL_STORAGE)
                startActivity(Intent(this@PulsePatientFragment.context, LoginActivity::class.java))
                this@PulsePatientFragment.activity.finish()
            }
        } catch(e: JSONException) {
            e.printStackTrace()
            try {
                Toast.makeText(this@PulsePatientFragment.context, Constants.NO_SERVER_MESSAGE, Toast.LENGTH_SHORT).show()
            } catch (e: NullPointerException) {
                e.printStackTrace()
            }
        }
    }
}
