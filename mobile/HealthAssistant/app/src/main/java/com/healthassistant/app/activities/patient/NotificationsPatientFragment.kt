package com.healthassistant.app.activities.patient

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.design.widget.AppBarLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.view.ViewPager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Adapter

import com.healthassistant.app.R
import kotlinx.android.synthetic.main.fragment_notifications_patient.*
import java.nio.file.Files.size
import android.support.v4.app.FragmentPagerAdapter
import com.healthassistant.app.activities.patient.notificationstabs.DoctorAdvicesFragment
import com.healthassistant.app.activities.patient.notificationstabs.NotificationsListFragment
import android.support.design.widget.TabLayout
import com.healthassistant.app.helpers.Constants


class NotificationsPatientFragment : Fragment() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val rootView = inflater!!.inflate(R.layout.fragment_notifications_patient, container, false)

        val viewPager = rootView.findViewById<ViewPager>(R.id.viewpager) as ViewPager
        val tabs = rootView.findViewById<TabLayout>(R.id.result_tabs) as TabLayout

        setupViewPager(viewPager)
        tabs.setupWithViewPager(viewPager)

        return rootView
    }

    private fun setupViewPager(vPager: ViewPager) {
        val adapter = Adapter(childFragmentManager)
        adapter.addFragment(NotificationsListFragment(), Constants.TAB_SYSTEM_NOTIFICATIONS)
        adapter.addFragment(DoctorAdvicesFragment(), Constants.TAB_DOCTOR_ADVICES)
        vPager.adapter = adapter
    }

    internal class Adapter(manager: FragmentManager) : FragmentPagerAdapter(manager) {
        private val mFragmentList = ArrayList<Fragment>()
        private val mFragmentTitleList = ArrayList<String>()

        override fun getItem(position: Int): Fragment {
            return mFragmentList.get(position)
        }

        override fun getCount(): Int {
            return mFragmentList.size
        }

        fun addFragment(fragment: Fragment, title: String) {
            mFragmentList.add(fragment)
            mFragmentTitleList.add(title)
        }

        override fun getPageTitle(position: Int): CharSequence {
            return mFragmentTitleList.get(position)
        }
    }
}
