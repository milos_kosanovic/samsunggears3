package com.healthassistant.app.activities.patient

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.content.pm.PackageManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.os.IBinder
import com.healthassistant.app.R
import android.text.format.DateUtils
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import com.healthassistant.app.HealthAssistant
import com.healthassistant.app.activities.global.ProfileFragment
import com.healthassistant.app.activities.login.LoginActivity
import com.healthassistant.app.helpers.CallLogHelper
import com.healthassistant.app.helpers.Constants
import com.healthassistant.app.helpers.FileHelper
import com.healthassistant.app.helpers.LocalStorageService
import com.healthassistant.app.services.http.Responsable
import com.healthassistant.app.services.http.VolleyService
import com.healthassistant.app.services.measuring.Measurable
import com.healthassistant.app.services.measuring.MeasureStartable
import com.healthassistant.app.services.measuring.MobileSensorsService
import com.healthassistant.app.services.measuring.SAccessoryService
import com.healthassistant.app.services.notifications.Notifable
import com.healthassistant.app.services.notifications.NotificationsService
import com.jaeger.library.StatusBarUtil
import com.pusher.client.Pusher
import com.pusher.client.PusherOptions
import com.roughike.bottombar.BottomBarTab
import kotlinx.android.synthetic.main.activity_patient.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.*
import java.lang.reflect.InvocationTargetException
import java.util.*

class PatientActivity : AppCompatActivity(), Measurable, Notifable, Responsable {
    private var mIsBound: Boolean = false
    lateinit var actionBar: android.support.v7.widget.Toolbar
    lateinit var notificationsTab: BottomBarTab
    lateinit var activityDurationsTimer: CountDownTimer
    var isHrmNotificationEnabled: Boolean = true
    lateinit var activityActive: String
    private lateinit var userRole: String
    private var isAccFirstCount: Boolean = true
    private var accWatchStartedTime: Long = 0
    private var isGyrFirstCount: Boolean = true
    private var gyrWatchStartedTime: Long = 0
    private var number = 0

    companion object {
        var isConnected = false
        lateinit var connectBtn: MenuItem
        lateinit var connectedBtn: MenuItem
        lateinit var measureStartable: MeasureStartable
        lateinit var measuringMessage: String
        lateinit var measureMessage: String
        lateinit var activeTab: String
        var isMeasuring: Boolean = false
        lateinit var saService: SAccessoryService
        lateinit var volleyService: VolleyService
        lateinit var mobileSensorsService: MobileSensorsService
        lateinit var startWatchSensorsTimer: CountDownTimer
        lateinit var stopWatchSensorsTimer: CountDownTimer
        lateinit var context: Context

        @JvmStatic
        fun startMeasuring(measureS: MeasureStartable?, message: String) {
            if(measureS != null) {
                measureStartable = measureS
            }
            measuringMessage = message
            saService.startMeasuring(message)
        }

        @JvmStatic
        fun stopMeasuring(measureS: MeasureStartable?) {
            if(measureS != null) {
                measureStartable = measureS
            }
            saService.stopMeasuring(Constants.STOP_MEASURING_MESSAGE)
            val httpParams = JSONObject()
            httpParams.put(Constants.PUSHER_MESSAGE_PARAMS_KEY, Constants.MEASURING_REQUEST_SUCCESS_VALUE)
            volleyService.post(Constants.PUSHER_MEASURING_STOP_API_URL, httpParams)
        }

        @JvmStatic
        fun startWatchSensors(message: String) {
            Log.i("START_WATCH_SENSORS", "" + HealthAssistant.startMeasuringTime)
            startWatchSensorsTimer = object: CountDownTimer(HealthAssistant.startMeasuringTime, 1000) {
                override fun onFinish() {
                    if(isConnected && isMeasuring) {
                        saService.startWatchSensors(message)
                        stopWatchSensors()
                    }
                }

                override fun onTick(millisUntilFinished: Long) {}
            }.start()
        }

        @JvmStatic
        fun stopWatchSensors() {
            Log.i("STOP_WATCH_SENSORS", "START_STOP_TIME")
            stopWatchSensorsTimer.start()
        }

        @JvmStatic
        fun sendData(message: String) {
            saService.sendData(message)
        }

        @JvmStatic
        fun findPeers() {
            saService.findPeers()
        }

        @JvmStatic
        fun checkCheckedBoxes(): String {
            measureMessage = ""
            if(HealthAssistant.cbPulseChecked && HealthAssistant.cbActivityChecked && HealthAssistant.cbLightChecked) {
                measureMessage = Constants.START_MEASURING_MESSAGE
            } else {
                if(HealthAssistant.cbPulseChecked) {
                    measureMessage += Constants.PULSE_MEASURING_MESSAGE
                }
                if(HealthAssistant.cbActivityChecked) {
                    measureMessage += Constants.ACTIVITY_MEASURING_MESSAGE
                }
                if(HealthAssistant.cbLightChecked) {
                    measureMessage += Constants.LIGHT_MEASURING_MESSAGE
                }
            }

            if(measureMessage.equals("") && HealthAssistant.cbPhoneChecked) {
                measureMessage = Constants.PHONE_MEASURING_MESSAGE
            }

            return measureMessage
        }

        // Send data to server
        @JvmStatic
        fun sendMeasuringData() {
            val params = JSONObject()

            if(HealthAssistant.cbActivityChecked) {
                var stationaryDuration = Constants.STARTED_MINUTES_TIME
                var walkingDuration = Constants.STARTED_MINUTES_TIME
                var runningDuration = Constants.STARTED_MINUTES_TIME
                var inVehicleDuration = Constants.STARTED_MINUTES_TIME

                if(LocalStorageService.checkLocalData(context, Constants.STATIONARY_DURATION)) {
                    stationaryDuration = DateUtils.formatElapsedTime(LocalStorageService.getActivityData(context, Constants.STATIONARY_DURATION)).toString()
                }

                if(LocalStorageService.checkLocalData(context, Constants.WALKING_DURATION)) {
                    walkingDuration = DateUtils.formatElapsedTime(LocalStorageService.getActivityData(context, Constants.WALKING_DURATION)).toString()
                }

                if(LocalStorageService.checkLocalData(context, Constants.RUNNING_DURATION)) {
                    runningDuration = DateUtils.formatElapsedTime(LocalStorageService.getActivityData(context, Constants.RUNNING_DURATION)).toString()
                }

                if(LocalStorageService.checkLocalData(context, Constants.IN_VEHICLE_DURATION)) {
                    inVehicleDuration = DateUtils.formatElapsedTime(LocalStorageService.getActivityData(context, Constants.IN_VEHICLE_DURATION)).toString()
                }

                params.put(Constants.ACTIVITY_PARAMS_KEY, LocalStorageService.getLocalData(context, Constants.LAST_ACTIVITY))
                params.put(Constants.RUNNING_PARAMS_KEY, runningDuration)
                params.put(Constants.STATIONARY_PARAMS_KEY, stationaryDuration)
                params.put(Constants.WALKING_PARAMS_KEY, walkingDuration)
                params.put(Constants.IN_VEHICLE_PARAMS_KEY, inVehicleDuration)
            }

            if(HealthAssistant.cbPhoneChecked) {
                try {
                    val curLog = CallLogHelper.getAllCallLogs(context.contentResolver)
                    LocalStorageService.setLocalData(context, Constants.CALLS_DURATION, CallLogHelper.countCallsDuration(curLog))
                } catch(e: SecurityException) {
                    Toast.makeText(context, Constants.CALL_LOG_PERMISSION_MESSAGE, Toast.LENGTH_LONG).show()
                }

                var phoneCallsDuration = Constants.STARTED_MINUTES_TIME
                if(LocalStorageService.checkLocalData(context, Constants.CALLS_DURATION)) {
                    phoneCallsDuration = LocalStorageService.getLocalData(context, Constants.CALLS_DURATION)
                }

                params.put(Constants.PHONE_DURATION_PARAMS_KEY, phoneCallsDuration)
            }

            if(HealthAssistant.cbLightChecked) {
                params.put(Constants.LIGHT_PARAMS_KEY, LocalStorageService.getLocalData(context, Constants.LIGHT_ACTIVE))
            }

            if(HealthAssistant.cbPulseChecked) {
                params.put(Constants.PULSE_PARAMS_KEY, LocalStorageService.getLocalData(context, Constants.HRM_ACTIVE))
            }

            volleyService.post(Constants.POST_STATISTIC_URL, params)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_patient)

        val bottomBar = bottomBar
        context = this

        notificationsTab = bottomBar.getTabWithId(R.id.tab_notifications)

        val userId = JSONObject(LocalStorageService.getLocalData(this, Constants.USER_LOCAL_STORAGE)).getString("id")

        val options = PusherOptions()
        options.setCluster("eu")
        val pusher = Pusher("4db5a09a2fe8cf130080", options)

        val channel = pusher.subscribe("health-channel")

        channel.bind("measuring-event" + userId) { channelName, eventName, data ->
            run {
                val channelData = JSONObject(data)
                if(!isMeasuring) {
                    var pusherMessage = ""
                    if(channelData.getString(Constants.PUSHER_MESSAGE_KEY).equals(Constants.PUSHER_MESSAGE_START_KEY)) {
                        if(channelData.getString(Constants.PUSHER_ACTIVITY_KEY).equals(Constants.PUSHER_DATA_TRUE) && channelData.getString(Constants.PUSHER_LIGHT_KEY).equals(Constants.PUSHER_DATA_TRUE) && channelData.getString(Constants.PUSHER_PULSE_KEY).equals(Constants.PUSHER_DATA_TRUE) && channelData.getString(Constants.PUSHER_PHONE_KEY).equals(Constants.PUSHER_DATA_TRUE)) {
                            HealthAssistant.cbActivityChecked = true
                            HealthAssistant.cbLightChecked = true
                            HealthAssistant.cbPhoneChecked = true
                            HealthAssistant.cbPulseChecked = true
                            pusherMessage += Constants.START_MEASURING_MESSAGE
                        } else {
                            if(channelData.getString(Constants.PUSHER_PULSE_KEY).equals(Constants.PUSHER_DATA_TRUE)) {
                                HealthAssistant.cbPulseChecked = true
                                pusherMessage += Constants.PULSE_MEASURING_MESSAGE
                            }
                            if(channelData.getString(Constants.PUSHER_ACTIVITY_KEY).equals(Constants.PUSHER_DATA_TRUE)) {
                                HealthAssistant.cbActivityChecked = true
                                pusherMessage += Constants.ACTIVITY_MEASURING_MESSAGE
                            }
                            if(channelData.getString(Constants.PUSHER_LIGHT_KEY).equals(Constants.PUSHER_DATA_TRUE)) {
                                HealthAssistant.cbLightChecked = true
                                pusherMessage += Constants.LIGHT_MEASURING_MESSAGE
                            }
                        }
                        if(pusherMessage.equals("") && channelData.getString(Constants.PUSHER_PHONE_KEY).equals(Constants.PUSHER_DATA_TRUE)) {
                            HealthAssistant.cbPhoneChecked = true
                            pusherMessage = Constants.PHONE_MEASURING_MESSAGE
                            try {
                                val curLog = CallLogHelper.getAllCallLogs(this.contentResolver)
                                LocalStorageService.setLocalData(this, Constants.CALLS_DURATION, CallLogHelper.countCallsDuration(curLog))
                            } catch(e: SecurityException) {
                                Toast.makeText(this, Constants.CALL_LOG_PERMISSION_MESSAGE, Toast.LENGTH_LONG).show()
                            }
                        }

                        if(!pusherMessage.equals("")) {
                            try {
                                saService.startMeasuring(pusherMessage)
                            } catch (e: UninitializedPropertyAccessException) {
                                val httpParams = JSONObject()
                                httpParams.put(Constants.PUSHER_MESSAGE_PARAMS_KEY, Constants.MEASURING_REQUEST_FAILED_VALUE)
                                volleyService.post(Constants.PUSHER_MEASURING_START_API_URL, httpParams)
                            }
                        }
                    }
                } else if(channelData.getString(Constants.PUSHER_MESSAGE_KEY).equals(Constants.PUSHER_MESSAGE_STOP_KEY)) {
                    try {
                        saService.stopMeasuring(Constants.STOP_MEASURING_MESSAGE)
                        val httpParams = JSONObject()
                        httpParams.put(Constants.PUSHER_MESSAGE_PARAMS_KEY, Constants.MEASURING_REQUEST_SUCCESS_VALUE)
                        volleyService.post(Constants.PUSHER_MEASURING_STOP_API_URL, httpParams)
                    } catch(e: UninitializedPropertyAccessException) {
                        val httpParams = JSONObject()
                        httpParams.put(Constants.PUSHER_MESSAGE_PARAMS_KEY, Constants.MEASURING_REQUEST_FAILED_VALUE)
                        volleyService.post(Constants.PUSHER_MEASURING_STOP_API_URL, httpParams)
                    }
                }
            }
        }

        pusher.connect()

        mIsBound = this.bindService(Intent(this, SAccessoryService::class.java), mConnection, Context.BIND_AUTO_CREATE)

        object : CountDownTimer(3000, 1000) {
            override fun onTick(millisUntilFinished: Long) {}
            override fun onFinish() {
                saService.findPeers()
            }
        }.start()

        userRole = HealthAssistant.userRole

        actionBar = toolbar
        setSupportActionBar(toolbar)
        title = ""
        if(isMeasuring) {
            StatusBarUtil.setColor(this@PatientActivity, resources.getColor(R.color.statusBarPrimaryRed))
        } else {
            StatusBarUtil.setColor(this@PatientActivity, resources.getColor(R.color.statusBarPrimary))
        }

        volleyService = VolleyService(this, this)
        this.initActivityDurationsTimer()
        stopWatchSensorsTimer = object: CountDownTimer(Constants.MEASURING_TIME, 1000) {
            override fun onFinish() {
                saService.stopWatchSensors(Constants.STOP_MEASURING_MESSAGE)
                sendMeasuringData()
                startWatchSensors(checkCheckedBoxes())
            }

            override fun onTick(millisUntilFinished: Long) {}
        }

        btnStart.setOnClickListener {
            if(isConnected) {
                if(!checkCheckedBoxes().equals("")) {
                    startMeasuring(null, checkCheckedBoxes())
                    volleyService.get(Constants.MEASURING_TIMER_API_URL, null)
                    if(userRole.equals(Constants.ADMIN_ROLE_RESPONSE_KEY)) {
                        mobileSensorsService.startMobileSensors(this)
                    }
                    measuringStarted()
                } else {
                    bottomBar.selectTabAtPosition(1)
                    Toast.makeText(this, Constants.CHOOSE_MEASURING_TYPE_MESSAGE, Toast.LENGTH_LONG).show()
                }
            } else if(userRole.equals(Constants.ADMIN_ROLE_RESPONSE_KEY)) {
                if(!checkCheckedBoxes().equals("")) {
                    mobileSensorsService.startMobileSensors(this)
                    onStartMeasuring()
                    Toast.makeText(this, Constants.PEER_NOT_CONNECTED_MESSAGE, Toast.LENGTH_LONG).show()
                } else {
                    bottomBar.selectTabAtPosition(1)
                    Toast.makeText(this, Constants.CHOOSE_MEASURING_TYPE_MESSAGE, Toast.LENGTH_LONG).show()
                }
            }

            if(HealthAssistant.cbPhoneChecked) {
                try {
                    val curLog = CallLogHelper.getAllCallLogs(this.contentResolver)
                    LocalStorageService.setLocalData(this, Constants.CALLS_DURATION, CallLogHelper.countCallsDuration(curLog))
                } catch(e: SecurityException) {
                    Toast.makeText(this, Constants.CALL_LOG_PERMISSION_MESSAGE, Toast.LENGTH_LONG).show()
                }
            }
        }

        btnStop.setOnClickListener {
            if(isConnected) {
                stopMeasuring(null)
                try {
                    stopWatchSensorsTimer.cancel()
                    startWatchSensorsTimer.cancel()
                } catch (e: UninitializedPropertyAccessException) {}
            }
            mobileSensorsService.unregisterSensors()
            isMeasuring = false
            StatusBarUtil.setColor(this@PatientActivity, resources.getColor(R.color.statusBarPrimary))
            actionBar.setBackgroundColor(resources.getColor(R.color.colorPrimary))
            measuringStopped()
        }

        // Reset local Data on new day
        if(LocalStorageService.checkLocalData(this, Constants.ACTIVITIES_DURATION_TIME_LOCALSTORAGE)) {
            if(!DateUtils.isToday(LocalStorageService.getActivityDurationsData(this, Constants.ACTIVITIES_DURATION_TIME_LOCALSTORAGE))) {
                LocalStorageService.removeLocalData(this, Constants.ACTIVITY_ACTIVE)
                LocalStorageService.removeLocalData(this, Constants.STATIONARY_STARTED_STORAGE)
                LocalStorageService.removeLocalData(this, Constants.WALKING_STARTED_STORAGE)
                LocalStorageService.removeLocalData(this, Constants.RUNNING_STARTED_STORAGE)
                LocalStorageService.removeLocalData(this, Constants.IN_VEHICLE_STARTED_STORAGE)

                LocalStorageService.removeLocalData(this, Constants.WALKING_DURATION)
                LocalStorageService.removeLocalData(this, Constants.STATIONARY_DURATION)
                LocalStorageService.removeLocalData(this, Constants.RUNNING_DURATION)
                LocalStorageService.removeLocalData(this, Constants.IN_VEHICLE_DURATION)

                LocalStorageService.setActivityDurationsData(this, Constants.ACTIVITIES_DURATION_TIME_LOCALSTORAGE, System.currentTimeMillis())
            }
        } else {
            LocalStorageService.setActivityDurationsData(this, Constants.ACTIVITIES_DURATION_TIME_LOCALSTORAGE, System.currentTimeMillis())
        }

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.contentContainer, HomePatientFragment())
                .commit()

        activeTab = Constants.HOME_ACTIVE_TAB

        volleyService.get(Constants.NOTIFICATIONS_COUNT_URL, null)

        measuringStopped()

        mobileSensorsService = MobileSensorsService()

        bottomBar.setOnTabSelectListener { tabId ->
            when(tabId) {
                R.id.tab_home -> {
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.contentContainer, HomePatientFragment())
                            .commit()
                    activeTab = Constants.HOME_ACTIVE_TAB
                    if(isMeasuring) {
                        measuringStarted()
                    } else {
                        measuringStopped()
                    }
                    volleyService.get(Constants.NOTIFICATIONS_COUNT_URL, null)
                }
                R.id.tab_measuring -> {
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.contentContainer, MeasuringPatientFragment())
                            .commit()
                    hideMeasuringLayouts()
                    activeTab = Constants.MEASURING_ACTIVE_TAB
                    volleyService.get(Constants.NOTIFICATIONS_COUNT_URL, null)
                }
                R.id.tab_notifications -> {
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.contentContainer, NotificationsPatientFragment())
                            .commit()
                    activeTab = Constants.NOTIFICATIONS_ACTIVE_TAB
                    if(isMeasuring) {
                        measuringStarted()
                    } else {
                        measuringStopped()
                    }
                    volleyService.get(Constants.NOTIFICATIONS_COUNT_URL, null)
                }
                R.id.tab_statistic -> {
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.contentContainer, StatisticPatientFragment())
                            .commit()
                    activeTab = Constants.STATISTIC_ACTIVE_TAB
                    if(isMeasuring) {
                        measuringStarted()
                    } else {
                        measuringStopped()
                    }
                    volleyService.get(Constants.NOTIFICATIONS_COUNT_URL, null)
                }
                R.id.tab_profile -> {
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.contentContainer, ProfileFragment())
                            .commit()
                    activeTab = Constants.PROFILE_ACTIVE_TAB
                    if(isMeasuring) {
                        measuringStarted()
                    } else {
                        measuringStopped()
                    }
                    volleyService.get(Constants.NOTIFICATIONS_COUNT_URL, null)
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        if(userRole.equals(Constants.ADMIN_ROLE_RESPONSE_KEY)) {
            mobileSensorsService.registerSensors()
        }
    }

    override fun onPause() {
        super.onPause()
        if(userRole.equals(Constants.ADMIN_ROLE_RESPONSE_KEY)) {
            mobileSensorsService.unregisterSensors()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.measuring_menu, menu)
        connectBtn = menu!!.findItem(R.id.btn_connect)
        connectedBtn = menu.findItem(R.id.btn_connected)
        if(isConnected) {
            connectBtn.setVisible(false)
            connectedBtn.setVisible(true)
        } else {
            connectBtn.setVisible(true)
            connectedBtn.setVisible(false)
        }
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if(item!!.itemId == R.id.btn_connect) {
            findPeers()
        }
        return true
    }

    private fun measuringStarted() {
        if(!activeTab.equals(Constants.MEASURING_ACTIVE_TAB)) {
            startMeasuring.visibility = View.GONE
            stopMeasuring.visibility = View.VISIBLE
            layout_align.visibility = View.VISIBLE
        }
    }

    private fun measuringStopped() {
        if(!activeTab.equals(Constants.MEASURING_ACTIVE_TAB)) {
            startMeasuring.visibility = View.VISIBLE
            stopMeasuring.visibility = View.GONE
            layout_align.visibility = View.VISIBLE
        }
    }

    private fun hideMeasuringLayouts() {
        startMeasuring.visibility = View.GONE
        stopMeasuring.visibility = View.GONE
        layout_align.visibility = View.GONE
    }

    private val mConnection = object : ServiceConnection {
        override fun onServiceConnected(className: ComponentName, service: IBinder) {
            saService = (service as SAccessoryService.LocalBinder).service
            SAccessoryService.measurableResponse = this@PatientActivity
        }

        override fun onServiceDisconnected(className: ComponentName) {
            saService = SAccessoryService()
            mIsBound = false
            SAccessoryService.measurableResponse = this@PatientActivity
        }
    }

    override fun onPeerAgentNotConnected() {
        connectBtn.setVisible(true)
        connectedBtn.setVisible(false)
        isConnected = false
        measuringStopped()
    }

    override fun onPeerAgentConnectionSuccess() {
        connectBtn.setVisible(false)
        connectedBtn.setVisible(true)
        isConnected = true
    }

    override fun onPeerAgentConnectionLost() {
        Toast.makeText(applicationContext, Constants.CONNECTION_LOST_MESSAGE, Toast.LENGTH_SHORT).show()
        connectBtn.setVisible(true)
        connectedBtn.setVisible(false)
        isConnected = false
        isMeasuring = false
        StatusBarUtil.setColor(this@PatientActivity, resources.getColor(R.color.statusBarPrimary))
        actionBar.setBackgroundColor(resources.getColor(R.color.colorPrimary))
        measuringStopped()
        try {
            measureStartable.onStopMeasuring()
        } catch(e: UninitializedPropertyAccessException) {
            e.printStackTrace()
        }
    }

    override fun onStartMeasuring() {
        if(userRole.equals(Constants.ADMIN_ROLE_RESPONSE_KEY)) {
            mobileSensorsService.startMobileSensors(this)
        }

        isMeasuring = true
        StatusBarUtil.setColor(this@PatientActivity, resources.getColor(R.color.statusBarPrimaryRed))
        actionBar.setBackgroundColor(resources.getColor(R.color.colorPrimaryRed))
        measuringStarted()

        val httpParams = JSONObject()
        httpParams.put(Constants.PUSHER_MESSAGE_PARAMS_KEY, Constants.MEASURING_REQUEST_SUCCESS_VALUE)
        volleyService.post(Constants.PUSHER_MEASURING_START_API_URL, httpParams)

        try {
            measureStartable.onStartMeasuring()
        } catch(e: UninitializedPropertyAccessException) {
            e.printStackTrace()
        }
    }

    override fun onErrorMeasuring() {
        //Hardware sensors
        if(userRole.equals(Constants.ADMIN_ROLE_RESPONSE_KEY)) {
            mobileSensorsService.unregisterSensors()
        }

        isMeasuring = false
        StatusBarUtil.setColor(this@PatientActivity, resources.getColor(R.color.statusBarPrimary))
        actionBar.setBackgroundColor(resources.getColor(R.color.colorPrimary))
        measuringStopped()

        try {
            measureStartable.onStopMeasuring()
            if (LocalStorageService.checkLocalData(this, Constants.ACTIVITY_ACTIVE)) {
                when (LocalStorageService.getLocalData(this, Constants.ACTIVITY_ACTIVE)) {
                    Constants.ACTIVITY_WALKING -> {
                        val startMillis = LocalStorageService.getActivityData(this, Constants.WALKING_STARTED_STORAGE)
                        val now = System.currentTimeMillis()
                        val difference = (now - startMillis) / DateUtils.SECOND_IN_MILLIS
                        var activeDuration = LocalStorageService.getActivityData(this, Constants.WALKING_DURATION)
                        activeDuration += difference
                        LocalStorageService.setActivityData(this, Constants.WALKING_DURATION, activeDuration) // Save WALKING Duration
                    }
                    Constants.ACTIVITY_STATIONARY -> {
                        val startMillis = LocalStorageService.getActivityData(this, Constants.STATIONARY_STARTED_STORAGE)
                        val now = System.currentTimeMillis()
                        val difference = (now - startMillis) / DateUtils.SECOND_IN_MILLIS
                        var activeDuration = LocalStorageService.getActivityData(this, Constants.STATIONARY_DURATION)
                        activeDuration += difference
                        LocalStorageService.setActivityData(this, Constants.STATIONARY_DURATION, activeDuration) // Save Stationary Duration
                    }
                    Constants.ACTIVITY_RUNNING -> {
                        val startMillis = LocalStorageService.getActivityData(this, Constants.RUNNING_STARTED_STORAGE)
                        val now = System.currentTimeMillis()
                        val difference = (now - startMillis) / DateUtils.SECOND_IN_MILLIS
                        var activeDuration = LocalStorageService.getActivityData(this, Constants.RUNNING_DURATION)
                        activeDuration += difference
                        LocalStorageService.setActivityData(this, Constants.RUNNING_DURATION, activeDuration) // Save RUNNING Duration
                    }
                    Constants.ACTIVITY_IN_VEHICLE -> {
                        val startMillis = LocalStorageService.getActivityData(this, Constants.IN_VEHICLE_STARTED_STORAGE)
                        val now = System.currentTimeMillis()
                        val difference = (now - startMillis) / DateUtils.SECOND_IN_MILLIS
                        var activeDuration = LocalStorageService.getActivityData(this, Constants.IN_VEHICLE_DURATION)
                        activeDuration += difference
                        LocalStorageService.setActivityData(this, Constants.IN_VEHICLE_DURATION, activeDuration) // Save In Vehicle Duration
                    }
                }
                LocalStorageService.removeLocalData(this, Constants.ACTIVITY_ACTIVE)
            }
        } catch(e: UninitializedPropertyAccessException) {
            e.printStackTrace()
        }
    }

    override fun onNotConnected() {
        isMeasuring = false
        StatusBarUtil.setColor(this@PatientActivity, resources.getColor(R.color.statusBarPrimary))
        actionBar.setBackgroundColor(resources.getColor(R.color.colorPrimary))
        measuringStopped()

        val httpParams = JSONObject()
        httpParams.put(Constants.PUSHER_MESSAGE_PARAMS_KEY, Constants.MEASURING_REQUEST_FAILED_VALUE)
        volleyService.post(Constants.PUSHER_MEASURING_START_API_URL, httpParams)
    }

    override fun onSuccessMeasure(message: String) {
        Log.i("WATCH_DATA", message)
        this.number++
        if (isMeasuring) {
            if (message.contains("Pulse")) {
                val hrmValue = message.removePrefix("Pulse ").split(",")[0]
                LocalStorageService.setLocalData(this, Constants.HRM_ACTIVE, hrmValue)
                try {
                    measureStartable.updateValues()
                } catch(e: UninitializedPropertyAccessException) {
                    e.printStackTrace()
                }

                if(isHrmNotificationEnabled) {
                    if (hrmValue.toInt() > Constants.HRM_TOO_HIGH_VALUE) {
                        val contents = JSONObject().put(Constants.LANGUAGE_ONESIGNAL_KEY, Constants.HRM_TOO_HIGH_NOTIFICATION_TITLE + ": " + hrmValue.toInt())
                        val params = JSONObject().put(Constants.CONTENTS_ONESIGNAL_KEY, contents)
                        val playerId = LocalStorageService.getLocalData(applicationContext, Constants.ONESIGNAL_PLAYER_ID_LOCALSTORAGE)
                        params.put(Constants.PLAYER_IDS_ONESIGNAL_KEY, JSONArray().put(playerId))

                        val notifyService = NotificationsService(this)
                        notifyService.postNotification(params)

                        val httpParams = JSONObject()
                        httpParams.put("title", Constants.HRM_TOO_HIGH_NOTIFICATION_TITLE + ": " + hrmValue.toInt())
                        httpParams.put("description", Constants.HRM_TOO_HIGH_NOTIFICATION_DESCRIPTION_PREFIX + ": " + hrmValue.toInt() + ". " + Constants.HRM_TOO_HIGH_NOTIFICATION_DESCRIPTION)
                        httpParams.put("user_id", JSONObject(LocalStorageService.getLocalData(this, Constants.USER_LOCAL_STORAGE)).getString("id"))
                        httpParams.put("type", Constants.SYSTEM_NOTIFICATION_TYPE_KEY)
                        httpParams.put("status", Constants.NOTIFICATION_STATUS_HEALTH)
                        volleyService.post(Constants.SEND_NOTIFICATION_URL, httpParams)

                        isHrmNotificationEnabled = false
                        setHrmNotificationEnabled()
                    }
                }
            }

            if (message.contains("Activity")) {
                val activityType = message.split(",")[1]

                activityActive = activityType
                LocalStorageService.removeLocalData(this, Constants.ACTIVITY_ACTIVE)

                this.activityDurationsTimer.cancel()
                if(isMeasuring) {
                    this.activityDurationsTimer.start()
                }

                // Send durations to watch
                sendDurationsToWatch(this)
            }

            if (message.contains("light")) {
                val lightValue = message.removePrefix("light ")
                LocalStorageService.setLocalData(this, Constants.LIGHT_ACTIVE, lightValue)
                try {
                    measureStartable.updateValues()
                } catch(e: UninitializedPropertyAccessException) {
                    e.printStackTrace()
                }
            }

            var accTimeStamp = 0.000

            if(message.contains("Accelerometer") && userRole.equals(Constants.ADMIN_ROLE_RESPONSE_KEY)) {
                if(!this.isAccFirstCount) {
                    val currentTime = Date()
                    accTimeStamp = ((currentTime.time  - accWatchStartedTime) / 1000.0)
                } else {
                    val date = Date()
                    this.accWatchStartedTime = date.time
                    this.isAccFirstCount = false
                }
                val data = message.removePrefix("Accelerometer: ") + " " + accTimeStamp
                LocalStorageService.setLocalData(this, Constants.WATCH_ACC_ACTIVE, data)
                try {
                    measureStartable.updateValues()
                } catch(e: UninitializedPropertyAccessException) {
                    e.printStackTrace()
                }
                if(FileHelper.isExternalStorageWritable()) {
                    val fileName = "acc_watch_" + LocalStorageService.getLocalData(this, Constants.FILE_NUMBER) +  ".csv"
                    FileHelper.writeDatatoCSV(File(FileHelper.getFilesStorageDir(applicationContext, "watch_docs"), fileName), data)
                }
            }

            var gyrTimeStamp = 0.000
            if(message.contains("Gyroscope") && userRole.equals(Constants.ADMIN_ROLE_RESPONSE_KEY)) {
                if(!this.isGyrFirstCount) {
                    val currentTime = Date()
                    gyrTimeStamp = ((currentTime.time  - gyrWatchStartedTime) / 1000.0)
                } else {
                    val date = Date()
                    this.gyrWatchStartedTime = date.time
                    this.isGyrFirstCount = false
                }
                val data = message.removePrefix("Gyroscope: ") + " " + gyrTimeStamp
                LocalStorageService.setLocalData(this, Constants.WATCH_GYR_ACTIVE, data)
                try {
                    measureStartable.updateValues()
                } catch(e: UninitializedPropertyAccessException) {
                    e.printStackTrace()
                }
                if(FileHelper.isExternalStorageWritable()) {
                    val fileName = "gyr_watch_" + LocalStorageService.getLocalData(this, Constants.FILE_NUMBER) + ".csv"
                    FileHelper.writeDatatoCSV(File(FileHelper.getFilesStorageDir(applicationContext, "watch_docs"), fileName), data)
                }
            }
        }
    }

    override fun onErrorMeasure(error: String) {
    }

    private fun setHrmNotificationEnabled() {
        object : CountDownTimer(Constants.SEND_HRM_NOTIFICATION_TIME, 1000) {
            override fun onTick(millisUntilFinished: Long) {}
            override fun onFinish() {
                isHrmNotificationEnabled = true
            }
        }.start()
    }

    private fun sendDurationsToWatch(context: Context) {
        var stationaryDuration = Constants.STARTED_MINUTES_TIME
        var walkingDuration = Constants.STARTED_MINUTES_TIME
        var runningDuration = Constants.STARTED_MINUTES_TIME
        var inVehicleDuration = Constants.STARTED_MINUTES_TIME

        if(LocalStorageService.checkLocalData(context, Constants.STATIONARY_DURATION)) {
            stationaryDuration = DateUtils.formatElapsedTime(LocalStorageService.getActivityData(context, Constants.STATIONARY_DURATION)).toString()
        }

        if(LocalStorageService.checkLocalData(context, Constants.WALKING_DURATION)) {
            walkingDuration = DateUtils.formatElapsedTime(LocalStorageService.getActivityData(context, Constants.WALKING_DURATION)).toString()
        }

        if(LocalStorageService.checkLocalData(context, Constants.RUNNING_DURATION)) {
            runningDuration = DateUtils.formatElapsedTime(LocalStorageService.getActivityData(context, Constants.RUNNING_DURATION)).toString()
        }

        if(LocalStorageService.checkLocalData(context, Constants.IN_VEHICLE_DURATION)) {
            inVehicleDuration = DateUtils.formatElapsedTime(LocalStorageService.getActivityData(context, Constants.IN_VEHICLE_DURATION)).toString()
        }
        sendData("Stationary " + stationaryDuration + "," +
                "Walking " + walkingDuration + "," +
                "Running " + runningDuration + "," +
                "InVehicle " + inVehicleDuration)
    }

    private fun initActivityDurationsTimer() {
        this.activityDurationsTimer = object : CountDownTimer(1000, 1000) {
            override fun onTick(millisUntilFinished: Long) {}
            override fun onFinish() {
                when (activityActive) {
                    Constants.ACTIVITY_STATIONARY -> {
                        activityDurationMeasuring(Constants.STATIONARY_STARTED_STORAGE, Constants.ACTIVITY_STATIONARY)
                    }
                    Constants.ACTIVITY_WALKING -> {
                        activityDurationMeasuring(Constants.WALKING_STARTED_STORAGE, Constants.ACTIVITY_WALKING)
                    }
                    Constants.ACTIVITY_RUNNING -> {
                        activityDurationMeasuring(Constants.RUNNING_STARTED_STORAGE, Constants.ACTIVITY_RUNNING)
                    }
                    Constants.ACTIVITY_IN_VEHICLE -> {
                        activityDurationMeasuring(Constants.IN_VEHICLE_STARTED_STORAGE, Constants.ACTIVITY_IN_VEHICLE)
                    }
                }
                if(isMeasuring) {
                    activityDurationsTimer.start()
                }
            }
        }
    }

    private fun activityDurationMeasuring(startedKey: String, activityKey: String) {
        if (!LocalStorageService.checkLocalData(this, Constants.ACTIVITY_ACTIVE)) {
            LocalStorageService.setActivityData(this, startedKey, Calendar.getInstance().getTimeInMillis()) // Start Measuring
            LocalStorageService.setLocalData(this, Constants.ACTIVITY_ACTIVE, activityKey)
            LocalStorageService.setLocalData(this, Constants.LAST_ACTIVITY, activityKey) // Set Active Activity
        } else {
            when (LocalStorageService.getLocalData(this, Constants.ACTIVITY_ACTIVE)) {
                Constants.ACTIVITY_WALKING -> {
                    val startMillis = LocalStorageService.getActivityData(this, Constants.WALKING_STARTED_STORAGE)
                    val now = System.currentTimeMillis()
                    val difference = (now - startMillis) / DateUtils.SECOND_IN_MILLIS
                    var activeDuration = LocalStorageService.getActivityData(this, Constants.WALKING_DURATION)
                    activeDuration += difference
                    LocalStorageService.setActivityData(this, Constants.WALKING_DURATION, activeDuration) // Save WALKING Duration
                }
                Constants.ACTIVITY_STATIONARY -> {
                    val startMillis = LocalStorageService.getActivityData(this, Constants.STATIONARY_STARTED_STORAGE)
                    val now = System.currentTimeMillis()
                    val difference = (now - startMillis) / DateUtils.SECOND_IN_MILLIS
                    var activeDuration = LocalStorageService.getActivityData(this, Constants.STATIONARY_DURATION)
                    activeDuration += difference
                    LocalStorageService.setActivityData(this, Constants.STATIONARY_DURATION, activeDuration) // Save Stationary Duration
                }
                Constants.ACTIVITY_RUNNING -> {
                    val startMillis = LocalStorageService.getActivityData(this, Constants.RUNNING_STARTED_STORAGE)
                    val now = System.currentTimeMillis()
                    val difference = (now - startMillis) / DateUtils.SECOND_IN_MILLIS
                    var activeDuration = LocalStorageService.getActivityData(this, Constants.RUNNING_DURATION)
                    activeDuration += difference
                    LocalStorageService.setActivityData(this, Constants.RUNNING_DURATION, activeDuration) // Save RUNNING Duration
                }
                Constants.ACTIVITY_IN_VEHICLE -> {
                    val startMillis = LocalStorageService.getActivityData(this, Constants.IN_VEHICLE_STARTED_STORAGE)
                    val now = System.currentTimeMillis()
                    val difference = (now - startMillis) / DateUtils.SECOND_IN_MILLIS
                    var activeDuration = LocalStorageService.getActivityData(this, Constants.IN_VEHICLE_DURATION)
                    activeDuration += difference
                    LocalStorageService.setActivityData(this, Constants.IN_VEHICLE_DURATION, activeDuration) // Save In Vehicle Duration
                }
            }
            LocalStorageService.setActivityData(this, startedKey, Calendar.getInstance().getTimeInMillis()) // Start Measuring
            try {
                measureStartable.updateValues()
            } catch(e: UninitializedPropertyAccessException) {
                e.printStackTrace()
            }
        }
    }

    override fun onNotificationSent(res: JSONObject?) {
    }

    override fun onNotificationError(err: JSONObject?) {
    }

    override fun successResponse(res: JSONObject) {
        if(res.getString(Constants.MESSAGE_RESPONSE_KEY).equals(Constants.NOTIFICATIONS_COUNT_MESSAGE)) {
            if(res.getInt(Constants.ENTITY_RESPONSE_KEY) > 0) {
                notificationsTab.setBadgeCount(res.getInt(Constants.ENTITY_RESPONSE_KEY))
            } else {
                notificationsTab.removeBadge()
            }
        } else if(res.getJSONObject(Constants.ENTITY_RESPONSE_KEY).has(Constants.MEASURING_TIME_RESPONSE_KEY)) {
            HealthAssistant.startMeasuringTime = res.getJSONObject(Constants.ENTITY_RESPONSE_KEY).getString(Constants.MEASURING_TIME_RESPONSE_KEY).toLong()
            stopWatchSensors()
        }
    }

    override fun errorResponse(err: JSONObject) {
        try {
            if(err.getString(Constants.ERROR_RESPONSE_KEY).equals(Constants.TOKEN_EXPIRED_RESPONSE_KEY)) {
                LocalStorageService.removeLocalData(this, Constants.USER_LOCAL_STORAGE)
                startActivity(Intent(this, LoginActivity::class.java))
                finish()
            }
        } catch(e: JSONException) {
            e.printStackTrace()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if(grantResults.size > 0) {
            if(grantResults.get(0) == PackageManager.PERMISSION_GRANTED) {
                val curLog = CallLogHelper.getAllCallLogs(this.contentResolver)
                LocalStorageService.setLocalData(this, Constants.CALLS_DURATION, CallLogHelper.countCallsDuration(curLog))
            } else {
                Toast.makeText(this, Constants.CALL_LOG_PERMISSION_MESSAGE, Toast.LENGTH_LONG).show()
            }
        } else {
            Toast.makeText(this, Constants.CALL_LOG_PERMISSION_MESSAGE, Toast.LENGTH_LONG).show()
        }
    }
}
