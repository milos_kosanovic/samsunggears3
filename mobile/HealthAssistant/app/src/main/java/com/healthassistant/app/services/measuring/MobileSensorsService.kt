package com.healthassistant.app.services.measuring

import android.app.Activity
import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.util.Log
import com.healthassistant.app.activities.patient.PatientActivity
import com.healthassistant.app.helpers.Constants
import com.healthassistant.app.helpers.FileHelper
import com.healthassistant.app.helpers.LocalStorageService
import java.io.File
import java.util.*

/**
 * Created by darko on 13.3.18..
 */
class MobileSensorsService : SensorEventListener {
    // Hardware sensors
    private lateinit var sensorManager: SensorManager
    private lateinit var gyrSensor: Sensor
    private lateinit var accSensor: Sensor
    private lateinit var context: Context

    private var isAccMobFirstCount: Boolean = true
    private var isGyrMobFirstCount: Boolean = true
    private var accMobStartedTime: Long = 0
    private var gyrMobStartedTime: Long = 0

    fun registerSensors() {
        try {
            this.sensorManager.registerListener(this, gyrSensor, SensorManager.SENSOR_DELAY_GAME)
            this.sensorManager.registerListener(this, accSensor, SensorManager.SENSOR_DELAY_GAME)
        } catch(e: UninitializedPropertyAccessException) {
            e.printStackTrace()
        }
    }

    fun unregisterSensors() {
        try {
            this.sensorManager.unregisterListener(this)
        } catch (e: UninitializedPropertyAccessException) {
            e.printStackTrace()
        }
    }

    fun startMobileSensors(context: Context) {
        this.context = context

        // Hardware sensors
        try {
            this.sensorManager = context.getSystemService(Context.SENSOR_SERVICE) as SensorManager
        } catch (e: UninitializedPropertyAccessException) {
            e.printStackTrace()
        }

        try {
            this.accSensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER)
            this.sensorManager.registerListener(this, accSensor, SensorManager.SENSOR_DELAY_GAME)
        } catch(e: IllegalStateException) {
            e.printStackTrace()
        }

        try {
            this.gyrSensor = sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE)
            this.sensorManager.registerListener(this, gyrSensor, SensorManager.SENSOR_DELAY_GAME)
        } catch(e: IllegalStateException) {
            e.printStackTrace()
        }

        if(!LocalStorageService.checkLocalData(context, Constants.FILE_NUMBER)) {
            LocalStorageService.setLocalData(context, Constants.FILE_NUMBER, "1")
        } else {
            val fileNumber = Integer.parseInt(LocalStorageService.getLocalData(context, Constants.FILE_NUMBER))
            LocalStorageService.setLocalData(context, Constants.FILE_NUMBER, (fileNumber + 1).toString())
        }
    }

    override fun onSensorChanged(sensorEvent: SensorEvent?) {
        var accTimestamp = 0.000
        var gyrTimestamp = 0.000
        val data: String
        val fileName: String

        if(FileHelper.isExternalStorageWritable()) {
            if(sensorEvent!!.sensor.type == Sensor.TYPE_ACCELEROMETER) {
                fileName = "acc_mobile_" + LocalStorageService.getLocalData(this.context, Constants.FILE_NUMBER) + ".csv"

                if(!this.isAccMobFirstCount) {
                    val currentTime = Date()
                    accTimestamp = ((currentTime.time  - accMobStartedTime) / 1000.0)
                } else {
                    val date = Date()
                    this.accMobStartedTime = date.time
                    this.isAccMobFirstCount = false
                }

                data = "" + sensorEvent.values[0] + " " + sensorEvent.values[1] + " " + sensorEvent.values[2] + " " + accTimestamp
                LocalStorageService.setLocalData(this.context, Constants.MOBILE_ACC_ACTIVE, data)
                try {
                    PatientActivity.measureStartable.updateValues()
                } catch(e: UninitializedPropertyAccessException) {
                    e.printStackTrace()
                }
            } else {
                fileName = "gyr_mobile_" + LocalStorageService.getLocalData(this.context, Constants.FILE_NUMBER) + ".csv"
                if(!this.isGyrMobFirstCount) {
                    val currentTime = Date()
                    gyrTimestamp = ((currentTime.time  - gyrMobStartedTime) / 1000.0)
                } else {
                    val date = Date()
                    this.gyrMobStartedTime = date.time
                    this.isGyrMobFirstCount = false
                }

                data = "" + sensorEvent.values[0] + " " + sensorEvent.values[1] + " " + sensorEvent.values[2] + " " + gyrTimestamp
                LocalStorageService.setLocalData(this.context, Constants.MOBILE_GYR_ACTIVE, data)
                try {
                    PatientActivity.measureStartable.updateValues()
                } catch(e: UninitializedPropertyAccessException) {
                    e.printStackTrace()
                }
            }

            FileHelper.writeDatatoCSV(File(FileHelper.getFilesStorageDir(this.context, "mobile_docs"), fileName), data)
        }
    }

    override fun onAccuracyChanged(sensor: Sensor?, p1: Int) {
        Log.i(sensor!!.name, p1.toString())
    }
}