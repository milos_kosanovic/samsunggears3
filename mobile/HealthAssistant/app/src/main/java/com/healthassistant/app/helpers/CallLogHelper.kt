package com.healthassistant.app.helpers

import android.content.ContentResolver
import android.database.Cursor
import android.net.Uri
import android.provider.CallLog
import android.text.format.DateUtils
import android.util.Log

/**
 * Created by darko on 11/13/17.
 */
class CallLogHelper {
    companion object {
        @JvmStatic
        fun getAllCallLogs(cr: ContentResolver): Cursor {
            // reading all data in descending order according to DATE
            val strOrder = android.provider.CallLog.Calls.DATE + " DESC"
            val callUri = Uri.parse("content://call_log/calls")

            return cr.query(callUri, null, null, null, strOrder)
        }

        @JvmStatic
        fun countCallsDuration(curLog: Cursor): String {
            var totalDuration = 0
            val current = System.currentTimeMillis()
            var br = false

            while (curLog.moveToNext() && !br) {
//                val callDate = curLog.getString(curLog.getColumnIndex(android.provider.CallLog.Calls.DATE))
                val timestamp = curLog.getLong(curLog.getColumnIndex("date"))

                if(current - timestamp < Constants.DAY_DURATION_MILISECONDS) {
                    val duration = curLog.getString(curLog.getColumnIndex(android.provider.CallLog.Calls.DURATION))
                    totalDuration += Integer.parseInt(duration)
                }
                else br = true
            }

            return DateUtils.formatElapsedTime(java.lang.Long.parseLong(totalDuration.toString()))
        }

        @JvmStatic
        fun countCalls(cr: ContentResolver): Int {
            val cursor = this.getAllCallLogs(cr)
            var count = 0
            val current = System.currentTimeMillis() / 1000
            var br = false

            while(cursor.moveToNext() && !br) {
//                val callDate = cursor.getString(cursor.getColumnIndex(android.provider.CallLog.Calls.DATE))
                val timestamp = cursor.getLong(cursor.getColumnIndex("date"))

                if(current - timestamp < Constants.DAY_DURATION_MILISECONDS) count++
                else br = true
            }

            cursor.close()

            return count
        }

        @JvmStatic
        fun getUniqueNumbers(cr: ContentResolver): ArrayList<String> {
            val cursor = this.getAllCallLogs(cr)
            val numbers = ArrayList<String>()
            var br = false
            val current = System.currentTimeMillis()

            while(cursor.moveToNext() && !br) {
                val timestamp = cursor.getLong(cursor.getColumnIndex("date"))

                if(current - timestamp < Constants.DAY_DURATION_MILISECONDS) {
                    val num = cursor.getString(cursor.getColumnIndex(CallLog.Calls.NUMBER))
                    if(!numbers.contains(num)) numbers.add(num)
                }
                else br = true
            }

            cursor.close()

            return numbers
        }
    }
}