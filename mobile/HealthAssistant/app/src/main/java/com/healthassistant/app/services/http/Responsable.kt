package com.healthassistant.app.services.http
import com.android.volley.VolleyError
import org.json.JSONObject
/**
 * Created by darko on 10/25/17.
 */
interface Responsable {
    fun successResponse(res: JSONObject)
    fun errorResponse(err: JSONObject)
}