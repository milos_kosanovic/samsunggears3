package com.healthassistant.app.activities.login

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import com.healthassistant.app.HealthAssistant
import com.healthassistant.app.R
import com.healthassistant.app.activities.doctor.DoctorActivity
import com.healthassistant.app.activities.patient.PatientActivity
import com.healthassistant.app.helpers.Constants
import com.healthassistant.app.helpers.LocalStorageService
import com.healthassistant.app.helpers.NetworkConnectionService
import com.healthassistant.app.services.http.Responsable
import com.healthassistant.app.services.http.VolleyService
import org.json.JSONObject
import java.lang.reflect.InvocationTargetException

class SignInFragment : Fragment(), Responsable {
    private lateinit var spinner: ProgressBar
    private lateinit var btnLogin: Button
    private lateinit var txtEmail: TextView
    private lateinit var txtPassword: TextView

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater!!.inflate(R.layout.fragment_sign_in, container, false)
        this.spinner = rootView.findViewById(R.id.pgLogin)
        this.btnLogin = rootView.findViewById(R.id.btnLogin)
        this.txtEmail = rootView.findViewById(R.id.txtEmail)
        this.txtPassword = rootView.findViewById(R.id.txtPassword)

        this.spinner.visibility = View.GONE

        this.btnLogin.setOnClickListener(View.OnClickListener {
            this.spinner.visibility = View.VISIBLE
            if(NetworkConnectionService.ifNetworkAvailable(this@SignInFragment.context)) {
                if(!(this.txtEmail.text.toString().equals("") || this.txtPassword.text.toString().equals(""))) {
                    if(validateEmail(this.txtEmail.text.toString())) {
                        val params = JSONObject()
                        params.put(Constants.EMAIL_PARAMS_KEY, this.txtEmail.text.toString())
                        params.put(Constants.PASSWORD_PARAMS_KEY, this.txtPassword.text.toString())
                        params.put(Constants.PLAYER_ID_PARAMS_KEY, LocalStorageService.getLocalData(this@SignInFragment.context, Constants.ONESIGNAL_PLAYER_ID_LOCALSTORAGE))
                        val vService = VolleyService(this, this.context)
                        vService.post(Constants.LOGIN_API_URL, params)
                    } else {
                        this.spinner.visibility = View.GONE
                        Toast.makeText(this@SignInFragment.context, Constants.EMAIL_NOT_VALID_MESSAGE, Toast.LENGTH_SHORT).show()
                    }
                } else {
                    this.spinner.visibility = View.GONE
                    if(txtEmail.text.toString().equals("")) {
                        Toast.makeText(this@SignInFragment.context, Constants.NO_EMAIL_MESSAGE, Toast.LENGTH_SHORT).show()
                    } else {
                        Toast.makeText(this@SignInFragment.context, Constants.NO_PASSWORD_MESSAGE, Toast.LENGTH_SHORT).show()
                    }
                }
            } else {
                this.spinner.visibility = View.GONE
                Toast.makeText(this@SignInFragment.context, Constants.NO_NETWORK_CONNECTION_MESSAGE, Toast.LENGTH_SHORT).show()
            }
        })

        return rootView
    }

    override fun successResponse(res: JSONObject) {
        this.spinner.visibility = View.GONE
        if(JSONObject(res.getString(Constants.ENTITY_RESPONSE_KEY)).getString(Constants.ROLE_RESPONSE_KEY).equals(Constants.PATIENT_ROLE_RESPONSE_KEY) || JSONObject(res.getString(Constants.ENTITY_RESPONSE_KEY)).getString(Constants.ROLE_RESPONSE_KEY).equals(Constants.ADMIN_ROLE_RESPONSE_KEY)) {
            Toast.makeText(this@SignInFragment.context, res.getString(Constants.MESSAGE_RESPONSE_KEY), Toast.LENGTH_SHORT).show()
            LocalStorageService.setLocalData(this@SignInFragment.context, Constants.USER_LOCAL_STORAGE, res.getString(Constants.ENTITY_RESPONSE_KEY))
            HealthAssistant.userRole = JSONObject(LocalStorageService.getLocalData(this@SignInFragment.context, Constants.USER_LOCAL_STORAGE)).getString(Constants.ROLE_PARAMS_KEY)
            startActivity(Intent(this@SignInFragment.context, PatientActivity::class.java))
            this@SignInFragment.activity.finish()
        } else {
            Toast.makeText(this@SignInFragment.context, Constants.DOCTOR_LOGGED_IN_MESSAGE, Toast.LENGTH_SHORT).show()
        }
    }

    override fun errorResponse(err: JSONObject) {
        this.spinner.visibility = View.GONE
        Log.i("ERROR", err.toString())
        try {
            Toast.makeText(this@SignInFragment.context, err.getString(Constants.MESSAGE_RESPONSE_KEY), Toast.LENGTH_SHORT).show()
        } catch (e: InvocationTargetException) {
            e.printStackTrace()
        }
    }

    fun validateEmail(emailStr: String): Boolean {
        val matcher = Constants.VALID_EMAIL_ADDRESS_REGEX.matcher(emailStr)
        return matcher.find()
    }
}
