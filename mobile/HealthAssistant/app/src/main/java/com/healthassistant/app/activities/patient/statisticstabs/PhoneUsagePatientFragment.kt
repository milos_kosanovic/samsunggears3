package com.healthassistant.app.activities.patient.statisticstabs

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ExpandableListView
import android.widget.Toast

import com.healthassistant.app.R
import com.healthassistant.app.activities.login.LoginActivity
import com.healthassistant.app.adapters.StatisticExpListAdapter
import com.healthassistant.app.entities.StatisticItem
import com.healthassistant.app.helpers.Constants
import com.healthassistant.app.helpers.LocalStorageService
import com.healthassistant.app.services.http.Responsable
import com.healthassistant.app.services.http.VolleyService
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject

class PhoneUsagePatientFragment : Fragment(), Responsable {
    lateinit var phoneExpListView: ExpandableListView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater!!.inflate(R.layout.fragment_phone_usage_patient, container, false)

        phoneExpListView = view.findViewById(R.id.statisticPhoneList)

        val volleyService = VolleyService(this, this@PhoneUsagePatientFragment.context)
        volleyService.get(Constants.GET_STATISTIC_URL, null)

        return view
    }

    override fun successResponse(res: JSONObject) {
        val statisticJsonArray = JSONArray(res.getString(Constants.ENTITY_RESPONSE_KEY))
        val statisticList = ArrayList<StatisticItem>()
        val valuesList = HashMap<StatisticItem, ArrayList<StatisticItem>>()

        for(i in 0..(statisticJsonArray.length() - 1)) {
            val newStatisticItem = StatisticItem(R.drawable.phone_log_icon)

            newStatisticItem.date = JSONObject(statisticJsonArray.get(i).toString()).getString("created_at")
            var isDateExist = false
            for(j in 0 .. (statisticList.size - 1)) {
                if(statisticList.get(j).date.equals(newStatisticItem.date)) {
                    isDateExist = true
                }
            }
            if(!isDateExist && !JSONObject(statisticJsonArray.get(i).toString()).getString("phonetime").equals("")) {
                statisticList.add(newStatisticItem)
            }
        }

        for(i in 0 .. (statisticList.size - 1)) {
            val subItem = ArrayList<StatisticItem>()
            for(j in 0 .. (statisticJsonArray.length() - 1)) {
                if(JSONObject(statisticJsonArray.get(j).toString()).getString("created_at").equals(statisticList.get(i).date)) {
                    val newValue = StatisticItem(R.drawable.phone_log_icon)
                    newValue.value = JSONObject(statisticJsonArray.get(j).toString()).getString("phonetime") + Constants.MIN_TXT
                    newValue.time = JSONObject(statisticJsonArray.get(j).toString()).getString("time")
                    if(!newValue.value.equals("" + Constants.MIN_TXT)) {
                        subItem.add(newValue)
                    }
                }
            }
            valuesList.put(statisticList.get(i), subItem)
        }

        try {
            val adapter = StatisticExpListAdapter(this@PhoneUsagePatientFragment.context, statisticList, valuesList)
            phoneExpListView.setAdapter(adapter)
        } catch(e: IllegalStateException) {
            e.printStackTrace()
        }
    }

    override fun errorResponse(err: JSONObject) {
        try {
            if(err.getString(Constants.ERROR_RESPONSE_KEY).equals(Constants.TOKEN_EXPIRED_RESPONSE_KEY)) {
                LocalStorageService.removeLocalData(this@PhoneUsagePatientFragment.context, Constants.USER_LOCAL_STORAGE)
                startActivity(Intent(this@PhoneUsagePatientFragment.context, LoginActivity::class.java))
                this@PhoneUsagePatientFragment.activity.finish()
            }
        } catch(e: JSONException) {
            e.printStackTrace()
            try {
                Toast.makeText(this@PhoneUsagePatientFragment.context, Constants.NO_SERVER_MESSAGE, Toast.LENGTH_SHORT).show()
            } catch (e: NullPointerException) {
                e.printStackTrace()
            }
        }
    }
}
