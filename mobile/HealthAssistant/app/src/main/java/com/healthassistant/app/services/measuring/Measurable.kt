package com.healthassistant.app.services.measuring

/**
 * Created by darko on 10/31/17.
 */
interface Measurable {
    fun onSuccessMeasure(values: String)
    fun onErrorMeasure(error: String)
    fun onPeerAgentNotConnected()
    fun onPeerAgentConnectionSuccess()
    fun onPeerAgentConnectionLost()
    fun onStartMeasuring()
    fun onErrorMeasuring()
    fun onNotConnected()
}