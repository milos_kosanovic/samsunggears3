package com.healthassistant.app.services.notifications

import android.content.Context
import android.os.Handler
import com.onesignal.OneSignal
import org.json.JSONObject

/**
 * Created by darko on 11/10/17.
 */
class NotificationsService(val notifyCallback: Notifable) {
    internal var mHandler = Handler()

    fun postNotification(params: JSONObject) {
        OneSignal.postNotification(params,
                object: OneSignal.PostNotificationResponseHandler {
                    override fun onSuccess(response: JSONObject?) {
                        mHandler.post(Runnable {
                            notifyCallback.onNotificationSent(response)
                        })
                    }

                    override fun onFailure(error: JSONObject?) {
                        mHandler.post(Runnable {
                            notifyCallback.onNotificationError(error)
                        })
                    }

                })
    }
}