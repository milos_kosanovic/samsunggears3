package com.healthassistant.app.services.measuring

/**
 * Created by darko on 11/10/17.
 */
interface MeasureStartable {
    fun updateValues()
    fun onStartMeasuring()
    fun onStopMeasuring()
}