package com.healthassistant.app.helpers

import android.content.ContentResolver
import android.database.Cursor
import android.net.Uri
import com.healthassistant.app.entities.Sms

class SmsHelper {
    companion object {
        @JvmStatic
        fun getSmsCursor(cr: ContentResolver, folderName: String): Cursor {
            // reading all data in descending order according to DATE
            val strOrder = android.provider.Telephony.Sms.DATE + " DESC"
            val smsUri = Uri.parse("content://sms/$folderName")

            return cr.query(smsUri, null, null, null, strOrder)
        }

        @JvmStatic
        fun getPastDaySms(cr: ContentResolver, folderName: String): ArrayList<Sms> {
            val cursor = this.getSmsCursor(cr, folderName)
            val messages = ArrayList<Sms>()
            val current = System.currentTimeMillis()
            // used for brake condition
            var br = false

            while(cursor.moveToNext() && !br) {
                val timestamp = cursor.getLong(cursor.getColumnIndex("date"))

                if(current - timestamp < Constants.DAY_DURATION_MILISECONDS) {
                    val body = cursor.getString(cursor.getColumnIndex("body"))
                    val address = cursor.getString(cursor.getColumnIndex("address"))
                    val sms = Sms(body, address)
                    messages.add(sms)
                }
                else br = true
            }

            cursor.close()

            return messages
        }

        //returns list of unique contacts
        @JvmStatic
        fun getUniqueContactSms(cr:ContentResolver): ArrayList<String> {
            var cursor = this.getSmsCursor(cr, Constants.SMS_INBOX)
            val current = System.currentTimeMillis()
            var br = false
            val people = ArrayList<String>()

            while(cursor.moveToNext() && !br) {
                val timestamp = cursor.getLong(cursor.getColumnIndex("date"))

                if(current - timestamp < Constants.DAY_DURATION_MILISECONDS) {
                    val address = cursor.getString(cursor.getColumnIndex("address"))
                    if(!people.contains(address)) people.add(address)
                }
                else br = true
            }
            cursor.close()

            cursor = this.getSmsCursor(cr, Constants.SMS_SENT)
            br = false

            while(cursor.moveToNext() && !br) {
                val timestamp = cursor.getLong(cursor.getColumnIndex("date"))

                if(current - timestamp < Constants.DAY_DURATION_MILISECONDS) {
                    val address = cursor.getString(cursor.getColumnIndex("address"))
                    if(!people.contains(address)) people.add(address)
                }
                else br = true
            }
            cursor.close()

            return people
        }
    }
}