package com.healthassistant.app.helpers

import android.content.Context
import android.content.SharedPreferences

/**
 * Created by darko on 10/25/17.
 */
class LocalStorageService {
    companion object {
        // User Local Storage
        fun setLocalData(context: Context, key: String, data: String) {
            val dataSharedPreference: SharedPreferences = context.getSharedPreferences(Constants.LOCAL_DATA, Context.MODE_PRIVATE)
            val edit: SharedPreferences.Editor = dataSharedPreference.edit()
            edit.putString(key, data)
            edit.apply()
        }

        fun getLocalData(context: Context, key: String): String {
            val dataSharedPreference: SharedPreferences = context.getSharedPreferences(Constants.LOCAL_DATA, Context.MODE_PRIVATE)
            return dataSharedPreference.getString(key, "")
        }

        // Activity Recognation Data
        fun setActivityData(context: Context, key: String, data: Long) {
            val dataSharedPreference: SharedPreferences = context.getSharedPreferences(Constants.LOCAL_DATA, Context.MODE_PRIVATE)
            val edit: SharedPreferences.Editor = dataSharedPreference.edit()
            edit.putLong(key, data)
            edit.apply()
        }

        fun getActivityData(context: Context, key: String): Long {
            val dataSharedPreference: SharedPreferences = context.getSharedPreferences(Constants.LOCAL_DATA, Context.MODE_PRIVATE)
            return dataSharedPreference.getLong(key, 0L)
        }

        fun removeLocalData(context: Context, key: String) {
            val dataSharedPreference: SharedPreferences = context.getSharedPreferences(Constants.LOCAL_DATA, Context.MODE_PRIVATE)
            val edit: SharedPreferences.Editor = dataSharedPreference.edit()
            edit.remove(key)
            edit.apply()
        }

        // activities duration time
        fun setActivityDurationsData(context: Context, key: String, data: Long) {
            val dataSharedPreference: SharedPreferences = context.getSharedPreferences(Constants.LOCAL_DATA, Context.MODE_PRIVATE)
            val edit: SharedPreferences.Editor = dataSharedPreference.edit()
            edit.putLong(key, data)
            edit.apply()
        }

        fun getActivityDurationsData(context: Context, key: String): Long {
            val dataSharedPreference: SharedPreferences = context.getSharedPreferences(Constants.LOCAL_DATA, Context.MODE_PRIVATE)
            return dataSharedPreference.getLong(key, 0L)
        }

        fun checkLocalData(context: Context, key: String): Boolean {
            val dataSharedPreference: SharedPreferences = context.getSharedPreferences(Constants.LOCAL_DATA, Context.MODE_PRIVATE)
            if(dataSharedPreference.contains(key)) {
                return true
            }
            return false
        }
    }
}