package com.healthassistant.app.activities.global

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView

import com.healthassistant.app.R
import com.healthassistant.app.activities.login.LoginActivity
import com.healthassistant.app.helpers.Constants
import com.healthassistant.app.helpers.LocalStorageService
import kotlinx.android.synthetic.main.fragment_profile.*
import org.json.JSONObject

class ProfileFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val rootView = inflater!!.inflate(R.layout.fragment_profile, container, false)

        val txtUserName = rootView.findViewById<TextView>(R.id.txtUserName)
        val btnLogout = rootView.findViewById<Button>(R.id.btnLogout)

        val userData = JSONObject(LocalStorageService.getLocalData(this@ProfileFragment.context, Constants.USER_LOCAL_STORAGE))
        txtUserName.text = userData.getString(Constants.USER_NAME_RESPONSE_KEY) + " " + userData.getString(Constants.USER_LASTNAME_RESPONSE_KEY)

        btnLogout.setOnClickListener {
            LocalStorageService.removeLocalData(this@ProfileFragment.context, Constants.USER_LOCAL_STORAGE)
            startActivity(Intent(this@ProfileFragment.context, LoginActivity::class.java))
            this@ProfileFragment.activity.finish()
        }

        return rootView
    }

}
