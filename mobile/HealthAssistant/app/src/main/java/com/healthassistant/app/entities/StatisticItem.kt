package com.healthassistant.app.entities

/**
 * Created by darko on 11/18/17.
 */
class StatisticItem(val icon: Int) {
    lateinit var value: String
    lateinit var date: String
    lateinit var time: String
}