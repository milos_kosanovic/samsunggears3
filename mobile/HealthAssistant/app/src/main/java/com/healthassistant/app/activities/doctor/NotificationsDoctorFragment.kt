package com.healthassistant.app.activities.doctor

import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast

import com.healthassistant.app.R
import com.healthassistant.app.helpers.Constants
import com.healthassistant.app.services.notifications.Notifable
import com.healthassistant.app.services.notifications.NotificationsService
import org.json.JSONArray
import org.json.JSONObject




class NotificationsDoctorFragment : Fragment(), Notifable {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater!!.inflate(R.layout.fragment_notifications_doctor, container, false)

        val txtNotification = rootView.findViewById<EditText>(R.id.txtNotificationText)
        val btnSend = rootView.findViewById<Button>(R.id.btnSendNotification)

        btnSend.setOnClickListener {
            val params = JSONObject()
            val contents = JSONObject()
            contents.put(Constants.LANGUAGE_ONESIGNAL_KEY, txtNotification.text)
            params.put(Constants.CONTENTS_ONESIGNAL_KEY, contents)
            val playersId = JSONArray()
            playersId.put("23e519a9-3bf4-4a80-afcf-5c2e267afd14")
            params.put(Constants.PLAYER_IDS_ONESIGNAL_KEY, playersId)
            val notifyService = NotificationsService(this)
            notifyService.postNotification(params)
        }

        return rootView
    }

    override fun onNotificationSent(res: JSONObject?) {
        Toast.makeText(this@NotificationsDoctorFragment.context, "Notification sent", Toast.LENGTH_SHORT).show()
    }

    override fun onNotificationError(err: JSONObject?) {
        Toast.makeText(this@NotificationsDoctorFragment.context, "Notification sending error", Toast.LENGTH_SHORT).show()
    }
}
