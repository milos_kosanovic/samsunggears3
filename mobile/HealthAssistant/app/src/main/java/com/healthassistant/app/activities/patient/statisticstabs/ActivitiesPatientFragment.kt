package com.healthassistant.app.activities.patient.statisticstabs

import android.graphics.Color
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.github.mikephil.charting.charts.PieChart

import com.healthassistant.app.R
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.data.PieEntry
import com.github.mikephil.charting.formatter.PercentFormatter
import com.healthassistant.app.helpers.Constants
import com.healthassistant.app.helpers.LocalStorageService


class ActivitiesPatientFragment : Fragment() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater!!.inflate(R.layout.fragment_activities_patient, container, false)

        var activitiesChart = view.findViewById<PieChart>(R.id.activitiesChart)

        activitiesChart.getDescription().setEnabled(false)
        activitiesChart.setUsePercentValues(true)

        activitiesChart.setCenterText("")
        activitiesChart.setCenterTextSize(10f)

        // radius of the center hole in percent of maximum radius
        activitiesChart.setHoleRadius(0f)
        activitiesChart.setTransparentCircleRadius(0f)

        val l = activitiesChart.getLegend()
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP)
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.RIGHT)
        l.setOrientation(Legend.LegendOrientation.VERTICAL)
        l.setDrawInside(false)
        l.textColor = resources.getColor(R.color.notificationsTabsSelectedColor)
        l.textSize = 10f

        activitiesChart.setData(generatePieData())

        return view
    }

    private fun generatePieData(): PieData {

        val entries = ArrayList<PieEntry>()

        var total = 0L // 130
        var stationaryDuration = 0L // 300
        var walkingDuration = 0L // 50
        var runningDuration = 0L // 20
        var inVehicleDuration = 0L // 30

        if(LocalStorageService.checkLocalData(this@ActivitiesPatientFragment.context, Constants.STATIONARY_DURATION)) {
            total += LocalStorageService.getActivityData(this@ActivitiesPatientFragment.context, Constants.STATIONARY_DURATION)
            stationaryDuration = LocalStorageService.getActivityData(this@ActivitiesPatientFragment.context, Constants.STATIONARY_DURATION)
        }
        if(LocalStorageService.checkLocalData(this@ActivitiesPatientFragment.context, Constants.WALKING_DURATION)) {
            total += LocalStorageService.getActivityData(this@ActivitiesPatientFragment.context, Constants.WALKING_DURATION)
            walkingDuration = LocalStorageService.getActivityData(this@ActivitiesPatientFragment.context, Constants.WALKING_DURATION)
        }
        if(LocalStorageService.checkLocalData(this@ActivitiesPatientFragment.context, Constants.RUNNING_DURATION)) {
            total += LocalStorageService.getActivityData(this@ActivitiesPatientFragment.context, Constants.RUNNING_DURATION)
            runningDuration = LocalStorageService.getActivityData(this@ActivitiesPatientFragment.context, Constants.RUNNING_DURATION)
        }
        if(LocalStorageService.checkLocalData(this@ActivitiesPatientFragment.context, Constants.IN_VEHICLE_DURATION)) {
            total += LocalStorageService.getActivityData(this@ActivitiesPatientFragment.context, Constants.IN_VEHICLE_DURATION)
            inVehicleDuration = LocalStorageService.getActivityData(this@ActivitiesPatientFragment.context, Constants.IN_VEHICLE_DURATION)
        }

        val totalInt = total.toFloat()
        val onePercentage = (totalInt / 100)

        val stationaryPercentage = stationaryDuration / onePercentage
        val walkingPercentage = walkingDuration / onePercentage
        val runningPercentage = runningDuration / onePercentage
        val inVehiclePercentage = inVehicleDuration / onePercentage

        if(stationaryPercentage > 0) {
            entries.add(PieEntry(stationaryPercentage, getString(R.string.home_stationary_label)))
        }
        if(walkingPercentage > 0) {
            entries.add(PieEntry(walkingPercentage, getString(R.string.home_walking_label)))
        }
        if(runningPercentage > 0) {
            entries.add(PieEntry(runningPercentage, getString(R.string.home_running_label)))
        }
        if(inVehiclePercentage > 0) {
            entries.add(PieEntry(inVehiclePercentage, getString(R.string.home_in_vehicle_label)))
        }

        val dataSet = PieDataSet(entries, "")
        val dataColors = ArrayList<Int>()
        dataColors.add(resources.getColor(R.color.graphStationary))
        dataColors.add(resources.getColor(R.color.graphWalking))
        dataColors.add(resources.getColor(R.color.graphRunning))
        dataColors.add(resources.getColor(R.color.graphInVehicle))
        dataSet.setColors(dataColors)
        dataSet.sliceSpace = 2f
        dataSet.valueTextColor = Color.WHITE
        dataSet.valueTextSize = 12f
        dataSet.setValueFormatter(PercentFormatter())

        val d = PieData(dataSet)

        return d
    }
}
