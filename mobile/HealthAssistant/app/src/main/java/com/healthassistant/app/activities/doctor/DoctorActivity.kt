package com.healthassistant.app.activities.doctor

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.healthassistant.app.R
import com.healthassistant.app.activities.global.ProfileFragment
import com.healthassistant.app.activities.patient.StatisticPatientFragment
import com.jaeger.library.StatusBarUtil
import kotlinx.android.synthetic.main.activity_doctor.*

class DoctorActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_doctor)

        StatusBarUtil.setColor(this@DoctorActivity, resources.getColor(R.color.statusBarPrimary))

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.contentContainer, HomeDoctorFragment())
                .commit()

        bottomBar.setOnTabSelectListener { tabId ->
            when(tabId) {
                R.id.tab_home -> {
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.contentContainer, HomeDoctorFragment())
                            .commit()
                }
                R.id.tab_measuring -> {
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.contentContainer, MeasuringDoctorFragment())
                            .commit()
                }
                R.id.tab_notifications -> {
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.contentContainer, NotificationsDoctorFragment())
                            .commit()
                }
                R.id.tab_statistic -> {
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.contentContainer, StatisticPatientFragment())
                            .commit()
                }
                R.id.tab_profile -> {
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.contentContainer, ProfileFragment())
                            .commit()
                }
            }
        }
    }
}
