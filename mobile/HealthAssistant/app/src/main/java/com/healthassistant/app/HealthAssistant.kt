package com.healthassistant.app

import android.app.Application
import android.util.Log
import com.healthassistant.app.helpers.Constants
import com.healthassistant.app.helpers.LocalStorageService
import com.onesignal.OneSignal


/**
 * Created by darko on 11/8/17.
 */
class HealthAssistant : Application() {
    companion object {
        var cbPulseChecked = false
        var cbActivityChecked = false
        var cbLightChecked = false
        var cbPhoneChecked = false
        var userRole = ""
        var startMeasuringTime: Long = 1800000
    }

    override fun onCreate() {
        super.onCreate()

        OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .unsubscribeWhenNotificationsAreDisabled(true)
                .init()

        OneSignal.idsAvailable { userId, registrationId ->
            run {
                Log.i("ONESIGNAL_PLAYERID", userId)
                LocalStorageService.setLocalData(applicationContext, Constants.ONESIGNAL_PLAYER_ID_LOCALSTORAGE, userId)
            }
        }
    }
}