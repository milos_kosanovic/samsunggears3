package com.healthassistant.app.helpers

import android.app.AppOpsManager
import android.app.usage.UsageStats
import android.app.usage.UsageStatsManager
import android.content.Context
import android.content.pm.ApplicationInfo

class UsagePresenter {
    companion object {

        private val tracedApps = arrayListOf(
                "com.instagram.android", //instagram
                "com.facebook.orca", //messenger
                "com.facebook.katana" //facebook
        )

        private fun getUsageStats(context: Context): List<UsageStats> {
            val usageStatsManager: UsageStatsManager =
                    context.getSystemService(Context.USAGE_STATS_SERVICE) as UsageStatsManager
            val currentTime = System.currentTimeMillis()
            val yesterdayTime = currentTime - Constants.DAY_DURATION_MILISECONDS
            return usageStatsManager.queryUsageStats(UsageStatsManager.INTERVAL_DAILY, yesterdayTime, currentTime)
        }

        @JvmStatic
        fun getTimeSpentOnPhone(context: Context): Long {
            var time: Long = 0
            val usageStats = this.getUsageStats(context)
            usageStats.forEach{
                time += it.totalTimeInForeground
            }

            return time
        }

        fun getTimeOnTracedApps(context: Context): Long {
            var time: Long = 0
            val usageStats = this.getUsageStats(context)
            usageStats.forEach {
                if(tracedApps.contains(it.packageName))
                    time += it.totalTimeInForeground
            }

            return time
        }



        @JvmStatic
        fun getInstalledAppList(context: Context):ArrayList<String> {
            val packageManager = context.packageManager
            val infos = packageManager.getInstalledApplications(0)
            val installedApps = ArrayList<String>()
            infos.forEach {
                installedApps.add(it.packageName)
            }

            return installedApps
        }
    }

//    private fun checkForPermission(context: Context): Boolean {
//        val appOps: AppOpsManager = context.getSystemService(Context.APP_OPS_SERVICE) as AppOpsManager
//        val mode = appOps.checkOpNoThrow(OPSTR_GET_USAGE_STATS)
//    }
}