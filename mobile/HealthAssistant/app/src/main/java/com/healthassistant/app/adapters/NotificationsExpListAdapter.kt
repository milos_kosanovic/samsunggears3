package com.healthassistant.app.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseExpandableListAdapter
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.healthassistant.app.R
import com.healthassistant.app.entities.Notification
import com.healthassistant.app.helpers.Constants
import com.healthassistant.app.services.http.Responsable
import com.healthassistant.app.services.http.VolleyService
import org.json.JSONObject


/**
 * Created by darko on 11/16/17.
 */
class NotificationsExpListAdapter(val responsable: Responsable, val context: Context, val itemsList: List<Notification>, val subItemsList: HashMap<Notification, ArrayList<Notification>>) : BaseExpandableListAdapter() {



    override fun getGroup(position: Int): Any {
        return this.itemsList.get(position)
    }

    override fun isChildSelectable(p0: Int, p1: Int): Boolean {
        return false
    }

    override fun hasStableIds(): Boolean {
        return false
    }

    override fun getGroupView(groupPosition: Int, isExpanded: Boolean, convertView: View?, parent: ViewGroup): View {
        val inflater = this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.notification_item, null)

        val notificationItem = getGroup(groupPosition) as Notification

        val notificationTitle = view.findViewById<TextView>(R.id.txtNotificationTitle)
        val expandButton = view.findViewById<ImageView>(R.id.expandButton)
        val notificationTime = view.findViewById<TextView>(R.id.txtNotificationAgo)
        val groupLayout = view.findViewById<LinearLayout>(R.id.notificationsGroupLayout)
        val notificationStatusImage = view.findViewById<ImageView>(R.id.notificationStatusImage)

        if(isExpanded) {
            expandButton.setImageResource(R.drawable.up_arrow)
            notificationTime.visibility = View.GONE
            if(notificationItem.isRead == 0) {
                val volleyService = VolleyService(responsable, context)
                val params = JSONObject()
                params.put(Constants.READ_PARAMS_KEY, 1)
                params.put(Constants.ID_PARAMS_KEY, notificationItem.id)
                volleyService.post(Constants.READ_NOTIFICATION_URL, params)
            }
        } else {
            expandButton.setImageResource(R.drawable.down_arrow)
            notificationTime.visibility = View.VISIBLE
        }

        notificationTitle.text = notificationItem.title
        notificationTime.text = notificationItem.date

        if(notificationItem.status.equals(Constants.NOTIFICATION_STATUS_HEALTH)) {
            notificationStatusImage.setImageResource(R.drawable.notifications_pulse_icon)
        } else {
            notificationStatusImage.setImageResource(R.drawable.notifications_activity_icon)
        }

        if(notificationItem.isRead == 0) {
            groupLayout.setBackgroundColor(context.resources.getColor(R.color.notificationUnreadBackground))
        } else {
            groupLayout.setBackgroundColor(context.resources.getColor(R.color.notificationReadBackground))
        }

        return view
    }

    override fun getChildrenCount(itemPosition: Int): Int {
        return this.subItemsList.get(this.itemsList.get(itemPosition))!!.size
    }

    override fun getChild(itemPosition: Int, subItemPosition: Int): Any {
        return this.subItemsList.get(this.itemsList.get(itemPosition))!!.get(subItemPosition)
    }

    override fun getGroupId(groupPosition: Int): Long {
        return groupPosition.toLong()
    }

    override fun getChildView(groupPosition: Int, childPosition: Int, isLastChild: Boolean, convertView: View?, parent: ViewGroup): View {
        val inflater = this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.notification_subitem, null)

        val subItem = getChild(groupPosition, childPosition) as Notification

        val txtNotificationDetails = view.findViewById<TextView>(R.id.txtNotificationDetails)
        val notificationTime = view.findViewById<TextView>(R.id.txtNotificationAgo2)
        val subItemLayout = view.findViewById<LinearLayout>(R.id.notificationsSubItemLayout)

        txtNotificationDetails.text = subItem.description
        notificationTime.text = subItem.date

        if(subItem.isRead == 0) {
            subItemLayout.setBackgroundColor(context.resources.getColor(R.color.notificationUnreadBackground))
        } else {
            subItemLayout.setBackgroundColor(context.resources.getColor(R.color.notificationReadBackground))
        }

        return view
    }

    override fun getChildId(itemPosition: Int, subitemPosition: Int): Long {
        return subitemPosition.toLong()
    }

    override fun getGroupCount(): Int {
        return this.itemsList.size
    }
}