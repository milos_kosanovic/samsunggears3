package com.healthassistant.app.entities

/**
 * Created by darko on 11/16/17.
 */
class Notification(val id: Int, val title: String, val description: String, val user_id: Int, val status: String, val isRead: Int, val date: String) {

}