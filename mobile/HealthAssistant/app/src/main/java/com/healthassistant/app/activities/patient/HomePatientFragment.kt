package com.healthassistant.app.activities.patient

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.CardView
import android.text.format.DateUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.healthassistant.app.HealthAssistant

import com.healthassistant.app.R
import com.healthassistant.app.helpers.Constants
import com.healthassistant.app.helpers.LocalStorageService
import com.healthassistant.app.services.measuring.LiveUpdateable
import kotlinx.android.synthetic.main.fragment_home_patient.*

class HomePatientFragment : Fragment(), LiveUpdateable {
    private lateinit var txtCurrentHrm: TextView
    private lateinit var txtCurrentLight: TextView
    private lateinit var txtCurrentActivity: TextView
    private lateinit var txtStationaryDuration: TextView
    private lateinit var txtWalkingDuration: TextView
    private lateinit var txtRunningDuration: TextView
    private lateinit var txtInVehicleDuration: TextView
    private lateinit var txtCallsDuration: TextView
    private lateinit var txtWatchGyr: TextView
    private lateinit var txtWatchAcc: TextView
    private lateinit var txtMobileAcc: TextView
    private lateinit var txtMobileGyr: TextView
    private lateinit var cardWatchGyr: CardView
    private lateinit var cardWatchAcc: CardView
    private lateinit var cardMobGyr: CardView
    private lateinit var cardMobAcc: CardView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val rootView = inflater!!.inflate(R.layout.fragment_home_patient, container, false)

        MeasuringPatientFragment.liveUpdateable = this
        txtCurrentHrm = rootView.findViewById(R.id.txtCurrentPulse)
        txtCurrentActivity = rootView.findViewById(R.id.txtCurrentActivity)
        txtCurrentLight = rootView.findViewById(R.id.txtCurrentLight)
        txtStationaryDuration = rootView.findViewById(R.id.txtStationaryDuration)
        txtWalkingDuration = rootView.findViewById(R.id.txtWalkingDuration)
        txtRunningDuration = rootView.findViewById(R.id.txtRunningDuration)
        txtInVehicleDuration = rootView.findViewById(R.id.txtInVehicleDuration)
        txtCallsDuration = rootView.findViewById(R.id.txtCallsDuration)
        txtWatchAcc = rootView.findViewById(R.id.txtAccelerometerWatch)
        txtWatchGyr = rootView.findViewById(R.id.txtGyroscopWatch)
        txtMobileAcc = rootView.findViewById(R.id.txtAccelerometerMobile)
        txtMobileGyr = rootView.findViewById(R.id.txtGyroscopMobile)
        cardWatchGyr = rootView.findViewById(R.id.cardWatchGyr)
        cardWatchAcc = rootView.findViewById(R.id.cardWatchAcc)
        cardMobGyr = rootView.findViewById(R.id.cardMobGyr)
        cardMobAcc = rootView.findViewById(R.id.cardMobAcc)

        if(!HealthAssistant.userRole.equals(Constants.ADMIN_ROLE_RESPONSE_KEY)) {
            cardWatchGyr.visibility = View.GONE
            cardWatchAcc.visibility = View.GONE
            cardMobGyr.visibility = View.GONE
            cardMobAcc.visibility = View.GONE
        }

        updateValues()

        return rootView
    }

    override fun onValuesUpdated() {
        updateValues()
    }

    private fun updateValues() {
        if(this@HomePatientFragment.context != null) {
            if(LocalStorageService.checkLocalData(this@HomePatientFragment.context, Constants.HRM_ACTIVE)) {
                txtCurrentHrm.text = LocalStorageService.getLocalData(this@HomePatientFragment.context, Constants.HRM_ACTIVE)
            }

            if(LocalStorageService.checkLocalData(this@HomePatientFragment.context, Constants.LIGHT_ACTIVE)) {
                txtCurrentLight.text = LocalStorageService.getLocalData(this@HomePatientFragment.context, Constants.LIGHT_ACTIVE) + Constants.LUX_TXT
            }

            if(LocalStorageService.checkLocalData(this@HomePatientFragment.context, Constants.LAST_ACTIVITY)) {
                when(LocalStorageService.getLocalData(this@HomePatientFragment.context, Constants.LAST_ACTIVITY)) {
                    Constants.ACTIVITY_STATIONARY -> {
                        txtCurrentActivity.text = getString(R.string.home_stationary_label)
                    }
                    Constants.ACTIVITY_WALKING -> {
                        txtCurrentActivity.text = getString(R.string.home_walking_label)
                    }
                    Constants.ACTIVITY_RUNNING -> {
                        txtCurrentActivity.text = getString(R.string.home_running_label)
                    }
                    Constants.ACTIVITY_IN_VEHICLE -> {
                        txtCurrentActivity.text = getString(R.string.home_in_vehicle_label)
                    }
                }
            }

            if(LocalStorageService.checkLocalData(this@HomePatientFragment.context, Constants.STATIONARY_DURATION)) {
                txtStationaryDuration.text = DateUtils.formatElapsedTime(LocalStorageService.getActivityData(this@HomePatientFragment.context, Constants.STATIONARY_DURATION)).toString() + Constants.MIN_TXT
            }

            if(LocalStorageService.checkLocalData(this@HomePatientFragment.context, Constants.WALKING_DURATION)) {
                txtWalkingDuration.text = DateUtils.formatElapsedTime(LocalStorageService.getActivityData(this@HomePatientFragment.context, Constants.WALKING_DURATION)).toString() + Constants.MIN_TXT
            }

            if(LocalStorageService.checkLocalData(this@HomePatientFragment.context, Constants.RUNNING_DURATION)) {
                txtRunningDuration.text = DateUtils.formatElapsedTime(LocalStorageService.getActivityData(this@HomePatientFragment.context, Constants.RUNNING_DURATION)).toString() + Constants.MIN_TXT
            }

            if(LocalStorageService.checkLocalData(this@HomePatientFragment.context, Constants.IN_VEHICLE_DURATION)) {
                txtInVehicleDuration.text = DateUtils.formatElapsedTime(LocalStorageService.getActivityData(this@HomePatientFragment.context, Constants.IN_VEHICLE_DURATION)).toString() + Constants.MIN_TXT
            }

            if(LocalStorageService.checkLocalData(this@HomePatientFragment.context, Constants.CALLS_DURATION)) {
                txtCallsDuration.text = LocalStorageService.getLocalData(this@HomePatientFragment.context, Constants.CALLS_DURATION) + Constants.MIN_TXT
            }

            if(LocalStorageService.checkLocalData(this@HomePatientFragment.context, Constants.WATCH_ACC_ACTIVE)) {
                txtWatchAcc.text = LocalStorageService.getLocalData(this@HomePatientFragment.context, Constants.WATCH_ACC_ACTIVE)
            }

            if(LocalStorageService.checkLocalData(this@HomePatientFragment.context, Constants.WATCH_GYR_ACTIVE)) {
                txtWatchGyr.text = LocalStorageService.getLocalData(this@HomePatientFragment.context, Constants.WATCH_GYR_ACTIVE)
            }

            if(LocalStorageService.checkLocalData(this@HomePatientFragment.context, Constants.MOBILE_ACC_ACTIVE)) {
                txtMobileAcc.text = LocalStorageService.getLocalData(this@HomePatientFragment.context, Constants.MOBILE_ACC_ACTIVE)
            }

            if(LocalStorageService.checkLocalData(this@HomePatientFragment.context, Constants.MOBILE_GYR_ACTIVE)) {
                txtMobileGyr.text = LocalStorageService.getLocalData(this@HomePatientFragment.context, Constants.MOBILE_GYR_ACTIVE)
            }
        }
    }
}
