package com.healthassistant.app.services.notifications

import org.json.JSONObject

/**
 * Created by darko on 11/10/17.
 */
interface Notifable {
    fun onNotificationSent(res: JSONObject?)
    fun onNotificationError(err: JSONObject?)
}