package com.healthassistant.app.helpers;

import java.util.ArrayList;

/**
 * Created by Aleksa Simic on 5/13/2019.
 */

public class FacebookData {
    public float getAvg_likes() {
        return avg_likes;
    }

    public float getAvg_comments() {
        return avg_comments;
    }

    public float getFriends_num() {
        return friends_num;
    }

    public ArrayList<Float> getAvg_reacts() {
        return avg_reacts;
    }

    public String getBirthday() {
        return birthday;
    }

    public ArrayList<String> getMy_comments() {
        return my_comments;
    }

    public ArrayList<String> getPosts() {
        return posts;
    }

    public ArrayList<String> getTagged_places() {
        return tagged_places;
    }

    public ArrayList<String> getLiked_places() {
        return liked_places;
    }

    private float avg_likes;
    private float avg_comments;
    private int friends_num;
    private ArrayList<Float> avg_reacts;
    private String birthday;
    private ArrayList<String> my_comments;
    private ArrayList<String> posts;
    private ArrayList<String> tagged_places;
    private ArrayList<String> liked_places;
    public FacebookData(float avg_likes, float avg_comments, int friends_num, ArrayList<Float> avg_reacts,
                        String birthday, ArrayList<String>my_comments, ArrayList<String> posts,
                        ArrayList<String> tagged_places, ArrayList<String> liked_places){

        this.avg_likes=avg_likes;
        this.avg_comments=avg_comments;
        this.friends_num=friends_num;
        this.avg_reacts=avg_reacts;
        this.birthday=birthday;
        this.my_comments=my_comments;
        this.posts=posts;
        this.tagged_places=tagged_places;
        this.liked_places=liked_places;

    }
    @Override
    public String toString() {
        return "Rodjendan"+birthday+":Prosecno lajkova:"+avg_likes+":Prosecno komentara:"+avg_comments+
                ":Broj prijatelja:" +friends_num+ ":Posts:" +posts.toString()+":Comments:"+my_comments.toString()+
                ":Tagged places"+tagged_places.toString()+"Liked posts:"+liked_places.toString();
    }
}
