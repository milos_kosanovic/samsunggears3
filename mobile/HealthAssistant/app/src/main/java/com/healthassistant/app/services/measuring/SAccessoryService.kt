package com.healthassistant.app.services.measuring

import android.content.Intent
import android.os.Binder
import android.os.Handler
import android.os.IBinder
import android.util.Log
import android.widget.Toast
import com.healthassistant.app.activities.patient.PatientActivity
import com.healthassistant.app.helpers.Constants
import com.samsung.android.sdk.SsdkUnsupportedException
import com.samsung.android.sdk.accessory.*
import java.io.IOException


/**
 * Created by darko on 10/5/17.
 */
class SAccessoryService: SAAgent(Constants.PEER_AGENT_SERVICE_NAME, ServiceConnection::class.java) {
    private val mBinder = LocalBinder()
    private lateinit var mServiceConnection: ServiceConnection
    private var isConnected: Boolean = false

    companion object {
        lateinit var measurableResponse: Measurable
    }

    internal var mHandler = Handler()

    override fun onCreate() {
        super.onCreate()
        val sa = SA()
        try {
            sa.initialize(this)
        } catch (e: SsdkUnsupportedException) {
            Log.i("PROBA", "PROBA")
            e.printStackTrace()
        }
    }

    override fun onBind(p0: Intent?): IBinder {
        return mBinder
    }

    override fun onFindPeerAgentsResponse(peerAgents: Array<out SAPeerAgent>?, result: Int) {
        super.onFindPeerAgentsResponse(peerAgents, result)
        when(result) {
            PEER_AGENT_FOUND -> {
                if(peerAgents != null) {
                    for (peerAgent in peerAgents)
                        requestServiceConnection(peerAgent)
                }
            }
            FINDPEER_DEVICE_NOT_CONNECTED -> {
                Toast.makeText(applicationContext, Constants.PEER_NOT_CONNECTED_MESSAGE, Toast.LENGTH_SHORT).show()
                mHandler.post(Runnable {
                    measurableResponse.onPeerAgentNotConnected()
                })
                isConnected = false
            }
            FINDPEER_SERVICE_NOT_FOUND -> {
                Toast.makeText(applicationContext, Constants.FIND_PEER_SERVICE_NOT_FOUND, Toast.LENGTH_SHORT).show()
                mHandler.post(Runnable {
                    measurableResponse.onPeerAgentNotConnected()
                })
                isConnected = false
            }
        }
    }

    override fun onServiceConnectionRequested(peerAgent: SAPeerAgent?) {
        super.onServiceConnectionRequested(peerAgent)
        if (peerAgent != null) {
            acceptServiceConnectionRequest(peerAgent)
        }
    }

    override fun onServiceConnectionResponse(peerAgent: SAPeerAgent?, socket: SASocket?, result: Int) {
        super.onServiceConnectionResponse(peerAgent, socket, result)

        when(result) {
            SAAgent.CONNECTION_SUCCESS -> {
                mServiceConnection = socket as ServiceConnection
                isConnected = true
                Toast.makeText(applicationContext, Constants.CONNECTION_SUCCESS_MESSAGE, Toast.LENGTH_SHORT).show()
                mHandler.post(Runnable {
                    measurableResponse.onPeerAgentConnectionSuccess()
                })
            }
            SAAgent.CONNECTION_ALREADY_EXIST -> {
                isConnected = true
                Toast.makeText(applicationContext, Constants.CONNECTION_ALREADY_EXIST_MESSAGE, Toast.LENGTH_SHORT).show()
                mHandler.post(Runnable {
                    measurableResponse.onPeerAgentConnectionSuccess()
                })
            }
            else -> {
                isConnected = false
                Toast.makeText(applicationContext, Constants.CONNECTION_FAILED_MESSAGE, Toast.LENGTH_SHORT).show()
                mHandler.post(Runnable {
                    measurableResponse.onPeerAgentNotConnected()
                })
            }
        }
    }

    override fun onError(peerAgent: SAPeerAgent?, errorMessage: String?, errorCode: Int) {
        super.onError(peerAgent, errorMessage, errorCode)
        Log.i(errorMessage, errorCode.toString())
        mHandler.post(Runnable {
            if(errorCode == 2049) {
                Toast.makeText(applicationContext, Constants.SDK_NOT_SUPPORTED_MESSAGE, Toast.LENGTH_LONG).show()
            }
        })
    }

    fun findPeers() {
        findPeerAgents()
    }

    fun startMeasuring(message: String) {
        try {
            mServiceConnection!!.send(getServiceChannelId(0), message.toByteArray())
            mHandler.post(Runnable {
                measurableResponse.onStartMeasuring()
            })
        } catch (e: IOException) {
            e.printStackTrace()
            mHandler.post(Runnable {
                if(!isConnected) {
                    Toast.makeText(applicationContext, Constants.PEER_NOT_CONNECTED_MESSAGE, Toast.LENGTH_SHORT).show()
                }
                measurableResponse.onErrorMeasuring()
            })
        } catch (e: UninitializedPropertyAccessException) {
            e.printStackTrace()
            mHandler.post(Runnable {
                if(!isConnected) {
                    Toast.makeText(applicationContext, Constants.PEER_NOT_CONNECTED_MESSAGE, Toast.LENGTH_SHORT).show()
                }
                measurableResponse.onNotConnected()
            })
        }
    }

    fun stopMeasuring(message: String) {
        try {
            mServiceConnection!!.send(getServiceChannelId(0), message.toByteArray())
            mHandler.post(Runnable {
                measurableResponse.onErrorMeasuring()
            })
        } catch (e: IOException) {
            e.printStackTrace()
            mHandler.post(Runnable {
                if(!isConnected) {
                    Toast.makeText(applicationContext, Constants.PEER_NOT_CONNECTED_MESSAGE, Toast.LENGTH_SHORT).show()
                }
                measurableResponse.onErrorMeasuring()
            })
        } catch (e: UninitializedPropertyAccessException) {
            e.printStackTrace()
            mHandler.post(Runnable {
                if(!isConnected) {
                    Toast.makeText(applicationContext, Constants.PEER_NOT_CONNECTED_MESSAGE, Toast.LENGTH_SHORT).show()
                }
                measurableResponse.onErrorMeasuring()
            })
        }
    }

    fun startWatchSensors(message: String) {
        try {
            mServiceConnection!!.send(getServiceChannelId(0), message.toByteArray())
        } catch (e: IOException) {
            e.printStackTrace()
        } catch (e: UninitializedPropertyAccessException) {
            e.printStackTrace()
        }
    }

    fun stopWatchSensors(message: String) {
        try {
            mServiceConnection!!.send(getServiceChannelId(0), message.toByteArray())
        } catch (e: IOException) {
            e.printStackTrace()
        } catch (e: UninitializedPropertyAccessException) {
            e.printStackTrace()
        }
    }

    fun sendData(message: String) {
        try {
            mServiceConnection!!.send(getServiceChannelId(0), message.toByteArray())
        } catch (e: IOException) {
            e.printStackTrace()
        } catch (e: UninitializedPropertyAccessException) {
            e.printStackTrace()
        }
    }

    inner class ServiceConnection: SASocket(ServiceConnection::class.java.name) {

        override fun onReceive(channelId: Int, data: ByteArray?) {
            if(data != null) {
                val message = String(data)
                mHandler.post(Runnable {
                    measurableResponse.onSuccessMeasure(message)
                })
            }
        }

        override fun onError(channelId: Int, errorMessage: String?, errorCode: Int) {
            mHandler.post(Runnable {
                measurableResponse.onErrorMeasure(errorMessage!!)
            })
        }

        override fun onServiceConnectionLost(reason: Int) {
            mHandler.post(Runnable {
                measurableResponse.onPeerAgentConnectionLost()
            })
        }
    }

    inner class LocalBinder : Binder() {
        val service: SAccessoryService
            get() = this@SAccessoryService
    }
}
