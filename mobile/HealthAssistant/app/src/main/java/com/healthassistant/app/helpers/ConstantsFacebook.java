package com.healthassistant.app.helpers;

/**
 * Created by Aleksa Simic on 5/13/2019.
 */

public class ConstantsFacebook {
    public static final String FB_FIELDS="fields";
    public static final String FB_FIELDS_LEFT="id,birthday,posts.since(";
    public static final String FB_FIELDS_RIGHT="),likes,tagged_places,friends";

    public static final String FB_PERMISSION_PROFILE="public_profile";
    public static final String FB_PERMISSION_USER_BIRTHDAY="user_birthday";
    public static final String FB_PERMISSION_USER_FRIENDS="user_friends";
    public static final String FB_PERMISSION_USER_POSTS="user_posts";
    public static final String FB_PERMISSION_USER_LIKES="user_likes";
    public static final String FB_PERMISSION_USER_TAGGED_PLACES="user_tagged_places";


    public static final String FB_POSTS_STRING="posts";
    public static final String FB_DATA_STRING="data";
    public static final String FB_TAGGED_PLACES_STRING="tagged_places";
    public static final String FB_LIKES_STRING="likes";
    public static final String FB_FRIENDS_STRING="friends";

    public static final String FB_BIRTHDAY="birthday";
    public static final String FB_BIRTHDAY_EMPTY="";

    public static final String FB_SUMMARY="summary";
    public static final String FB_TOTAL_COUNT="total_count";

    public static final String FB_POST_MESSAGE="message";

    public static final String FB_NAME_STRING="name";
    public static final String FB_PLACE_STRING="place";
    public static final String FB_LOCATION_STRING="location";
    public static final String FB_CITY_STRING="city";
    public static final String FB_COUNTRY_STRING="country";
    public static final String FB_STREET_STRING="street";


    public static final String FB_CREATED_TIME="created_time";
    public static final String FB_ID="id";
    public static final String FB_GET_COMMENTS="/comments?summary=true";
    public static final String FB_GET_LIKES="/likes?summary=true";

    public static final String SDF_PATTERN="yyyy-MM-dd";

    public static final String FB_REACTS_LEFT="?id=";
    public static final String FB_REACTS_RIGHT="&fields=reactions.type(LOVE).limit(0).summary(total_count).as(reactions_love),reactions.type(WOW).limit(0).summary(total_count).as(reactions_wow),reactions.type(HAHA).limit(0).summary(total_count).as(reactions_haha),reactions.type(SAD).limit(0).summary(total_count).as(reactions_sad),reactions.type(ANGRY).limit(0).summary(total_count).as(reactions_angry),reactions.type(THANKFUL).limit(0).summary(total_count).as(reactions_thankful)";
    public static final String FB_REACTS_LOVE="reactions_love";
    public static final String FB_REACTS_WOW="reactions_wow";
    public static final String FB_REACTS_HAHA="reactions_haha";
    public static final String FB_REACTS_SAD="reactions_sad";
    public static final String FB_REACTS_ANGRY="reactions_angry";
    public static final String FB_REACTS_THANKFUL="reactions_thankful";
}
