package com.healthassistant.app.helpers

import java.util.regex.Pattern

/**
 * Created by darko on 10/25/17.
 */
class Constants {
    companion object {
        // API URLs
//        const val ENDPOINT: String = "http://merenja.appsteam.vtsnis.edu.rs/api/"
        const val ENDPOINT: String = "http://health-assistant-server.herokuapp.com/api/"
        //        const val ENDPOINT: String = "http://192.168.0.144:8000/api/"
        const val REGISTRATION_API_URL = ENDPOINT + "registration"
        const val LOGIN_API_URL: String = ENDPOINT + "login"
        const val NOTIFICATIONS_URL: String = ENDPOINT + "list/notification/"
        const val SEND_NOTIFICATION_URL: String = ENDPOINT + "notifications"
        const val READ_NOTIFICATION_URL: String = ENDPOINT + "read/notification"
        const val NOTIFICATIONS_COUNT_URL: String = ENDPOINT + "count/notification"
        const val GET_STATISTIC_URL: String = ENDPOINT + "allparameters"
        const val POST_STATISTIC_URL: String = ENDPOINT + "parameters"
        const val PUSHER_MEASURING_START_API_URL: String = ENDPOINT + "measuring/start"
        const val PUSHER_MEASURING_STOP_API_URL: String = ENDPOINT + "measuring/stop"
        const val MEASURING_TIMER_API_URL: String = ENDPOINT + "measuring/time"

        // Response Keys
        const val MESSAGE_RESPONSE_KEY: String = "message"
        const val ENTITY_RESPONSE_KEY: String = "entity"
        const val ROLE_RESPONSE_KEY: String = "role"
        const val PATIENT_ROLE_RESPONSE_KEY: String = "PATIENT"
        const val ADMIN_ROLE_RESPONSE_KEY: String = "ADMIN"
        const val USER_NAME_RESPONSE_KEY: String = "name"
        const val USER_LASTNAME_RESPONSE_KEY: String = "lastname"
        const val TOKEN_RESPONSE_KEY: String = "token"
        const val NOTIFICATION_TITLE_RESPONSE_KEY: String = "title"
        const val NOTIFICATION_DESCRIPTION_RESPONSE_KEY: String = "description"
        const val NOTIFICATION_ID_RESPONSE_KEY: String = "id"
        const val NOTIFICATION_USER_ID_RESPONSE_KEY: String = "user_id"
        const val NOTIFICATION_STATUS_RESPONSE_KEY: String = "status"
        const val NOTIFICATION_IS_READ_RESPONSE_KEY: String = "read"
        const val NOTIFICATION_DATE_RESPONSE_KEY: String = "created_at"
        const val READ_RESPONSE_MESSAGE: String = "Read successfully"
        const val NOTIFICATIONS_COUNT_MESSAGE: String = "COUNT"
        const val ERROR_RESPONSE_KEY: String = "error"
        const val TOKEN_EXPIRED_RESPONSE_KEY: String = "token_expired"
        const val MEASURING_TIME_RESPONSE_KEY: String = "measuring_time"

        // Pusher data keys
        const val PUSHER_ACTIVITY_KEY: String = "walking"
        const val PUSHER_LIGHT_KEY: String = "sun"
        const val PUSHER_PHONE_KEY: String = "phonetime"
        const val PUSHER_PULSE_KEY: String = "pulse"
        const val PUSHER_MESSAGE_KEY: String = "message"
        const val PUSHER_MESSAGE_START_KEY: String = "start"
        const val PUSHER_MESSAGE_STOP_KEY: String = "stop"
        const val PUSHER_DATA_TRUE: String = "true"

        // Params Keys
        const val EMAIL_PARAMS_KEY: String = "email"
        const val PASSWORD_PARAMS_KEY: String = "password"
        const val NAME_PARAMS_KEY: String = "name"
        const val LASTNAME_PARAMS_KEY: String = "lastname"
        const val ROLE_PARAMS_KEY: String = "role"
        const val PLAYER_ID_PARAMS_KEY: String = "player_id"
        const val SYSTEM_NOTIFICATION_TYPE_KEY: String = "SYSTEM"
        const val DOCTOR_ADVICES_NOTIFICATION_TYPE_KEY: String = "DOCTOR-ADVICE"
        const val READ_PARAMS_KEY: String = "read"
        const val ID_PARAMS_KEY: String = "id"
        const val PULSE_PARAMS_KEY: String = "pulse"
        const val LIGHT_PARAMS_KEY: String = "light"
        const val ACTIVITY_PARAMS_KEY: String = "activity"
        const val RUNNING_PARAMS_KEY: String = "running"
        const val WALKING_PARAMS_KEY: String = "walking"
        const val STATIONARY_PARAMS_KEY: String = "stationary"
        const val IN_VEHICLE_PARAMS_KEY: String = "incar"
        const val PHONE_DURATION_PARAMS_KEY: String = "phonetime"
        const val PUSHER_MESSAGE_PARAMS_KEY: String = "message"

        // LocalStorage Keys
        const val LOCAL_DATA: String = "localData"
        const val USER_LOCAL_STORAGE: String = "user"
        const val STATIONARY_STARTED_STORAGE: String = "stationaryStarted"
        const val WALKING_STARTED_STORAGE: String = "walkingStarted"
        const val RUNNING_STARTED_STORAGE: String = "runningStarted"
        const val IN_VEHICLE_STARTED_STORAGE: String = "inVehicleStarted"
        const val STATIONARY_DURATION: String = "stationaryDuration"
        const val WALKING_DURATION: String = "walkingDuration"
        const val RUNNING_DURATION: String = "runningDuration"
        const val IN_VEHICLE_DURATION: String = "inVehicleDuration"
        const val CALLS_DURATION: String = "callsDuration"
        const val ONESIGNAL_PLAYER_ID_LOCALSTORAGE: String = "playerId"
        const val ACTIVITIES_DURATION_TIME_LOCALSTORAGE: String = "activitiesDurationTime"
        const val FILE_NUMBER: String = "filenumber";

        // LocalStorage Active values keys
        const val ACTIVITY_ACTIVE: String = "activityActive"
        const val LAST_ACTIVITY: String = "lastActivity"
        const val HRM_ACTIVE: String = "hrmActive"
        const val LIGHT_ACTIVE: String = "lightActive"
        const val GYRO_ACTIVE: String = "gyroActive"
        const val WATCH_ACC_ACTIVE = "watchAccActive"
        const val WATCH_GYR_ACTIVE = "watchGyrActive"
        const val MOBILE_ACC_ACTIVE = "mobileAccActive"
        const val MOBILE_GYR_ACTIVE = "mobileGyrActive"

        // Messages
        const val SDK_NOT_SUPPORTED_MESSAGE: String = "Konekcija sa satom neuspešna. Uređaj nije kompaktibilan ili nema instaliran Samsung Accessory SDK"
        const val NO_NETWORK_CONNECTION_MESSAGE: String = "Nema internet konekcije"
        const val NO_EMAIL_MESSAGE: String = "Niste uneli email adresu"
        const val NO_PASSWORD_MESSAGE: String = "Niste uneli lozinku"
        const val NO_NAME_MESSAGE: String = "Niste uneli vaše ime"
        const val NO_LASTNAME_MESSAGE: String = "Niste uneli vaše prezime"
        const val EMAIL_NOT_VALID_MESSAGE: String = "Email adresa nije odgovarajuća"
        const val FIND_PEER_SERVICE_NOT_FOUND: String = "Aplikacija na satu nije pronađena"
        const val PEER_NOT_CONNECTED_MESSAGE: String = "Proverite Bluetooth konekciju između telefona i sata"
        const val CONNECTION_SUCCESS_MESSAGE: String = "Konekcija sa satom uspostavljena"
        const val CONNECTION_ALREADY_EXIST_MESSAGE: String = "Konekcija sa satom već uspostavljena"
        const val CONNECTION_FAILED_MESSAGE: String = "Konekcija nije uspostavljena"
        const val CONNECTION_LOST_MESSAGE: String = "Konekcija sa satom je izgubljena"
        const val CHOOSE_MEASURING_TYPE_MESSAGE: String = "Morate izabrati minimum jedan tip merenja"
        const val NO_SERVER_MESSAGE: String = "Server nije dostupan"
        const val CALL_LOG_PERMISSION_MESSAGE: String = "Aplikaciji nije dozvoljen pristup listi poziva."
        const val DOCTOR_LOGGED_IN_MESSAGE: String = "Prijavljivanje doktora trenutno nije moguće."

        // Email Regex
        val VALID_EMAIL_ADDRESS_REGEX: Pattern = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE)

        // Peer Agent Service name
        const val PEER_AGENT_SERVICE_NAME: String = "Health Assistant"

        // Request data key
        const val START_MEASURING_MESSAGE: String = "Start"
        const val STOP_MEASURING_MESSAGE: String = "Stop"
        const val PULSE_MEASURING_MESSAGE: String = "Pulse,"
        const val ACTIVITY_MEASURING_MESSAGE: String = "Activity,"
        const val LIGHT_MEASURING_MESSAGE: String = "Light,"
        const val PHONE_MEASURING_MESSAGE: String = "Phone"

        // Activity Recognation keys
        const val ACTIVITY_STATIONARY: String = "STATIONARY"
        const val ACTIVITY_WALKING: String = "WALKING"
        const val ACTIVITY_RUNNING: String = "RUNNING"
        const val ACTIVITY_IN_VEHICLE: String = "IN_VEHICLE"

        // Onesignal post Notification json keys
        const val LANGUAGE_ONESIGNAL_KEY: String = "en"
        const val CONTENTS_ONESIGNAL_KEY: String = "contents"
        const val PLAYER_IDS_ONESIGNAL_KEY: String = "include_player_ids"

        // Notifications Messages
        const val HRM_TOO_HIGH_NOTIFICATION_TITLE: String = "Vaš puls je previše visok"
        const val HRM_TOO_HIGH_NOTIFICATION_DESCRIPTION_PREFIX: String = "Vaš puls je iznosio"
        const val HRM_TOO_HIGH_NOTIFICATION_DESCRIPTION: String = "Pokušajte malo mirovati."

        // Heart rate value
        const val HRM_TOO_HIGH_VALUE: Int = 120

        const val MIN_TXT: String = " min"
        const val LUX_TXT: String = " lux"

        // Tabs
        const val TAB_SYSTEM_NOTIFICATIONS: String = "Notifikacije"
        const val TAB_DOCTOR_ADVICES: String = "Saveti doktora"
        const val TAB_PULSE_STATISTIC: String = "Puls"
        const val TAB_ACTIVITIES_STATISTIC: String = "Aktivnosti"
        const val TAB_LIGHT_STATISTIC: String = "Izloženost svetlosti"
        const val TAB_PHONE_USAGE_STATISTIC: String = "Upotreba telefona"

        // Notifications
        const val NOTIFICATION_STATUS_HEALTH: String = "HEALTH"

        // Tabs tags
        const val HOME_ACTIVE_TAB: String = "HOME_FRAGMENT"
        const val MEASURING_ACTIVE_TAB: String = "MEASURING_FRAGMENT"
        const val NOTIFICATIONS_ACTIVE_TAB: String = "NOTIFICATIONS_FRAGMENT"
        const val STATISTIC_ACTIVE_TAB: String = "STATISTIC_FRAGMENT"
        const val PROFILE_ACTIVE_TAB: String = "PROFILE_FRAGMENT"

        // Send data to server time
        const val SEND_HRM_TIME: Long = 1800000 // 30 mins
        const val SEND_HRM_NOTIFICATION_TIME: Long = 1800000 // 30 mins
        const val SEND_LIGHT_TIME: Long = 1800000 // 30 mins
        const val SEND_ACTIVITY_DURATION_TIME: Long = 3600000 // 1 hour
        const val SEND_PHONE_DATA_TIME: Long = 3600000 // 1 hour
        const val MEASURING_TIME: Long = 300000

        // Start minutes duration
        const val STARTED_MINUTES_TIME: String = "00:00:00"

        // Pusher param for measuring message
        const val MEASURING_REQUEST_SUCCESS_VALUE: Int = 1
        const val MEASURING_REQUEST_FAILED_VALUE: Int = 0

        const val PERMISSION_ALL: Int = 1
        const val SMS_INBOX: String = "inbox"
        const val SMS_SENT: String = "sent"
        const val DAY_DURATION_MILISECONDS: Long = 86400000

    }
}