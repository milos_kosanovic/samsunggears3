package com.healthassistant.app.activities.patient

import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.ViewPager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.healthassistant.app.R
import com.healthassistant.app.activities.patient.statisticstabs.ActivitiesPatientFragment
import com.healthassistant.app.activities.patient.statisticstabs.LightPatientFragment
import com.healthassistant.app.activities.patient.statisticstabs.PhoneUsagePatientFragment
import com.healthassistant.app.activities.patient.statisticstabs.PulsePatientFragment
import com.healthassistant.app.helpers.Constants

class StatisticPatientFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val rootView = inflater!!.inflate(R.layout.fragment_statistic_patient, container, false)

        val viewPager = rootView.findViewById<ViewPager>(R.id.statisticPatientViewPager) as ViewPager
        val tabs = rootView.findViewById<TabLayout>(R.id.statisticPatientTabs) as TabLayout

        setupViewPager(viewPager)
        tabs.setupWithViewPager(viewPager)

        return rootView
    }

    private fun setupViewPager(vPager: ViewPager) {
        val adapter = StatisticPatientFragment.Adapter(childFragmentManager)
        adapter.addFragment(PulsePatientFragment(), Constants.TAB_PULSE_STATISTIC)
        adapter.addFragment(ActivitiesPatientFragment(), Constants.TAB_ACTIVITIES_STATISTIC)
        adapter.addFragment(LightPatientFragment(), Constants.TAB_LIGHT_STATISTIC)
        adapter.addFragment(PhoneUsagePatientFragment(), Constants.TAB_PHONE_USAGE_STATISTIC)
        vPager.adapter = adapter
    }

    internal class Adapter(manager: FragmentManager) : FragmentPagerAdapter(manager) {
        private val mFragmentList = ArrayList<Fragment>()
        private val mFragmentTitleList = ArrayList<String>()

        override fun getItem(position: Int): Fragment {
            return mFragmentList.get(position)
        }

        override fun getCount(): Int {
            return mFragmentList.size
        }

        fun addFragment(fragment: Fragment, title: String) {
            mFragmentList.add(fragment)
            mFragmentTitleList.add(title)
        }

        override fun getPageTitle(position: Int): CharSequence {
            return mFragmentTitleList.get(position)
        }
    }
}
