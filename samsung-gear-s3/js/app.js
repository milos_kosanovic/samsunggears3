/*
* Copyright (c) 2015 Samsung Electronics Co., Ltd.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are
* met:
*
* * Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
* * Redistributions in binary form must reproduce the above
* copyright notice, this list of conditions and the following disclaimer
* in the documentation and/or other materials provided with the
* distribution.
* * Neither the name of Samsung Electronics Co., Ltd. nor the names of its
* contributors may be used to endorse or promote products derived from
* this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
* A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

var SAAgent,
    SASocket,
    connectionListener,
    SAMessageObj,
    responseTxt = document.getElementById("responseTxt");

/* Make Provider application running in background */
//tizen.application.getCurrentApplication().hide();

function createHTML(log_string)
{
    var content = document.getElementById("toast-content");
    content.innerHTML = log_string;
    tau.openPopup("#toast");
}
	
	connectionListener = {
    /* Remote peer agent (Consumer) requests a service (Provider) connection */
    onrequest: function (peerAgent) {
        createHTML("peerAgent: peerAgent.appName<br />" +
                    "is requsting Service conncetion...");

        /* Check connecting peer by appName*/
        if (peerAgent.appName === "HealthAssistant") {
            SAAgent.acceptServiceConnectionRequest(peerAgent);
            createHTML("Service connection request accepted.");

        } else {
            SAAgent.rejectServiceConnectionRequest(peerAgent);
            createHTML("Service connection request rejected.");

        }
    },

    /* Connection between Provider and Consumer is established */
    onconnect: function (socket) {
        var onConnectionLost,
            dataOnReceive;

        createHTML("Service connection established");

        /* Obtaining socket */
        SASocket = socket;

        onConnectionLost = function onConnectionLost (reason) {
            createHTML("Service Connection disconnected due to following reason:<br />" + reason);
        };

        /* Inform when connection would get lost */
        SASocket.setSocketStatusListener(onConnectionLost);
        
        var currentActivity = null;

        dataOnReceive =  function dataOnReceive (channelId, data) {
            if (!SAAgent.channelIds[0]) {
                createHTML("Something goes wrong...NO CHANNEL ID!");
                return;
            }
            tizen.power.request('SCREEN', 'SCREEN_NORMAL');
//            SASocket.sendData(SAAgent.channelIds[0], "Data: " + data);
            var hrm = null;
            var gyr = null;
            var lig = null;
            
            var listenerWalking;
            var listenerStationary;
            var listenerRunning;
            var listenerInVehicle;
            var lightSensor;
            
            var lightCapatibility = tizen.systeminfo.getCapability("http://tizen.org/feature/sensor.photometer");
            if(lightCapatibility == true) {
            	lightSensor = tizen.sensorservice.getDefaultSensor("LIGHT");
            } else {
            	SASocket.sendData(SAAgent.channelIds[0], "Light is not supported");
            }
            
//            var listenerGeasture;
            
         // Gestures
//            try {
//                var isSupported = tizen.humanactivitymonitor.isGestureSupported('GESTURE_PICK_UP');
//                SASocket.sendData(SAAgent.channelIds[0], "GESTURE_PICK_UP is" + (isSupported ? "supported" : "not supported"));
//            } catch (error) {
//            	SASocket.sendData(SAAgent.channelIds[0], "error " + error.message);
//            }
//            try {
//            	listenerGeasture = tizen.humanactivitymonitor.addGestureRecognitionListener('GESTURE_SHAKE', function(data) {
//                	
//                	SASocket.sendData(SAAgent.channelIds[0], "Received" + data.event + "event on" + new Date(data.timestamp * 1000) + "for" + data.type + "type");
//                }, function(error) {
//                	SASocket.sendData(SAAgent.channelIds[0], "error " + error.message);
//                });
//            } catch (error) {
//            	SASocket.sendData(SAAgent.channelIds[0], "error " + error.message);
//            }
           
//   Funkcije za merenje           
         // Merenje aktivnosti
            function startActivityMeasuring() {
            	
            	SASocket.sendData(SAAgent.channelIds[0], "Start");
            	if(currentActivity != null) {
            		SASocket.sendData(SAAgent.channelIds[0], "Activity," + currentActivity);
            	}
            	
            	// Activity recognation WALKING
            	try {
                    listenerWalking = tizen.humanactivitymonitor.addActivityRecognitionListener('WALKING', function(info) {
                    	SASocket.sendData(SAAgent.channelIds[0], "Activity," + info.type);
                    	currentActivity = info.type;
                    	activityTizen = info.type;
                    	var activity = document.getElementById("activity");
                    	activity.innerHTML = activityTizen;
                    }, function(error) {
                    	SASocket.sendData(SAAgent.channelIds[0], "error " + error.message);
                    });
                } catch (error) {
                	SASocket.sendData(SAAgent.channelIds[0], "error " + error.message);
                }
                
             // Activity recognation STATIONARY
                try {
                    listenerStationary = tizen.humanactivitymonitor.addActivityRecognitionListener('STATIONARY', function(info) {
                    	SASocket.sendData(SAAgent.channelIds[0], "Activity," + info.type);
                    	currentActivity = info.type;
                    	activityTizen = info.type;
                    	var activity = document.getElementById("activity");
                    	activity.innerHTML = activityTizen;
                    }, function(error) {
                    	SASocket.sendData(SAAgent.channelIds[0], "error " + error.message);
                    });
                } catch (error) {
                	SASocket.sendData(SAAgent.channelIds[0], "error " + error.message);
                }
                
             // Activity recognation RUNNING
                try {
                    listenerRunning = tizen.humanactivitymonitor.addActivityRecognitionListener('RUNNING', function(info) {
                    	SASocket.sendData(SAAgent.channelIds[0], "Activity," + info.type);
                    	currentActivity = info.type;
                    	activityTizen = info.type;
                    	var activity = document.getElementById("activity");
                    	activity.innerHTML = activityTizen;
                    }, function(error) {
                    	SASocket.sendData(SAAgent.channelIds[0], "error " + error.message);
                    });
                } catch (error) {
                	SASocket.sendData(SAAgent.channelIds[0], "error " + error.message);
                }
                
             // Activity recognation IN_VEHICLE
                try {
                    listenerInVehicle = tizen.humanactivitymonitor.addActivityRecognitionListener('IN_VEHICLE', function(info) {
                    	SASocket.sendData(SAAgent.channelIds[0], "Activity," + info.type);
                    	currentActivity = info.type;
                    	activityTizen = info.type;
                    	var activity = document.getElementById("activity");
                    	activity.innerHTML = activityTizen;
                    }, function(error) {
                    	SASocket.sendData(SAAgent.channelIds[0], "error " + error.message);
                    });
                } catch (error) {
                	SASocket.sendData(SAAgent.channelIds[0], "error " + error.message);
                }
            }
            
            function stopActivityMeasuring() {
            	SASocket.sendData(SAAgent.channelIds[0], "Stop");
            	 // Activity recognation WALKING
            	try {
                    tizen.humanactivitymonitor.removeActivityRecognitionListener(listenerWalking, function(error) {
                    	SASocket.sendData(SAAgent.channelIds[0], "error " + error.message);
                    });
                } catch (error) {
                	SASocket.sendData(SAAgent.channelIds[0], "error " + error.message);
                }
                
             // Activity recognation STATIONARY
                try {
                    tizen.humanactivitymonitor.removeActivityRecognitionListener(listenerStationary, function(error) {
                    	SASocket.sendData(SAAgent.channelIds[0], "error " + error.message);
                    });
                } catch (error) {
                	SASocket.sendData(SAAgent.channelIds[0], "error " + error.message);
                }
                
             // Activity recognation RUNNING
                try {
                    tizen.humanactivitymonitor.removeActivityRecognitionListener(listenerRunning, function(error) {
                    	SASocket.sendData(SAAgent.channelIds[0], "error " + error.message);
                    });
                } catch (error) {
                	SASocket.sendData(SAAgent.channelIds[0], "error " + error.message);
                }
                
             // Activity recognation IN_VEHICLE
                try {
                    tizen.humanactivitymonitor.removeActivityRecognitionListener(listenerInVehicle, function(error) {
                    	SASocket.sendData(SAAgent.channelIds[0], "error " + error.message);
                    });
                } catch (error) {
                	SASocket.sendData(SAAgent.channelIds[0], "error " + error.message);
                }
            }
            
      //  Merenje pulsa (start)    
            function startHearth() { 
            	tizen.humanactivitymonitor.start('HRM', function(hrmInfo) {
            		if(hrmInfo.heartRate > 0) {
    	        			hrm = "Pulse " + hrmInfo.heartRate + ",";
    	        			hrmTizen = hrmInfo.heartRate;
    	        			SASocket.sendData(SAAgent.channelIds[0], hrm);
    	        			var hearth = document.getElementById("hearth");
    	        			hearth.innerHTML = hrmTizen;
            		}
            	});
            }
      // Merenje svetlosti
            function starLight() {
                lightSensor.start(function() {
        			lightSensor.setChangeListener(function(sensorData) {
            			lig = "light " + sensorData.lightLevel;
            			ligTizen = sensorData.lightLevel + " lux";
                		SASocket.sendData(SAAgent.channelIds[0], lig);
//                		var light = document.getElementById("Light");
//                		light.innerHTML = ligTizen;

                		localStorage.setItem("light", ligTizen);
                		document.getElementById("Light").innerHTML = localStorage.getItem("light");
                		
                		
            		});
            		
            	}, function(error) {
            		SASocket.sendData(SAAgent.channelIds[0], "error " + error.message);
            	});
            } 
            
// Provera merenja
            
            if(data == "Phone") {
            	SASocket.sendData(SAAgent.channelIds[0], "Data: " + data);
            }
            if(data == "Stop") {
            	tizen.humanactivitymonitor.stop('HRM');
            	lightSensor.unsetChangeListener();
            	stopActivityMeasuring()
            }
            
            if(data == "Start") {
            	startHearth();
            	starLight();
            	startActivityMeasuring();
            } else {
            	var niz = data.split(",");
                for(var i = 0; i < niz.length; i++ ) {
                	if(niz[i] == "Pulse") {
                		startHearth();
                	}
                	if(niz[i] == "Light") {
                		starLight();
                	}
                	if(niz[i] == "Activity") {
                		startActivityMeasuring();
                	}
                }
                if(niz[i] != "Activity" && niz[i] != "Light" && niz[i] != "Pulse") {
                	for(var i = 0; i < niz.length; i++) {
                		var niz1 = niz[i].split(" ");
                		switch (niz1[0]) {
						case "Stationary":
							var stationaryDuration = niz1[1];
    	        			var stationary = document.getElementById("Stationary");
    	        			stationary.innerHTML = stationaryDuration;
							break;
						case "Walking":
							var walkingDuration = niz1[1];
    	        			var walking = document.getElementById("Walking");
    	        			walking.innerHTML = walkingDuration;
							break;	
						case "Running":
							var runningDuration = niz1[1];
    	        			var running = document.getElementById("Running");
    	        			running.innerHTML = runningDuration;
							break;
						case "InVehicle":
							var inVehicleDuration = niz1[1];
    	        			var inVehicle = document.getElementById("InVehicle");
    	        			inVehicle.innerHTML = inVehicleDuration;
							break;
						default:
							break;
						}
                    }
                }
            }
//        	
        	// Sleep monitor
//        	tizen.humanactivitymonitor.start('SLEEP_MONITOR', function(sleepInfo) {
//        		SASocket.sendData(SAAgent.channelIds[0], "start sleep monitor");
//    			SASocket.sendData(SAAgent.channelIds[0], "Sleep status " + sleepInfo.status);
//    			SASocket.sendData(SAAgent.channelIds[0], "Timestamp: " + sleepInfo.timestamp + "milliseconds");
//        	}); 
        	
        	function gyrosopceStart () {
       		var gyroscopeSensor = tizen.sensorservice.getDefaultSensor("GYROSCOPE");
          	gyroscopeSensor.start(function() {
            		gyroscopeSensor.getGyroscopeSensorData(function(sensorData) {
            			var gyr = "Gyroscope: " + sensorData.x + " " + sensorData.y + " " + sensorData.z;
        				SASocket.sendData(SAAgent.channelIds[0], gyr);
        				gyrosopceStart();
            		}, function(error) {
            			gyr = "GyrX " + "Error,";
            			SASocket.sendData(SAAgent.channelIds[0], gyr);
            		});
            	});
        	}
        	// Gyroscop Sensor
            var gyroscopeCapability = tizen.systeminfo.getCapability("http://tizen.org/feature/sensor.gyroscope");
            if(gyroscopeCapability == true) {
            	gyrosopceStart();
            } else {
            	SASocket.sendData(SAAgent.channelIds[0], "gyroscopeRotationCapability is not suported ");
            }
            
            function accelerometerStart() {
            	linearAccelerationSensor.start(function() {
            		 linearAccelerationSensor.getLinearAccelerationSensorData(function(sensorData) {
            			 var acc = "Accelerometer: " + sensorData.x + " " + sensorData.y + " " + sensorData.z;
         				 SASocket.sendData(SAAgent.channelIds[0], acc);
         				 accelerometerStart();
            		 }, function(error) {
            			 SASocket.sendData(SAAgent.channelIds[0], "AccX 2 " + error.message);
            		 });
            	}, function(error) {
            		SASocket.sendData(SAAgent.channelIds[0], "AccX " + error.message);
            	});
            }
//            Linear Acceleration
            var linearAccelerationCapability = tizen.systeminfo.getCapability("http://tizen.org/feature/sensor.linear_acceleration");
            if(linearAccelerationCapability == true) {
            	try {
            		var linearAccelerationSensor = tizen.sensorservice.getDefaultSensor("LINEAR_ACCELERATION");

            	} catch(error) {
       			 SASocket.sendData(SAAgent.channelIds[0], error);
            	}
            	accelerometerStart();
            } else {
            	SASocket.sendData(SAAgent.channelIds[0], "linearAccelerationSensor is not suported ");
            }
    	
        };

        /* Set listener for incoming data from Consumer */
        SASocket.setDataReceiveListener(dataOnReceive);
    },
    onerror: function (errorCode) {
        createHTML("Service connection error<br />errorCode: " + errorCode);
    }
};
function requestOnSuccess (agents) {
    var i = 0;

    for (i; i < agents.length; i += 1) {
        if (agents[i].role === "PROVIDER") {
            createHTML("Service Provider found!<br />" +
                        "Name: " +  agents[i].name);
            SAAgent = agents[i];
            break;
        }
    }

    /* Set listener for upcoming connection from Consumer */
    SAAgent.setServiceConnectionListener(connectionListener);
};

function requestOnError (e) {
    createHTML("requestSAAgent Error" +
                "Error name : " + e.name + "<br />" +
                "Error message : " + e.message);
};

/* Requests the SAAgent specified in the Accessory Service Profile */
webapis.sa.requestSAAgent(requestOnSuccess, requestOnError);

(function () {
    /* Basic Gear gesture & buttons handler */
    window.addEventListener('tizenhwkey', function(ev) {
        var page,
            pageid;

        if (ev.keyName === "back") {
            page = document.getElementsByClassName('ui-page-active')[0];
            pageid = page ? page.id : "";
            if (pageid === "main") {
                try {
                    tizen.application.getCurrentApplication().exit();
                } catch (ignore) {
                }
            } else {
                window.history.back();
            }
        }
    });
}());
(function(tau) {
    var toastPopup = document.getElementById('toast');

    toastPopup.addEventListener('popupshow', function(ev){
        setTimeout(function () {
            tau.closePopup();
        }, 3000);
    }, false);
})(window.tau);
