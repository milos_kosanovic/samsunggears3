<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'UserController@authenticate');
Route::post('registration', 'UserController@registration');

Route::group(['middleware' => 'jwt.auth'], function () {
    Route::get('users', 'UserController@index');

    Route::resource('notifications', 'NotificationController', ['only' => [
        'store'
    ]]);
    Route::get('list/notification/{type}', 'NotificationController@getNotifications');
    Route::post('read/notification', 'NotificationController@readNotification');
    Route::get('count/notification', 'NotificationController@getNotificationsCount');

    Route::get('allparameters', 'MeasuringController@getParameters');
    Route::post('parameters', 'MeasuringController@postParameters');

    Route::post('measuring/start', 'MeasuringController@responseMessageMeasuringStart');
    Route::post('measuring/stop', 'MeasuringController@responseMessageMeasuringStop');

    Route::get('measuring/time', 'UserController@measuringTime');
});

Route::post('measuring/parameters', 'MeasuringController@allMeasuring');
Route::post('month/measuring/parameters', 'MeasuringController@monthMeasuring');
Route::get('user/notification/{id}', 'NotificationController@getNotificationsForUser');
Route::post('single/measuring', 'MeasuringController@singleMeasuring');

Route::get('migrate', function () { \Artisan::call('migrate:refresh', ['--seed' => true]); });


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
