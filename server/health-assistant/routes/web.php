<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if (Auth::user()) {
        return redirect('home');
    }
    else {
        return redirect('index');
    }
});

Route::get('/contact', 'UserController@map');
Route::post('/contact', 'UserController@contactForm');

Route::get('/index', function () {
    return view('index');
});

Auth::routes();


Route::middleware(['auth'])->group(function () {

    Route::post('password/change', 'UserController@changePassword');
    Route::get('/home', 'UserController@homePage')->name('home');

    Route::get('/informations', function () {
        return view('informations');
    });

    Route::get('/profile', 'UserController@profilePage');

    Route::post('send/notification', 'MeasuringController@callPusher');
    Route::post('resend/notification', 'MeasuringController@recallPusher');

    Route::post('onesignal', 'NotificationController@callSignail');

    Route::get('logout','Auth\LoginController@logout');

    Route::post('measuring/time/{id}', 'UserController@setMeasuringTime');

});
