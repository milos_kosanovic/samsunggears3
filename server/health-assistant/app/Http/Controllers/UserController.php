<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserLoginRequest;
use App\Http\Requests\UserRegistrationRequest;
use App\User;
use Berkayk\OneSignal\OneSignalFacade;
use Illuminate\Support\Facades\Auth;
use Mapper;
use Illuminate\Http\Request;
use Pusher\Pusher;
use Tymon\JWTAuth\Facades\JWTAuth;
use Berkayk\OneSignal\OneSignalServiceProvider;
use Illuminate\Support\Facades\Mail;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::where('role', 'PATIENT')->where('player_id', '!=', null)->get();
        return response()->custom(200,'Patient users', $users);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * login
     */
    public function authenticate(UserLoginRequest $request)
    {
        $data = [];
        $payload = $request->only( 'email', 'password' );
        $token = JWTAuth::attempt( $payload );
        if ($token) {
            $entity = User::where('email', '=', $payload['email'])->first();
            if ($entity->role == 'PATIENT' || $entity->role == 'ADMIN')
            {
                $entity = User::where('email', '=', $payload['email'])->update(['player_id' => $request->player_id]);
                $entity = User::where('email', '=', $payload['email'])->first();
            }
            $entity->token = $token;
            return response()->custom(200, 'Uspešno ste pristupili.', $entity);
        }
        return response()->custom(400,'Neuspešno.', null);
    }

    /**
     * registration
     */
    public function registration(UserRegistrationRequest $request)
    {
        $user = new User();
        $user->name = $request->name;
        $user->lastname = $request->lastname;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->role = $request->role;
        $user->player_id = $request->player_id;

        if (User::where('email','=', $user->email)->first()) {
            return response()->custom(400,'Došlo je do greške prilikom kreiranja korisnika.', null);
        }

        if ($user->save()) {
            $payload = $request->only( 'email', 'password' );
            $user->token   = JWTAuth::attempt( $payload );
            return response()->custom(200,'Uspešno ste kreirali korisnika!', $user);
        }
        return response()->custom(400,'Došlo je do greške prilikom kreiranja korisnika.', null);
    }

    /**
     * contact map and view
     */
    public function map()
    {
        $map = Mapper::map(43.3296997, 21.8873622, ['zoom' => 17]);
        return view('contact', [
            'map' => $map
        ]);
    }

    /**
     * contact
     */
    public function contactForm(Request $request) {
        $email = $request->email;
        Mail::raw($request->message, function ($message) use ($email) {
            $message->from($email, 'HealthAssistant');
            $message->to('vtshealthassistant@gmail.com');
            $message->subject('Nova poruka!');
        });
        $notification = array(
            'message' => 'Uspešno ste poslali mail!',
            'type' => 'success'
        );
        return back()->with($notification);
    }

    /**
     * home page
     */
    public function homePage()
    {
        $users = User::where('role', 'PATIENT')->where('player_id', '!=', null)->latest()->get();
        return view('home',
        [
            'users' => $users
        ]);
    }

    /**
     * profile page
     */
    public function profilePage()
    {
        $users = User::where('role', 'PATIENT')->where('player_id', '!=', null)->latest()->get();

        return view('profile',
            [
                'users' => $users
            ]);
    }

    /**
     * change password
     */
    public function changePassword(Request $request) {
        $userId = Auth::user()->id;
        $user = User::find($userId);
        $user->password = bcrypt($request->password);
        if ($user->save()) {
            $message = array(
                'message' => 'Uspešno ste promenili lozinku!',
                'type' => 'success'
            );
        }
        else {
            $message = array(
                'message' => 'Lozinka nije promenjena!',
                'type' => 'error'
            );
        }
        return back()->with($message);
    }

    /**
     * get measuring time api
     */
    public function measuringTime() {
        $userId = Auth::user()->id;
        $measuring = User::select(['measuring_time'])->find($userId);

        if ($measuring) {
            return response()->custom(200,'Uspešno ste preuzeli vreme merenja!', $measuring);
        }
        return response()->custom(400,'Došlo je do greške prilikom preuzimanja vremena merenja!', null);
    }

    /**
     * change measuring time
     */
    public function setMeasuringTime(Request $request, $id) {
        $user = User::find($id);
        $user->measuring_time = $request->measuring_time;
        if ($user->save()) {
            $message = array(
                'message' => 'Uspešno ste promenili vreme merenja!',
                'type' => 'success'
            );
        }
        else {
            $message = array(
                'message' => 'Vreme merenja nije promenjena!',
                'type' => 'error'
            );
        }
        return back()->with($message);
    }
}
