<?php

namespace App\Http\Controllers;

use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Pusher\Pusher;

class MeasuringController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * push notification
     */
    public function callPusher(Request $request)
    {
        $userId = $request->user_id;
        $user = User::find($userId);
        $user->measuring = 0;
        if ($user->save()) {
            $options = array(
                'cluster' => 'eu',
                'encrypted' => true
            );

            $pusher = new Pusher(
                '4db5a09a2fe8cf130080',
                '44a3f8208e57791680ab',
                '427732',
                $options
            );

            $data['walking'] = $request->walking;
            $data['sun'] = $request->sun;
            $data['phonetime'] = $request->phonetime;
            $data['pulse'] = $request->pulse;

            $data['message'] = 'start';
            $pusher->trigger('health-channel', 'measuring-event'.$userId, $data);

        }

    }

    /**
     * push notification
     */
    public function recallPusher(Request $request)
    {
        $userId = $request->user_id;
        $user = User::find($userId);
        $user->measuring = 0;
        if ($user->save()) {
            $options = array(
                'cluster' => 'eu',
                'encrypted' => true
            );

            $pusher = new Pusher(
                '4db5a09a2fe8cf130080',
                '44a3f8208e57791680ab',
                '427732',
                $options
            );

            $data['message'] = 'stop';
            $pusher->trigger('health-channel', 'measuring-event'.$userId, $data);
        }
    }

    /**
     * stats for parameteters
     */
    public function getParameters()
    {

        $userId = Auth::user()->id;


        $file_handle = "csv/heart_rate.csv";
        if (!($fp = fopen($file_handle, 'r'))) {
            die("Can't open file...");
        }

        //read csv headers
        $key = fgetcsv($fp,"1024",",");

        // parse csv rows into array
        $json = array();
        while ($row = fgetcsv($fp,"1024",",")) {

            if ($row[1] == (int)$userId) {
                $json[] = array_combine($key, $row);
            }
        }

        // release file handle
        fclose($fp);

        return response()->custom(200,'Successfully.', $json);
    }

    /**
     * stats for parameteters
     */
    public function postParameters(Request $request)
    {
        $file_handle = "csv/heart_rate.csv";

        $_POST['user_id']=Auth::user()->id;
        $_POST['pulse']=$request->pulse;
        $_POST['light']=$request->light;
        $_POST['activity']=$request->activity;
        $_POST['phonetime']=$request->phonetime;
        $_POST['created_at']=Carbon::now()->format('d.m.Y');
        $_POST['running']=$request->running;
        $_POST['stationary']=$request->stationary;
        $_POST['walking']=$request->walking;
        $_POST['incar']=$request->incar;
        $_POST['time']=Carbon::now()->format('H:i:s');

        //ID
        $i=1;
        $fp = fopen($file_handle, 'r');
        $key = fgetcsv($fp,"1024",",");
        while ($row = fgetcsv($fp,"1024",",")) {
            $i++;
        }

        $_POST['id']=$i;

        $file = fopen($file_handle, 'a');

        $savearray = [ $_POST['id'], $_POST['user_id'], $_POST['pulse'], $_POST['light'], $_POST['activity'], $_POST['phonetime'], $_POST['created_at'], $_POST['running'], $_POST['stationary'], $_POST['walking'], $_POST['incar'],$_POST['time']];

        fputcsv($file, $savearray);
        fclose($file);

        return response()->custom(200,'Successfully.', $_POST);
    }


    /**
     * get stats for parameteters web
     */
    public function allMeasuring(Request $request)
    {
        $file_handle = "csv/heart_rate.csv";
        if (!($fp = fopen($file_handle, 'r'))) {
            die("Can't open file...");
        }

        //read csv headers
        $key = fgetcsv($fp,"1024",",");

        // parse csv rows into array
        $json = array();

        $month = $request->month;
        $year = $request->year;
        $day = $request->day;

        $date = $day.'.'.$month.'.'.$year;

        while ($row = fgetcsv($fp,"1024",",")) {

            if ($row[1] == (int)$request->id) {
                if (Carbon::parse($row[6])->format('d/m/Y') == Carbon::parse($date)->format('d/m/Y')) {
                    $json[] = array_combine($key, $row);
                }
            }
        }

        // release file handle
        fclose($fp);

        return response()->custom(200,'Successfully.', $json);
    }

    /**
     * get stats for parameteters web
     */
    public function monthMeasuring(Request $request)
    {
        $file_handle = "csv/heart_rate.csv";
        if (!($fp = fopen($file_handle, 'r'))) {
            die("Can't open file...");
        }

        //read csv headers
        $key = fgetcsv($fp,"1024",",");

        // parse csv rows into array
        $json = array();
        $month = $request->month;
        $year = $request->year;
        $date = '22.'.$month.'.'.$year;
        while ($row = fgetcsv($fp,"1024",",")) {

            if ($row[1] == (int)$request->id) {
                if (Carbon::parse($row[6])->format('m/Y') == Carbon::parse($date)->format('m/Y')) {
                    $json[] = array_combine($key, $row);
                }
            }
        }

        // release file handle
        fclose($fp);

        return response()->custom(200,'Successfully.', $json);
    }

    /**
     * get stats for parameteters web single
     */
    public function singleMeasuring(Request $request)
    {
        $i = 0;
        $file_handle = "csv/heart_rate.csv";
        if (!($fp = fopen($file_handle, 'r'))) {
            die("Can't open file...");
        }

        //read csv headers
        $key = fgetcsv($fp,"1024",",");

        // parse csv rows into array
        $json = null;
        while ($row = fgetcsv($fp,"1024",",")) {

            if ($row[1] == (int)$request->user_id) {
                $json = array_combine($key, $row);
            }
        }
        // release file handle
        fclose($fp);

        return response()->custom(200,'Successfully.', $json);
    }

    /**
     * if succsessfully meassuring started
     */
    public function responseMessageMeasuringStart(Request $request)
    {
        $userId = Auth::user()->id;
        $user = User::find($userId);

        if ($request->message == 1) {
            $user->measuring = 1;
            $message = array(
                'message' => 'Merenje je pokrenuto!',
                'type' => 'success'
            );
        }
        else {
            $user->measuring = 0;
            $message = array(
                'message' => 'Merenje nije pokrenuto!',
                'type' => 'error'
            );
        }

        $user->save();

        $pusher = new Pusher("4db5a09a2fe8cf130080", "44a3f8208e57791680ab", "427732", array('cluster' => 'eu'));

        $pusher->trigger('start-message', 'start-event', array('message' => $message, 'id' => $userId));

    }

    /**
     * if error meassuring started
     */
    public function responseMessageMeasuringStop(Request $request)
    {
        $userId = Auth::user()->id;
        $user = User::find($userId);
        if ($request->message == 1) {
            $user->measuring = 0;
            $message = array(
                'message' => 'Merenje je prekinuto!',
                'type' => 'success'
            );
        }
        else {
            $user->measuring = 1;
            $message = array(
                'message' => 'Merenje nije prekinuto!',
                'type' => 'error'
            );
        }

        $user->save();

        $pusher = new Pusher("4db5a09a2fe8cf130080", "44a3f8208e57791680ab", "427732", array('cluster' => 'eu'));

        $pusher->trigger('stop-message', 'stop-event', array('message' => $message, 'id' => $userId));
    }
}
