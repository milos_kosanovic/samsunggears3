<?php

namespace App\Http\Controllers;

use App\Notification;
use Illuminate\Http\Request;
use Berkayk\OneSignal\OneSignalServiceProvider;
use Berkayk\OneSignal\OneSignalFacade;
use Illuminate\Support\Facades\Auth;
use nilsenj\Toastr\Toastr;


class NotificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $notification = new Notification();
        $notification->title = $request->title;
        $notification->description = $request->description;
        $notification->user_id = $request->user_id;
        $notification->type = $request->type;
        $notification->status = $request->status;

        if ($notification->save()) {
            return response()->custom(200,'Successfully!', $notification);
        }

        return response()->custom(400,'There was an error while creating notification.', null);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * one signal
     */
    public function callSignail(Request $request)
    {
        $notification = new Notification();
        $notification->title = $request->title;
        $notification->description = $request->description;
        $notification->user_id = $request->user_id;
        $notification->type = 'DOCTOR-ADVICE';
        $notification->status = $request->status;

        if ($notification->save()) {
            $message = array(
                'message' => 'Poslali ste notifikaciju!',
                'type' => 'success'
            );
        }
        else {
            $message = array(
                'message' => 'Notifikacija nije poslata!',
                'type' => 'error'
            );
        }
        OneSignalFacade::sendNotificationToUser($request->title, $request->player_id, $url = null, $data = null, $buttons = null, $schedule = null);
        return back()->with($message);
    }

    /**
     * get notifications
     */
    public function getNotifications($type)
    {
        $userId =  Auth::user()->id;
        $notifications = Notification::where('type', $type)->where('user_id', $userId)->latest()->get();
        return response()->custom(200,'Notifications!', $notifications);
    }

    /**
     * read notification
     */
    public function readNotification(Request $request)
    {
        $notification = Notification::find($request->id);
        $notification->read = $request->read;
        $readss = 'Read successfully';
        if ($notification->save()) {
            return response()->custom(200,'Successfully!', $readss);
        }

        return response()->custom(400,'There was an error while updating notification.', null);
    }

    /**
     * count notification
     */
    public function getNotificationsCount()
    {
        $id = Auth::user()->id;
        $notififacions = Notification::where('read', 0)->where('user_id', $id)->get();
        $notificationsCount = count($notififacions);
        return response()->custom(200, 'COUNT', $notificationsCount);
    }

    /**
     * get notifications for web
     */
    public function getNotificationsForUser($id)
    {
        $notifications = Notification::where('user_id', $id)->latest()->get();
        return response()->custom(200,'Notifications!', $notifications);
    }
}
