<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Health Assistant</title>

        {{--STYLES--}}
        <link rel="stylesheet" type="text/css" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="bower_components/bootstrap/dist/css/bootstrap-theme.min.css">
        <link rel="stylesheet" type="text/css" href="css/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="bower_components/toastr/toastr.min.css">
        <link rel="stylesheet" href="bower_components/aos/dist/aos.css" />
        <link rel="stylesheet" type="text/css" href="css/style.css">

        {{--JS--}}
        <script src="bower_components/jquery/dist/jquery.js"></script>
        <script src="bower_components/aos/dist/aos.js"></script>
    </head>
    <body>
    @if(!Auth::user())
        <nav class="navbar navbar-default login-navbar">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/index"><img src="img/logo-login.png"></a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right navbar-style">
                        <li class="{{ Request::is( '/' ) ? 'active-login' : '' }}"><a href="/" style="color: #c1f5e3;">Početna</a></li>
                        <li class="{{ Request::is( 'contact' ) ? 'active-login' : '' }}"><a href="/contact" style="color: #c1f5e3;">Kontakt</a></li>
                        <li class="{{ Request::is( 'login' ) ? 'active-login' : '' }}"><a href="/login" style="color: #c1f5e3;"><i class="fa fa-lock" aria-hidden="true"></i></a></li>
                    </ul>
                </div>
            </div>
        </nav>
    @else
        <nav class="navbar navbar-default login-navbar">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span style="background: #3bdbac !important" class="icon-bar"></span>
                        <span style="background: #3bdbac !important" class="icon-bar"></span>
                        <span style="background: #3bdbac !important" class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand logo-brand" href="/"><img src="img/logo-home.png"></a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        <li class="{{ Request::is( 'home' ) ? 'active-logged-in' : '' }}"><a href="/home" style="color: #9fafb8;">Početna</a></li>
                        <li class="{{ Request::is( 'informations' ) ? 'active-logged-in' : '' }}"><a href="/informations" style="color: #9fafb8;">Informacije</a></li>
                        <li class="{{ Request::is( 'profile' ) ? 'active-logged-in' : '' }}"><a href="/profile" style="color: #9fafb8;">Profil</a></li>
                        <li><a href="/logout" style="color: #9fafb8;">Izlogujte se</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    @endif
    <?php
    $loader =false;
    ?>
        <div class="container">
            <div class="top-position-container">
                <div class="loader" id="loading"></div>
                @yield('content')
            </div>
        </div>
    </body>

    {{--JS--}}
    <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="bower_components/toastr/toastr.min.js"></script>

    <script>
        AOS.init();
    </script>
    <script>
        $( "#loading" ).hide();
        $( document ).ajaxStart(function() {
            $( "#loading" ).show();
        });
        setTimeout(function() {
            $( document ).ajaxStop(function() {
                $( "#loading" ).hide();

            });
        }, 300);

        $('#container').scroll(function() {
            $('#loading').css('top', $(this).scrollTop());
        });
    </script>
    <script>
        @if(Session::has('message'))
            @if(Session::has('type') == 'success')
                toastr.success("{{Session::get('message')}}");
            @else
                toastr.error("{{Session::get('message')}}");
            @endif
        @endif
    </script>
</html>
