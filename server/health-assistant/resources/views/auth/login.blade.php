@extends('welcome')

@section('content')
<div class="row" data-aos="fade-up">
    <div class="login-content">
        <div class="login-page">
            <div class="panel-heading">
                <h3 class="color-font-white">Dobrodošli nazad! Ulogujte se sada.</h3>
                <p class="color-font-green">Da biste pratili zdravlje pacijenata unesite korisničke podatke.</p>
            </div>
            <div class="panel-body">
                <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                    {{ csrf_field() }}

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <div class="col-md-12 input-login">
                            <input id="email" type="email" placeholder="E-mail" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <div class="col-md-12 input-login">
                            <input id="password" placeholder="Lozinka" type="password" class="form-control" name="password" required>

                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-12">
                            <button type="submit" class="btn login-btn">
                                ULOGUJTE SE
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<style>
    body {
        background: #3fe8b7 !important;
    }
</style>
@endsection
