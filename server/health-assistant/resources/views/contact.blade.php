@extends('welcome')

@section('content')
    <div class="row" data-aos="fade-up">
        <div class="col-md-8">
            <div style="width: 100%; height: 416px; margin-top: 26px;">
                {!! Mapper::render() !!}
            </div>
        </div>
        <div class="col-md-4">
            <div>
                <div class="panel-heading">
                    <h3 class="color-font-white">Budite slobodni da nas kontaktirate!</h3>
                    <p class="color-font-green">Unesite e-mail adresu i vašu poruku i odgovorićemo u što kraćem roku.</p>
                </div>
                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="/contact">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <div class="col-md-12 input-login">
                                <input id="email" type="email" placeholder="Vaša e-mail adresa" class="form-control" name="email" required autofocus>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12 input-login">
                                <textarea rows="8" placeholder="Vaša poruka" class="form-control contact-textarea" name="message" required></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12">
                                <button type="submit" class="btn login-btn">
                                    Pošaljite
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <style>
        body {
            background: #3fe8b7 !important;
        }
    </style>
@endsection