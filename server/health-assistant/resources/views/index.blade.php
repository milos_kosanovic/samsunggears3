@extends('welcome')

@section('content')
    <center data-aos="fade-up">
        <div class="home-font-color">
            <h1>Health Assistant</h1>
        </div>
    </center>
    <div class="row" data-aos="fade-up">
        <div class="col-xs-12 col-md-offset-3 col-md-6 home-font-color">
            <p style="text-align: center">Sistem "Health Assistant" za udaljeni nadzor pacijenata omogućava udaljeni nadzor zdravstvenog stanja pacijenata i samim tim poboljšanje zdravstvenih usluga.<br>
            </p>
        </div>
    </div>
    <div class="form-group" data-aos="fade-up">
        <center>
            <a href="/login" type="submit" class="btn login-btn login-home-page">
                ULOGUJTE SE
            </a>
        </center>
    </div>
    <div class="home-font-color" data-aos="fade-up">
        <center>
            <h2>Karakteristike</h2>
        </center>
    </div>
    <div class="row" data-aos="fade-up">
        <div class="col-xs-12 col-md-4 home-font-color">
            <center>
                <img src="img/watch.png">
                <p>Aplikacija na satu</p>
            </center>
        </div>
        <div class="col-xs-12 col-md-4 home-font-color">
            <center>
                <img src="img/mob.png">
                <p>Mobilna aplikacija</p>
            </center>
        </div>
        <div class="col-xs-12 col-md-4 home-font-color">
            <center>
                <img src="img/web.png">
                <p>Web aplikacija</p>
            </center>
        </div>
    </div>
    <div class="home-font-color" data-aos="fade-up">
        <center>
            <h2>Naš sistem</h2>
        </center>
    </div>
    <div class="row" data-aos="fade-up">
        <div class="col-xs-12 col-md-6 watch-home-img">
            <img src="img/watch-home.png">
        </div>
        <div class="col-xs-12 col-md-6 home-font-color">
            <p>Sistem "Health Assistant" za udaljeni nadzor pacijenata omogućava udaljeni nadzor zdravstvenog stanja pacijenata i samim tim poboljšanje zdravstvenih usluga.<br>
                Ciljna grupa sistema jeste populacija starijih ljudi, kao i zdravstvena nega osoba sa posebnim potrebama.
                Sistem omogućava medicinskom osoblju da pošalje poruku pacijentu, izvrši udaljeno merenje pulsa pacijenta, akcelerometarskih i drugih podataka, i prepozna njegovu trenutnu aktivnost (mirovanje, hodanje, trčanje).<br>
                Dalji ciljevi sistema jesu razvijanje hardverskog rešenja kako bi korisnici koji ne mogu da priušte pametan sat mogli da uživaju isti nivo nege.
                Ovim hardverskim rešenjem bio bi rešen i problem dementnih osoba, kod kojih postoji mogućnost skidanja i oštećenja sata.
                Health Assistant je sistem koji zahteva pristup privatnim podacima koji su osetljive prirode. Pristup može predstavljati izazov i problem ukoliko nije pravno i etički regulisan.<br>
                U budućnosti je planirano da se grupa senzora proširi i na one koji se ne nalaze na pametnom satu.
                Više pažnje će biti posvećeno obradi podataka i prepoznavanju dodatnih ljudskih aktivnosti primenom metoda mašinskog učenja, kao i prepoznavanje promene položaja tela ili slučajnog pada. </p>
        </div>
    </div>
    <style>
        body {
            background: #3fe8b7 !important;
        }
    </style>
@endsection