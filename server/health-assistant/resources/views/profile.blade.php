@extends('welcome')

@section('content')
<div class="row">
    <div class="col-md-5">
        <div class="users-table">
            <div class="row">
                <div class="title-informations col-xs-12">
                    <h3 style="font-size: 18px;">Vaši pacijenti</h3>
                </div>
            </div>
            @if($users->isEmpty())
                <center>
                    <h3>Nema rezultata</h3>
                </center>
            @endif
            @foreach($users as $user)
            <div class="panel collapse-informations">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse{{$user->id}}">
                    <div class="panel-heading collapse-text">
                        <h4 class="panel-title">
                            <img src="img/avatar.png" style="margin-right: 5px;">{{$user->name}} {{$user->lastname}}<span class="glyphicon glyphicon-chevron-down pull-right chevron-icon"></span>
                        </h4>
                    </div>
                </a>
                <div id="collapse{{$user->id}}" class="panel-collapse collapse">
                    <div class="panel-body">
                        <button type="submit" class="btn notification-btn modal-btn" onclick="getDayParameters({{$user->id}})" data-toggle="modal" data-target="#myModalStats{{$user->id}}">Statistika</button>
                        <button type="button" class="btn notification-btn modal-btn" onclick="getNotifications({{$user->id}})" data-toggle="modal" data-target="#myModalNotifications{{$user->id}}" >Notifikacije</button>
                        <button type="button" class="btn notification-btn modal-btn" data-toggle="modal" data-target="#myModalSettings{{$user->id}}" >Podešavanja</button>
                    </div>
                </div>
            </div>

            <!-- Modal stats -->
            <div id="myModalStats{{$user->id}}" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content notification-modal">
                        <div class="modal-header notification-modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title"><span>{{$user->name}} {{$user->lastname}}</span></h4>
                        </div>
                        <ul class="nav nav-tabs nav-modal-stats">
                            <li class="active"><button class="btn" id="buttonDay{{$user->id}}" onclick="getDayParameters({{$user->id}})" >Dan</button></li>
                            <li><button class="btn" id="buttonMonth{{$user->id}}" onclick="monthMeasuring({{$user->id}})">Mesec</button></li>
                        </ul>
                        <div class="modal-body pikcer-month" id="picker-month{{$user->id}}">
                            <div class="row">
                                <input class="col-xs-4 datepikcer-input" type="month" id="picker-value{{$user->id}}" name="bday-month">
                                <button class="btn notification-btn col-xs-2" onclick="startMonthMeasuring({{$user->id}})">Pretraži</button>
                            </div>
                        </div>
                        <div class="modal-body pikcer-day" id="picker-day{{$user->id}}">
                            <div class="row">
                                <input class="col-xs-4 datepikcer-input" type="date" id="day-picker-value{{$user->id}}">
                                <button class="btn notification-btn col-xs-2" onclick="findDayParameters({{$user->id}})">Pretraži</button>
                            </div>
                        </div>
                        <div class="modal-body" id="date{{$user->id}}">

                        </div>
                    </div>
                </div>
            </div>

            <!-- Modal notifications -->
            <div id="myModalNotifications{{$user->id}}" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content notification-modal">
                        <div class="modal-header notification-modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title"><span>{{$user->name}} {{$user->lastname}}</span></h4>
                        </div>
                        <div class="modal-body" id="notifications{{$user->id}}">

                        </div>
                    </div>
                </div>
            </div>

            <!-- Modal settings -->
            <div id="myModalSettings{{$user->id}}" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content notification-modal">
                        <div class="modal-header notification-modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title" style="margin-bottom: 0px !important;"><span>{{$user->name}} {{$user->lastname}}</span></h4>
                        </div>
                        <div class="modal-body">
                            <form action="measuring/time/{{$user->id}}" method="POST">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="form-group">
                                    <label for="sel1">Vreme merenja:</label>
                                    <select class="form-control" id="sel1" name="measuring_time">
                                        <option @if($user->measuring_time == 300000) selected @endif value="300000">5 minuta</option>
                                        <option @if($user->measuring_time == 600000) selected @endif value="600000">10 minuta</option>
                                        <option @if($user->measuring_time == 900000) selected @endif value="900000">15 minuta</option>
                                        <option @if($user->measuring_time == 1800000) selected @endif value="1800000">30 minuta</option>
                                        <option @if($user->measuring_time == 2700000) selected @endif value="2700000">45 minuta</option>
                                        <option @if($user->measuring_time == 3600000) selected @endif value="3600000">60 minuta</option>
                                    </select>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <button type="submit" class="btn notification-btn modal-btn">Sačuvaj</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
    <div class="col-md-7">
        <div class="users-table">
            <div class="row">
                <div class="col-xs-12">
                    <h3 style="font-size: 18px;">Vaš profil</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <img src="img/big-avatar.png">
                </div>
                <div class="col-md-5">
                    <h4>{{Auth::user()->name}} {{Auth::user()->lastname}}</h4>
                    <p>{{Auth::user()->email}}</p>
                </div>
                <div class="col-md-3">
                    <button type="button" class="btn notification-btn modal-btn" data-toggle="modal" data-target="#myModal">Promenite lozinku</button>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content notification-modal">
            <div class="modal-header notification-modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Promeni lozinku</h4>
            </div>
            <div class="modal-body">
                <form action="password/change" method="POST">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="row">
                        <div class="col-xs-12">
                            <input class="form-control notification-textarea" placeholder="Nova lozinka" name="password" type="password" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <button type="submit" class="btn notification-btn modal-btn">Sačuvaj</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<style>
    body {
        background: #ffffff !important;
    }
</style>
<script>
    var NotificationId = 0;
    var ParametersId = 0;
    var size = 0;
    var idForSize = 0;

    $('.pikcer-month').css("display", "none");

    function getDayParameters(id) {
        $('#picker-month' + id).css("display", "none");
        $('#picker-month' + id).hide();

        $('#picker-day' + id).css("display", "block");
        $('#picker-day' + id).show();

        $('.no-results-answer').html("");
        $('.no-results-answer').hide();

        $("#buttonMonth" + id).css("background-color", "transparent").css("color", "black");
        $("#buttonDay" + id).css("background-color", "#3bdbac").css("color", "white");

        while (size) {
            $('.month-measuring-stats').html("");
            $('.month-measuring-stats').hide();
            size--;
        }
    }

    function findDayParameters(id) {

        $('.no-results-answer').html("");
        $('.no-results-answer').hide();

        while (size) {
            $('.month-measuring-stats').html("");
            $('.month-measuring-stats').hide();
            size--;
        }

        var pikcerValue = $('#day-picker-value'+id).val();

        if (pikcerValue == "") {
            toastr.error("Izaberite datum koji pretražujete!");
        }
        else {
            var res = pikcerValue.split("-");
            $.ajax({
                type: "POST",
                url: "api/measuring/parameters",
                data: {
                    id:id,
                    day:res[2],
                    month:res[1],
                    year:res[0]
                }
            }).done(function(data) {
                var response = data.entity;
                idForSize = id;
                var i = response.length;
                if (i==0) {
                    $('#date'+id).after(
                        '<p class="no-results-answer">Nema rezultata</p>'
                    );
                }
                size = i;
                $(document).ready(function () {
                    while (i) {
                        $('#date'+id).after(
                            '<div id="month-measuring-stats'+ i + '" class="panel collapse-informations month-measuring-stats">'+
                            '<div class="panel-heading collapse-text">'+
                            '<h4 class="panel-title" id="date">' +
                            '<a id="new' + 1 + '" data-toggle="collapse" data-parent="#accordion" href="#collapseModal'+ i + '">'+ response[i-1].created_at +'</a>'+
                            '</h4>' +
                            '</div>'+
                            '<div id="collapseModal'+ i + '" class="panel-collapse collapse">'+
                            '<div class="panel-body statistic-modal">' +
                            '<img src="img/pulse-statistic.png"><p class="modal-puls">Puls: <span>'+ response[i-1].pulse +'</span></p><br>'+ '<hr>' +
                            '<img src="img/sun-statistic.png"><p class="modal-light">Osvetljenje: <span>'+ response[i-1].light +'</span></p><br>'+ '<hr>' +
                            '<img src="img/walking-statistic.png"><p class="modal-activity">Aktivnost: <span>'+ response[i-1].activity +'</span></p><br>'+ '<hr>' +
                            '<img src="img/phone_log_icon.png" style="width: 26px;"><p class="modal-phone">Vreme na telefonu: <span>'+ response[i-1].phonetime +'</span>'+ '<hr>' +
                            '</div>'+
                            '</div>'+
                            '</div>'
                        );
                        i--;
                    }
                });
            });
            ParametersId = id;
        }
    }

    function getNotifications(id) {
        var response;
        var image;

        $('.notifications-modal').html("");
        $('.notifications-modal').hide();

        if (id == NotificationId) {
        }
        else {
            $.ajax({
                type: "GET",
                url: "api/user/notification/" + id
            }).done(function(data) {
                response = data.entity;
                var i = response.length;
                if (i==0) {
                    $('#notifications'+id).after(
                        '<p class="no-results-answer">Nema rezultata</p>'
                    );
                }
                $(document).ready(function () {
                    while (i) {
                        if (response[i-1].type == 'DOCTOR-ADVICE'){
                            image = '<img src="img/notifications_pulse_icon.png">';
                        }
                        else {
                            image ='<img src="img/notifications_activity_icon.png">';
                        }
                        $('#notifications'+id).after(
                            '<div class="panel collapse-informations notifications-modal">'+
                            '<div class="panel-heading collapse-text">'+
                            '<h4 class="panel-title" id="date">' +
                            image+'<a id="new' + 1 + '" data-toggle="collapse" data-parent="#accordion" href="#collapseNotification'+ i + '">'+ '<span class="system">' + response[i-1].title + '</span>' + '</a>'+
                            '</h4>' +
                            '</div>'+
                            '<div id="collapseNotification'+ i + '" class="panel-collapse collapse">'+
                            '<div class="panel-body">' +
                            '<span>'+ response[i-1].type +'</span><br>' + '<p>'+ response[i-1].description +'</p>' +
                            '</div>'+
                            '</div>'+
                            '</div>'
                        );
                        i--;
                    }
                });
                NotificationId = id;
            });
        }
    }

    function monthMeasuring(id) {

        $('.no-results-answer').html("");
        $('.no-results-answer').hide();

        $('#picker-day'+id).css("display", "none");
        $('#picker-day'+id).hide();

        $("#buttonMonth"+id).css("background-color","#3bdbac").css("color","white");
        $("#buttonDay"+id).css("background-color","transparent").css("color","black");;

        while (size) {
            $('.month-measuring-stats').html("");
            $('.month-measuring-stats').hide();
            size--;
        }
        $('#picker-month'+id).css("display", "block");
    }

    function startMonthMeasuring(id) {

        $('.no-results-answer').html("");
        $('.no-results-answer').hide();

        while (size) {
            $('.month-measuring-stats').html("");
            $('.month-measuring-stats').hide();
            size--;
        }

        var pikcerValue = $('#picker-value'+id).val();
        if (pikcerValue == "") {
            toastr.error("Izaberite datum koji pretražujete!");
        }
        else {
            var res = pikcerValue.split("-");
            $.ajax({
                type: "POST",
                url: "api/month/measuring/parameters",
                data: {
                    id:id,
                    month: res[1],
                    year: res[0]
                }
            }).done(function(data) {
                var response = data.entity;
                var i = response.length;
                if (i==0) {
                    $('#date'+id).after(
                        '<p class="no-results-answer">Nema rezultata</p>'
                    );
                }
                idForSize = id;
                size = i;
                $(document).ready(function () {
                    while (i) {
                        $('#date'+id).after(
                            '<div class="panel collapse-informations month-measuring-stats">'+
                            '<div class="panel-heading collapse-text">'+
                            '<h4 class="panel-title" id="date">' +
                            '<a id="new' + 1 + '" data-toggle="collapse" data-parent="#accordion" href="#collapseModal'+ i + '">'+ response[i-1].created_at +'</a>'+
                            '</h4>' +
                            '</div>'+
                            '<div id="collapseModal'+ i + '" class="panel-collapse collapse">'+
                            '<div class="panel-body statistic-modal">' +
                            '<img src="img/pulse-statistic.png"><p class="modal-puls">Puls: <span>'+ response[i-1].pulse +'</span></p><br>'+ '<hr>' +
                            '<img src="img/sun-statistic.png"><p class="modal-light">Osvetljenje: <span>'+ response[i-1].light +'</span></p><br>'+ '<hr>' +
                            '<img src="img/walking-statistic.png"><p class="modal-activity">Aktivnost: <span>'+ response[i-1].activity +'</span></p><br>'+ '<hr>' +
                            '<img src="img/phone_log_icon.png" style="width: 26px;"><p class="modal-phone">Vreme na telefonu: <span>'+ response[i-1].phonetime +'</span>'+ '<hr>' +
                            '</div>'+
                            '</div>'+
                            '</div>'
                        );
                        i--;
                    }
                });
            });
        }
        ParametersId = id;
    }
</script>
@endsection