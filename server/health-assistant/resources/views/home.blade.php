@extends('welcome')
@section('content')
<div class="row">
    <div class="users-table col-xs-12 col-sm-12 col-md-7">
        <h4>Lista pacijenata</h4>
        <table class="table users-table-desc">
            <thead>
                <tr>
                    <th>IME/PREZIME</th>
                    <th>MERENJE</th>
                    <th class="notification-btn-width">NOTIFIKACIJE</th>
                </tr>
            </thead>
            <tbody>
            @foreach($users as $user)
                <tr>
                    <td class="name-avatar"><img src="img/avatar.png">
                        <input type="hidden" id="userId" value="{{ $user->id }}">
                        <span id="user{{$user->id}}" onclick="userlala('{{$user->name}}', '{{$user->lastname}}', '{{$user->id}}')">{{$user->name}} {{$user->lastname}}</span>
                    </td>
                    <td id="activatedAnalyzing{{$user->id}}" style="cursor: not-allowed" class="analyzing-text" onclick="changeBtn({{$user->id}})">
                        <img style="max-width: 43px; max-height:43px;" @if(!$user->measuring) src="img/analyzing-on.png" @else src="img/analyzing-off.png" @endif id="image{{$user->id}}">
                        <span @if(!$user->measuring) style="color: #3bdbac" @else style="color: red;" @endif id="text-statistic{{$user->id}}">@if(!$user->measuring)Pokreni merenje @else Prekini merenje @endif </span>
                    </td>
                    <td class="notification-btn-width"><button type="button" data-toggle="modal" data-target="#myModal{{$user->id}}" class="btn notification-btn">Pošalji notifikaciju</button></td>
                </tr>

                <!-- Modal -->
                <div id="myModal{{$user->id}}" class="modal fade" role="dialog">
                    <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content notification-modal">
                            <div class="modal-header notification-modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Pošalji notifikaciju korisniku <span>{{$user->name}} {{$user->lastname}}</span></h4>
                            </div>
                            <div class="modal-body">
                                <form action="onesignal" method="POST">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="hidden" name="user_id" value="{{$user->id}}">
                                    <input type="hidden" name="player_id" value="{{$user->player_id}}">
                                    <div class="row">
                                        <div class="col-xs-3 check-box-forgot-pw">
                                            <div class="sch_cal_column">
                                                <div class="sch_cal_row" style="background: #3bdbac">
                                                    <div class="sch_cal_time"><p class="ctime">Zdravlje</p></div>
                                                    <div style="height: 100%">
                                                         <input id="checkBox" type="radio" name="status" checked value="HEALTH">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-3">
                                            <div class="sch_cal_column">
                                                <div class="sch_cal_row">
                                                    <div class="sch_cal_time"><p class="ctime">Aktivnost</p></div>
                                                    <div style="height: 100%">
                                                         <input id="checkBox" class="input-assumpte" type="radio" name="status" value="ACTIVITY">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <p class="analyzing-text">Izaberite</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <input class="form-control notification-textarea" placeholder="Naslov" name="title" required>
                                        </div>
                                        <div class="col-xs-12">
                                            <textarea class="form-control notification-textarea" placeholder="Napišite poruku..." required rows="5" name="description"></textarea>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <button type="submit" class="btn notification-btn modal-btn" onclick="closeModal()">Pošaljite notifikaciju</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
            </tbody>
        </table>
        @if($users->isEmpty())
            <center>
                <h3>Nema rezultata</h3>
            </center>
        @endif
    </div>
    <div class="users-table single-user-table col-xs-12 col-sm-12 col-md-5">
        <div class="row">
            <div class="col-xs-6 col-sm-6 col-md-6">
                <h4>Statistika</h4>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-6 statistic-avatar">
                @foreach($users as $key => $user)
                    @if($key==0)
                        <img src="img/avatar.png" style="margin-right: 5px"><span id="name-stats">{{$user->name}} {{$user->lastname}}</span>
                    @endif
                @endforeach
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-3">
                <center>
                    <div class="single-stats">
                        <img src="img/inactioncy.png">
                        <p>Mirovanje</p>
                        <span id="mirovanjeTime">No results</span>
                    </div>
                </center>
            </div>
            <div class="col-xs-12 col-sm-3">
                <center>
                    <div class="single-stats">
                        <img src="img/runing.png">
                        <p>Trčanje</p>
                        <span id="trcanjeTime">No results</span>
                    </div>
                </center>
            </div>
            <div class="col-xs-12 col-sm-3">
                <center>
                    <div class="single-stats">
                        <img src="img/walking.png">
                        <p>Hodanje</p>
                        <span id="hodanjeTime">No results</span>
                    </div>
                </center>
            </div>
            <div class="col-xs-12 col-sm-3">
                <center>
                    <div class="single-stats">
                        <img src="img/in_vehicle.png">
                        <p>U vozilu</p>
                        <span id="autoTime">No results</span>
                    </div>
                </center>
            </div>
        </div>
        <div class="row">
            <div class="current-stats-title col-xs-12">
                <p>Poslednji rezultati</p>
            </div>
        </div>
        @if($users->isEmpty())
            <center>
                <h3>Nema rezultata</h3>
            </center>
        @endif
        @foreach($users as $key => $user)
            @if($key==0)
        <div class="row">
            <div class="col-xs-12">
                <div class="single-current-stats">
                    <div class="row">
                        <div class="col-xs-1 col-sm-1">
                            <img src="img/walking-statistic.png">
                        </div>
                        <div class="about-stats col-xs-8 col-sm-9">
                            <span>Hodanje</span><br>
                            <img src="img/walking-line.png">
                        </div>
                        <div class="number-stats col-xs-2 col-sm-2 check-box-forgot-pw">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" class="walking-checkbox" name="walking{{$user->id}}" id="walking{{$user->id}}">
                                        <span class="cr">
                                            <i class="cr-icon fa fa-check">
                                            </i>
                                        </span>
                                    </input>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="single-current-stats">
                    <div class="row">
                        <div class="col-xs-1 col-sm-1">
                            <img src="img/pulse-statistic.png">
                        </div>
                        <div class="about-stats col-xs-8 col-sm-9">
                            <span>Puls</span><br>
                            <img src="img/pulse-line.png">
                        </div>
                        <div class="number-stats col-xs-2 col-sm-2">
                             <div class="checkbox">
                                <label>
                                    <input type="checkbox" class="pulse-checkbox" name="pulse{{$user->id}}" id="pulse{{$user->id}}" value="">
                                        <span class="cr">
                                            <i class="cr-icon fa fa-check">
                                            </i>
                                        </span>
                                    </input>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="single-current-stats">
                    <div class="row">
                        <div class="col-xs-1 col-sm-1">
                            <img src="img/sun-statistic.png">
                        </div>
                        <div class="about-stats col-xs-8 col-sm-9">
                            <span>Izloženost svetlosti</span><br>
                            <img src="img/light-line.png">
                        </div>
                        <div class="number-stats col-xs-2 col-sm-2">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" class="sun-checkbox" id="sun{{$user->id}}" name="sun{{$user->id}}" value="">
                                        <span class="cr">
                                            <i class="cr-icon fa fa-check">
                                            </i>
                                        </span>
                                    </input>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="single-current-stats">
                    <div class="row">
                        <div class="phone-time col-xs-1 col-sm-1">
                            <img src="img/phone_log_icon.png">
                        </div>
                        <div class="about-stats phone-time-line col-xs-8 col-sm-9">
                            <span>Vreme na telefonu</span><br>
                            <img src="img/time_phone_lines.png">
                        </div>
                        <div class="number-stats col-xs-2 col-sm-2">
                            <div class="checkbox">
                                <label>
                                    <input style="background: red" type="checkbox" class="phonetime-checkbox" name="phonetime{{$user->id}}" id="phonetime{{$user->id}}" value="">
                                        <span class="cr">
                                            <i class="cr-icon fa fa-check">
                                            </i>
                                        </span>
                                    </input>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif
        @endforeach
    </div>
</div>
<style>
    body {
        background: #ffffff !important;
    }
</style>
<script src="https://js.pusher.com/4.1/pusher.min.js"></script>
<script>
    if (document.getElementById("userId")) {
        var acitiveBtn = document.getElementById("userId").value;
    }
    var measuringIsStarted = false;

    //pusher za notifikaciju
    var pusher = new Pusher('4db5a09a2fe8cf130080', {
        cluster: 'eu'
    });

    var channel = pusher.subscribe('start-message');
    channel.bind('start-event', function(data) {
        if (data.message.type == 'success')
        {
            toastr.success(data.message.message);
            measuringIsStarted = true;
            document.getElementById('image'+data.id).src='img/analyzing-off.png';
            document.getElementById('text-statistic'+data.id).style.color ='red';
            document.getElementById('text-statistic'+data.id).innerHTML = 'Prekini merenje';

            $('#walking' + acitiveBtn).prop('disabled', true);
            $('#sun' + acitiveBtn).prop('disabled', true);
            $('#pulse' + acitiveBtn).prop('disabled', true);
            $('#phonetime' + acitiveBtn).prop('disabled', true);

            localStorage.setItem("walking"+data.id,data.id);
            localStorage.setItem("sun"+data.id,data.id);
            localStorage.setItem("pulse"+data.id,data.id);
            localStorage.setItem("phonetime"+data.id,data.id);
        }
        else
        {
            measuringIsStarted = true;
            toastr.error(data.message.message);
        }

    });

    var pusher = new Pusher('4db5a09a2fe8cf130080', {
        cluster: 'eu'
    });
    var channel = pusher.subscribe('stop-message');
    channel.bind('stop-event', function(data) {
        if (data.message.type == 'success')
        {
            toastr.success(data.message.message);
            document.getElementById('image'+data.id).src = 'img/analyzing-on.png';
            document.getElementById('text-statistic'+data.id).style.color ='#3bdbac';
            document.getElementById('text-statistic'+data.id).innerHTML = 'Pokreni merenje ';

            $('#walking' + acitiveBtn).prop('disabled', false);
            $('#sun' + acitiveBtn).prop('disabled', false);
            $('#pulse' + acitiveBtn).prop('disabled', false);
            $('#phonetime' + acitiveBtn).prop('disabled', false);

            localStorage.removeItem("walking"+data.id);
            localStorage.removeItem("sun"+data.id);
            localStorage.removeItem("pulse"+data.id);
            localStorage.removeItem("phonetime"+data.id);
        }
        else
        {
            toastr.error(data.message.message);
        }
    });

    if (localStorage.getItem("walking"+acitiveBtn) || localStorage.getItem("sun"+acitiveBtn) || localStorage.getItem("phonetime"+acitiveBtn || localStorage.getItem("pulse"+acitiveBtn))) {
        if (localStorage.getItem("walking" + acitiveBtn)) {
            $('#walking' + acitiveBtn).prop('checked', true);
            $('#walking' + acitiveBtn).prop('disabled', true);
        }
        if (!localStorage.getItem("walking" + acitiveBtn)) {
            $('#walking' + acitiveBtn).prop('checked', false);
            $('#walking' + acitiveBtn).prop('disabled', true);
        }
        if (localStorage.getItem("sun" + acitiveBtn)) {
            $('#sun' + acitiveBtn).prop('checked', true);
            $('#sun' + acitiveBtn).prop('disabled', true);
        }
        if (!localStorage.getItem("sun" + acitiveBtn)) {
            $('#sun' + acitiveBtn).prop('checked', false);
            $('#sun' + acitiveBtn).prop('disabled', true);
        }
        if (localStorage.getItem("pulse" + acitiveBtn)) {
            $('#pulse' + acitiveBtn).prop('checked', true);
            $('#pulse' + acitiveBtn).prop('disabled', true);
        }
        if (!localStorage.getItem("pulse" + acitiveBtn)) {
            $('#pulse' + acitiveBtn).prop('checked', false);
            $('#pulse' + acitiveBtn).prop('disabled', true);
        }
        if (localStorage.getItem("phonetime" + acitiveBtn)) {
            $('#phonetime' + acitiveBtn).prop('checked', true);
            $('#phonetime' + acitiveBtn).prop('disabled', true);
        }
        if (!localStorage.getItem("phonetime" + acitiveBtn)) {
            $('#phonetime' + acitiveBtn).prop('checked', false);
            $('#phonetime' + acitiveBtn).prop('disabled', true);
        }
    }
    else {
        $('#walking' + acitiveBtn).removeAttr('checked');
        $('#sun' + acitiveBtn).removeAttr('checked');
        $('#pulse' + acitiveBtn).removeAttr('checked');
        $('#phonetime' + acitiveBtn).removeAttr('checked');

        $('#walking' + acitiveBtn).removeAttr('disabled');
        $('#sun' + acitiveBtn).removeAttr('disabled');
        $('#pulse' + acitiveBtn).removeAttr('disabled');
        $('#phonetime' + acitiveBtn).removeAttr('disabled');

        $('#walking' + acitiveBtn).prop('checked', true);
        $('#sun' + acitiveBtn).prop('checked', true);
        $('#pulse' + acitiveBtn).prop('checked', true);
        $('#phonetime' + acitiveBtn).prop('checked', true);

        $('#walking' + acitiveBtn).prop('disabled', false);
        $('#sun' + acitiveBtn).prop('disabled', false);
        $('#pulse' + acitiveBtn).prop('disabled', false);
        $('#phonetime' + acitiveBtn).prop('disabled', false);
    }

    $.ajax({
        type: "POST",
        url: "api/single/measuring",
        data: {
            'user_id': acitiveBtn
        }
    }).done(function(data) {
        if (!data.entity) {
            document.getElementById("mirovanjeTime").innerHTML = "No results";
            document.getElementById("hodanjeTime").innerHTML = "No results";
            document.getElementById("autoTime").innerHTML = "No results";
            document.getElementById("trcanjeTime").innerHTML = "No results";
        }
        else {
            if (data.entity.stationary) {
                document.getElementById("mirovanjeTime").innerHTML = data.entity.stationary;
            }
            if (data.entity.stationary == "") {
                document.getElementById("mirovanjeTime").innerHTML = "No results";
            }

            if (data.entity.walking) {
                document.getElementById("hodanjeTime").innerHTML = data.entity.walking;
            }
            if (data.entity.walking == "") {
                document.getElementById("hodanjeTime").innerHTML = "No results";
            }

            if (data.entity.incar) {
                document.getElementById("autoTime").innerHTML = data.entity.incar;
            }
            if (data.entity.incar == "") {
                document.getElementById("autoTime").innerHTML = "No results";
            }

            if (data.entity.running) {
                document.getElementById("trcanjeTime").innerHTML = data.entity.running;
            }
            if (data.entity.running == "") {
                document.getElementById("trcanjeTime").innerHTML = "No results";
            }
        }

    });

    if (document.getElementById("activatedAnalyzing"+acitiveBtn)) {
        document.getElementById("activatedAnalyzing"+acitiveBtn).style.cursor = "pointer";
        document.getElementById("user"+acitiveBtn).style.color = "#3bdbac";
    }

    function userlala(name, lastname, id) {
        document.getElementById("name-stats").innerHTML = name + ' ' + lastname;
        document.getElementById("user" + id).style.color = "#3bdbac";
        document.getElementById("user" + acitiveBtn).style.color = "black";
        document.getElementById("activatedAnalyzing" + id).style.cursor = "pointer";
        document.getElementById("activatedAnalyzing" + acitiveBtn).style.cursor = "not-allowed";

        document.getElementById("walking"+acitiveBtn).id = "walking"+id;
        document.getElementById("sun"+acitiveBtn).id = "sun"+id;
        document.getElementById("phonetime"+acitiveBtn).id = "phonetime"+id;
        document.getElementById("pulse"+acitiveBtn).id = "pulse"+id;

        acitiveBtn = id;

        if (localStorage.getItem("walking"+acitiveBtn) || localStorage.getItem("sun"+acitiveBtn) || localStorage.getItem("phonetime"+acitiveBtn || localStorage.getItem("pulse"+acitiveBtn))) {
            if (localStorage.getItem("walking" + acitiveBtn)) {
                $('#walking' + acitiveBtn).prop('checked', true);
                $('#walking' + acitiveBtn).prop('disabled', true);
            }
            if (!localStorage.getItem("walking" + acitiveBtn)) {
                $('#walking' + acitiveBtn).prop('checked', false);
                $('#walking' + acitiveBtn).prop('disabled', true);
            }
            if (localStorage.getItem("sun" + acitiveBtn)) {
                $('#sun' + acitiveBtn).prop('checked', true);
                $('#sun' + acitiveBtn).prop('disabled', true);
            }
            if (!localStorage.getItem("sun" + acitiveBtn)) {
                $('#sun' + acitiveBtn).prop('checked', false);
                $('#sun' + acitiveBtn).prop('disabled', true);
            }
            if (localStorage.getItem("pulse" + acitiveBtn)) {
                $('#pulse' + acitiveBtn).prop('checked', true);
                $('#pulse' + acitiveBtn).prop('disabled', true);
            }
            if (!localStorage.getItem("pulse" + acitiveBtn)) {
                $('#pulse' + acitiveBtn).prop('checked', false);
                $('#pulse' + acitiveBtn).prop('disabled', true);
            }
            if (localStorage.getItem("phonetime" + acitiveBtn)) {
                $('#phonetime' + acitiveBtn).prop('checked', true);
                $('#phonetime' + acitiveBtn).prop('disabled', true);
            }
            if (!localStorage.getItem("phonetime" + acitiveBtn)) {
                $('#phonetime' + acitiveBtn).prop('checked', false);
                $('#phonetime' + acitiveBtn).prop('disabled', true);
            }
        }
        else {
            $('#walking' + acitiveBtn).removeAttr('checked');
            $('#sun' + acitiveBtn).removeAttr('checked');
            $('#pulse' + acitiveBtn).removeAttr('checked');
            $('#phonetime' + acitiveBtn).removeAttr('checked');

            $('#walking' + acitiveBtn).removeAttr('disabled');
            $('#sun' + acitiveBtn).removeAttr('disabled');
            $('#pulse' + acitiveBtn).removeAttr('disabled');
            $('#phonetime' + acitiveBtn).removeAttr('disabled');

            $('#walking' + acitiveBtn).prop('checked', true);
            $('#sun' + acitiveBtn).prop('checked', true);
            $('#pulse' + acitiveBtn).prop('checked', true);
            $('#phonetime' + acitiveBtn).prop('checked', true);

            $('#walking' + acitiveBtn).prop('disabled', false);
            $('#sun' + acitiveBtn).prop('disabled', false);
            $('#pulse' + acitiveBtn).prop('disabled', false);
            $('#phonetime' + acitiveBtn).prop('disabled', false);
        }
        $.ajax({
            type: "POST",
            url: "api/single/measuring",
            data: {
                'user_id': id
            }
        }).done(function(data) {
            if (!data.entity) {
                document.getElementById("mirovanjeTime").innerHTML = "No results";
                document.getElementById("hodanjeTime").innerHTML = "No results";
                document.getElementById("autoTime").innerHTML = "No results";
                document.getElementById("trcanjeTime").innerHTML = "No results";
            }
            else {
                if (data.entity.stationary) {
                    document.getElementById("mirovanjeTime").innerHTML = data.entity.stationary;
                }
                if (data.entity.stationary == "") {
                    document.getElementById("mirovanjeTime").innerHTML = "No results";
                }

                if (data.entity.walking) {
                    document.getElementById("hodanjeTime").innerHTML = data.entity.walking;
                }
                if (data.entity.walking == "") {
                    document.getElementById("hodanjeTime").innerHTML = "No results";
                }

                if (data.entity.incar) {
                    document.getElementById("autoTime").innerHTML = data.entity.incar;
                }
                if (data.entity.incar == "") {
                    document.getElementById("autoTime").innerHTML = "No results";
                }

                if (data.entity.running) {
                    document.getElementById("trcanjeTime").innerHTML = data.entity.running;
                }
                if (data.entity.running == "") {
                    document.getElementById("trcanjeTime").innerHTML = "No results";
                }
            }

        });
    }

    function changeBtn(id) {
        if (acitiveBtn == id) {
            var walking;
            var pulse;
            var sun;
            var phonetime;

            if (document.getElementById('image'+id).src == 'http://homestead.test/img/analyzing-on.png') {
                setTimeout(function(){
                    if (measuringIsStarted == false) {
                        toastr.error("Aplikacija na satu nije pokrenuta");
                    }
                    else {
                        measuringIsStarted = false;
                    }

                }, 5000);
                if ($('#walking' + id).is(':checked')) {
                    walking = true;
                }
                else {
                    walking = false;
                }
                if ($('#pulse' + id).is(':checked')) {
                    pulse = true;
                }
                else {
                    pulse = false;
                }
                if ($('#sun' + id).is(':checked')) {
                    sun = true;
                }
                else {
                    sun = false;
                }
                if ($('#phonetime' + id).is(':checked')) {
                    phonetime = true;
                }
                else {
                    phonetime = false;
                }
                if (walking == false && pulse==false && sun==false && phonetime==false) {
                    toastr.error('Odaberite parametre koje želite da merite!');
                }
                else {
                    $.ajax({
                        type: "POST",
                        url: "send/notification",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                        },
                        data: {
                            'walking': walking,
                            'pulse': pulse,
                            'sun': sun,
                            'phonetime': phonetime,
                            'user_id': id
                        },
                        error: function (request, error) {
                            toastr.error('Došlo je do greške!');
                        }
                    }).done(function(data) {
                    });
                }
            }
            else {
                $.ajax({
                    type: "POST",
                    url: "resend/notification",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: {
                        'user_id': id
                    },
                    error: function (request, error) {
                        toastr.error('Došlo je do greške!');
                    }
                }).done(function(data) {
                });
            }
        }
        else {
            toastr.error('Pogrešan korisnik!');
        }
    }
    $(document).ready(function() {
        $("input:checkbox").click(function() {
            var actualTime = "";
            $(this).parent().toggleClass("highlight");
        });
    })
</script>
@endsection
