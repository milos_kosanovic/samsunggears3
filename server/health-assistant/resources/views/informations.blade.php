@extends('welcome')

@section('content')
<div class="users-table" data-aos="fade-up">
    <div class="row">
        <div class="title-informations col-xs-12">
            <h3>Korisne informacije</h3>
        </div>
    </div>
    <div class="panel collapse-informations">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">
            <div class="panel-heading collapse-text">
                <h4 class="panel-title">
                    <img src="img/avatar.png" style="margin-right: 5px;">
                    Kako ja mogu koristiti ovaj servis?
                    <span class="glyphicon glyphicon-chevron-down pull-right chevron-icon icon-arrow-responsive"></span>
                </h4>
            </div>
        </a>
        <div id="collapse2" class="panel-collapse collapse">
            <div class="panel-body">Neophodno je da korisnik preuzme aplikaciju “Health Assistant” sa Google Play prodavnice.
                Nakon toga mora kreirati svoj nalog i pristupiti aplikaciji.
                Takođe potrebno je da korisnik poseduje Samsung Gear S3 pametni sat sa instaliranom aplikacijom “Health Assistant” koju može preuzeti sa Galaxy Apps prodavnice.
                Nalog doktora kreira administrator.
            </div>
        </div>
    </div>
    <div class="panel collapse-informations">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">
            <div class="panel-heading collapse-text">
                <h4 class="panel-title">
                    <img src="img/avatar.png" style="margin-right: 5px;">
                    Kako da pošaljem notifikaciju?
                    <span class="glyphicon glyphicon-chevron-down pull-right chevron-icon icon-arrow-responsive"></span>
                </h4>
            </div>
        </a>
        <div id="collapse3" class="panel-collapse collapse">
            <div class="panel-body">Kada se ulogujete kao doktor, na početnoj strani se nalazi lista pacijenata.
                Pored svakog pacijenta postoji dugme "Pošalji notifikaciju".
                Klikom na dugme otvara vam se nova forma za izabranog korisnika gde možete birati kategoriju, naslov i poruku notifikacije.
                Klikom na dumge "Pošalji notifikaciju", notifikacija se šalje na sat i mobilni telefon izabranog korisnika.
            </div>
        </div>
    </div>
    <div class="panel collapse-informations">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
            <div class="panel-heading collapse-text">
                <h4 class="panel-title">
                    <img src="img/avatar.png" style="margin-right: 5px;">
                    Kako da pokrenem merenje?
                    <span class="glyphicon glyphicon-chevron-down pull-right chevron-icon icon-arrow-responsive"></span>
                </h4>
            </div>
        </a>
        <div id="collapse1" class="panel-collapse collapse">
            <div class="panel-body">Kada se ulogujete kao doktor, na početnoj strani se nalazi lista pacijenata.
                Pored svakog pacijenta postoji dugme "Pokreni merenje".
                Klikom na dugme automatski se pokreće merenje za izabranog korisnika na telefonu.
            </div>
        </div>
    </div>
    <div class="panel collapse-informations">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse4">
            <div class="panel-heading collapse-text">
                <h4 class="panel-title">
                    <img src="img/avatar.png" style="margin-right: 5px;">
                    Kako da vas kontaktiram?
                    <span class="glyphicon glyphicon-chevron-down pull-right chevron-icon icon-arrow-responsive"></span>
                </h4>
            </div>
        </a>
        <div id="collapse4" class="panel-collapse collapse">
            <div class="panel-body">Mogućnost kontaktiranja našeg administratora je dostupna svim korisnicima koji nisu prijavljenji na sajtu “Health Assistant” aplikacije.
                Ujedno kontakt forma dostupna je i korisnicima koji su prijavljeni i poseduju nalog.
            </div>
        </div>
    </div>
</div>
<style>
    body {
        background: #ffffff !important;
    }
</style>
@endsection