<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        $faker = Faker\Factory::create();
//        $dummy_status = ['Nikola', 'Miloš','Nemanja', 'Zoran'];
//        $dummy_status2 = ['Nikolić', 'Milosević','Nemanjić', 'Kustrimović'];
//        for($i = 0; $i <= 6; $i++) {
//            DB::table('users')->insert([
//                'name' => $dummy_status[rand( 0, 3 )],
//                'lastname' => $dummy_status2[rand( 0, 3 )],
//                'email' => $faker->unique()->email(),
//                'password' => bcrypt(123),
//                'player_id' => 'ads',
//                'role' => 'PATIENT',
//                'measuring_time' => '1800000'
//            ]);
//        }

        DB::table('users')->insert([
            'name' => 'Doctor',
            'lastname' => 'Doctor',
            'email' => 'doctor@demo.com',
            'password' => bcrypt(123),
            'role' => 'DOCTOR'
        ]);

//        DB::table('users')->insert([
//            'name' => 'Darko',
//            'lastname' => 'Kostic',
//            'email' => 'patient@demo.com',
//            'password' => bcrypt(123),
//            'role' => 'PATIENT',
//            'player_id' => 'ssss',
//            'measuring_time' => '00:30:00'
//        ]);

        DB::table('users')->insert([
            'name' => 'Admin',
            'lastname' => 'Admin',
            'email' => 'admin@demo.com',
            'password' => bcrypt('admin123'),
            'role' => 'ADMIN',
//            'measuring_time' => 1800000,
        ]);
    }
}
