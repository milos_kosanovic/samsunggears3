<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class NotificationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $dummy_status = ['ACTIVITY', 'HEALTH'];
        $dummy_status2 = ['SYSTEM', 'DOCTOR-ADVICE'];
        for($i = 0; $i <= 20; $i++) {
            DB::table('notifications')->insert([
                'title' => $faker->name(),
                'description' => $faker->text($maxNbChars = 80),
                'status' => $dummy_status[rand( 0, 1 )],
                'type' => $dummy_status2[rand( 0, 1 )],
                'user_id' => rand(1,8),
                'read' => rand(0,1),
                'created_at' => \Carbon\Carbon::now(),
            ]);
        }

        DB::table('notifications')->insert([
            'title' => 'Vaš puls je previsok',
            'description' => 'Odmorite, previše ste uzrujani.',
            'status' => 'HEALTH',
            'type' => 'SYSTEM',
            'user_id' => 9,
            'read' => rand(0,1),
            'created_at' => \Carbon\Carbon::now(),
        ]);

        DB::table('notifications')->insert([
            'title' => 'Vreme je za lek',
            'description' => 'Podsećamo vas da je vreme da popijete lek.',
            'status' => 'HEALTH',
            'type' => 'DOCTOR-ADVICE',
            'user_id' => 9,
            'read' => rand(0,1),
            'created_at' => \Carbon\Carbon::now(),
        ]);

        DB::table('notifications')->insert([
            'title' => 'Prošetajte malo',
            'description' => 'Predugo ste sedeli, prošetajte malo.',
            'status' => 'ACTIVITY',
            'type' => 'SYSTEM',
            'user_id' => 9,
            'read' => rand(0,1),
            'created_at' => \Carbon\Carbon::now(),
        ]);

        DB::table('notifications')->insert([
            'title' => 'Zakazivanje pregleda',
            'description' => 'Zakazujem vam pregled za 22.02.2018 u 12:20. Zbog redovne kontrole.',
            'status' => 'HEALTH',
            'type' => 'DOCTOR-ADVICE',
            'user_id' => 9,
            'read' => rand(0,1),
            'created_at' => \Carbon\Carbon::now(),
        ]);

        DB::table('notifications')->insert([
            'title' => 'Preporuka',
            'description' => 'Niste trčali ove nedelje, preporuka je da to u što kraćem roku obavite.',
            'status' => 'ACTIVITY',
            'type' => 'SYSTEM',
            'user_id' => 9,
            'read' => rand(0,1),
            'created_at' => \Carbon\Carbon::now(),
        ]);

        DB::table('notifications')->insert([
            'title' => 'Savet za kretanje',
            'description' => 'Trebali biste da u narednom periodu šetate što više.',
            'status' => 'ACTIVITY',
            'type' => 'DOCTOR-ADVICE',
            'user_id' => 9,
            'read' => rand(0,1),
            'created_at' => \Carbon\Carbon::now(),
        ]);
    }
}
